#!/bin/bash
mkdir release
zip -9 -X -x@releaseexclude.lst -r ./release/starboost.love .
mkdir /tmp/starboostwin
curl -o /tmp/starboostwin/love.zip https://files.catbox.moe/7h39v3.zip
unzip -j -d /tmp/starboostwin /tmp/starboostwin/love.zip
cat /tmp/starboostwin/love.exe ./release/starboost.love > /tmp/starboostwin/starboost.exe
zip -9 -j ./release/starboost-win.zip /tmp/starboostwin/starboost.exe /tmp/starboostwin/*.dll
rm -r /tmp/starboostwin