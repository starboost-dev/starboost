function love.load()
    -- image table
    image = {}
    textinput = "" -- all text input goes through here
    mousewheelinput = 0 -- all mouse wheel input goes through here
    debug_text = ""

    -- enable key repeat so backspace can be held down to trigger love.keypressed multiple times.
    love.keyboard.setKeyRepeat(true)

    -- yo whats the game called
    game_identity = "starboost"
    love.filesystem.setIdentity(game_identity)
    game_song_directory = "songs/"
    
    -- colors 
    c_bg = {0,0,28/255}
    c_main = {170/255,196/255,250/255}
    c_secondary = {0,0,28/255}

    -- fonts
    f_game = love.graphics.getFont()

    -- game properties 
    game_height = 540
    game_width = 960
    game_input = false
    game_audio_offset = 0

    game_state = 1
    game_state_cases = table.enum {
        "song_select",
        "ingame",
        "editor"
    }   

    game_background_dim = 0.4
    
    -- settings
    love.window.setMode( game_width, game_height )
    love.window.setVSync(0)
    
    -- song global variables
    song_delay = 5000
    song_bpm = 0
    song_beat = 0
    song_count_beat = 0
    song_count_measure = 0
    song_time = 0
    song_duration = 0
    song_sample = 0
    song_samplecount = 0
    song_samplerate = 0
    song_speed = 1

    -- objects
    object = {}
    object.background = "a_bg"
    object.beat = "a_beat"
    object.bpm = "a_bpm"
    object.color = "a_color"
    object.foreground = "a_fg"
    object.spin = "a_spin"
    object.scroll = "a_scroll"
    object.star = "z_star"
    object.moon = "z_moon"
    object.comet = "z_comet"

    -- global variables
    scroll_speed = 1
    
    game_render = 1200
    
    -- profiling
    PROF_CAPTURE = false

    -- libraries, or other code
    package.path = "./src/?.lua;" .. "./src/?/init.lua;" .. package.path
    utf8 = require("utf8")
    sone = require "lib.sone"
    prof = require "lib.jprof"
    lily = require "lib.lily"
    client = require("lib.websocket").new("165.232.76.188", 6630)

    require "src/network"
    require "src/locale"
    require "src/animation"
    require "src/keybind"
    require "src/database"
    -- scenes
    require "src/playfield"
    require "src/chat"
    require "src/gameplay"
    require "src/editor"
    require "src/song_select"

    buildLocale("loc/en_US")
end

function love.update(_dt)
    prof.push("frame")
    prof.push("websocket")
    client:update()
    prof.pop("websocket")
    prof.push("dt")
-- every frame, v fast for calculations
    prof.push("gameplay")
    if game_state == game_state_cases.ingame then
        gameplayLoop(_dt)
    end
    prof.pop("gameplay")
    prof.push("editor")
    if game_state == game_state_cases.editor then
        editorLoop(_dt)
    end
    prof.pop("editor")
    prof.push("song select")
    if game_state == game_state_cases.song_select then
        songSelectLoop(_dt)
    end
    prof.pop("song select")
    prof.pop("dt")
end

function love.textinput( text )
    textinput = textinput .. text
end

function love.wheelmoved( x, y )
    mousewheelinput = y
end

function love.keypressed(key)
    if key == "backspace" then
        -- get the byte offset to the last UTF-8 character in the string.
        local byteoffset = utf8.offset(textinput, -1)

        if byteoffset then
            -- remove the last UTF-8 character.
            -- string.sub operates on bytes rather than UTF-8 characters, so we couldn't do string.sub(text, 1, -2).
            textinput = string.sub(textinput, 1, byteoffset - 1)
        end
    end
end


function love.draw()
    prof.push("draw")
    if game_state == game_state_cases.ingame then ingameDraw() if not gameplay_pause then draw_debug() end end
    if game_state == game_state_cases.editor then editorDraw() draw_debug() end
    if game_state == game_state_cases.song_select then songSelectDraw() end
    prof.push("debug data")
    debugprint = love.timer.getFPS() .. " fps"

    if game_state == game_state_cases.editor then debugprint = debugprint .. "\t EDITOR MODE" end
    if game_state == game_state_cases.ingame then debugprint = debugprint .. "\t INGAME MODE" end
    if game_state == game_state_cases.song_select then debugprint = debugprint end

    prof.pop("debug data")
    prof.pop("draw")
    prof.pop("frame")
end 

function draw_debug()
    if LoadedData then
        debugprint = debugprint .. "\n" .. "\"" .. LoadedData.Title .. "\" by " .. LoadedData.Artist
        debugprint = debugprint .. "\n" 
        debugprint = debugprint .. "\n" .. "DETAILS"
        debugprint = debugprint .. "\n" .. "filepath: " .. LoadedData.AudioPath
        
        local _percentatge =  math.floor((song_time)/LoadedData.SongData.Duration*10000)/100

        debugprint = debugprint .. "\n" .. song_time .. "ms of ".. LoadedData.SongData.Duration .."ms " .. string.format("%.2f",_percentatge) .. "%"
        debugprint = debugprint .. "\n" .. song_delay ..", " .. LoadedData.SongData.Offset
        debugprint = debugprint .. "\n" .. song_bpm .." bpm"
        debugprint = debugprint .. "\n" .. "position " .. song_count_measure .. " : " .. song_count_beat
        debugprint = debugprint .. "\n" .. "song speed " .. math.floor(song_speed*100) .. "%"
        debugprint = debugprint .. "\n" .. "scroll speed " .. math.floor(scroll_speed*100) .. "%"
        debugprint = debugprint .. "\n" 
        debugprint = debugprint .. "\n" .. "SAMPLES"
        debugprint = debugprint .. "\n" .. song_sample .. " @" ..  LoadedData.SongData.SampleRate .. " per second. total " .. LoadedData.SongData.SampleCount 
        debugprint = debugprint .. "\n" 
        debugprint = debugprint .. "\n" .. "CHARTS"
        for _, chart in ipairs(LoadedData.ChartData) do
            debugprint = debugprint .. "\n" .. " [".. chart.Difficulty .. "] \"" .. chart.Name .. "\" by ".. chart.Charters[1]
            debugprint = debugprint .. "\n" .. "Notes: " .. chart.NoteCount
        end
        love.graphics.print(debugprint)
    end
end
-- packs any number of arguments into a table
function pack(...)
    return {...}
end

function sign(x)
-- returns number sign, if any
    if x > 0 then return 1 
    elseif x < 0 then return -1 
    else return 0 end
end

function angle(x,y)
    return math.atan2(y, x)
end

function string:getFirstWord()
    local _word = self
    if string.find(self,"%s") then
        _word = string.sub(self,1,string.find(self,"%s")-1)
    end
    return _word
end

function table.enum(tbl)
	for i = 1, #tbl do
		local v = tbl[i]
		tbl[v] = tonumber(i)
	end
	return tbl
end

function table.kenum(tbl)
    local i = 0
    local new_table = {}
	for k, _ in pairs(tbl) do
        i = i + 1
        new_table[i] = k
    end
    return new_table
end

function table.insert_batch(tbl, ...)
-- to avoid inserting one by one, when i need to do it in specific non loopable pairs
    local args = {...} 
    for i=1, #args do
        table.insert(tbl,args[i])
    end
end

function table.dcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[table.dcopy(orig_key)] = table.dcopy(orig_value)
        end
        setmetatable(copy, table.dcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end
 

function love.quit()
    lily.quit()
    prof.write("prof.mpack")
end
