#i genuinely dont understand this so im adding back the .sh file. sorry sol

all: lovefile

.PHONY:

lovefile: .PHONY
	mkdir -p release
	zip -9 $(addprefix -x ,.git\/* doc\/* release\/* .* *.xcf Makefile README.md) -r release/starboost.love .

windows: lovefile
	mkdir /tmp/starboostwin
	curl -o /tmp/starboostwin/love.zip https://files.catbox.moe/7h39v3.zip
	unzip -j -d /tmp/starboostwin /tmp/starboostwin/love.zip
	cat /tmp/starboostwin/love.exe ./release/starboost.love > /tmp/starboostwin/starboost.exe
	zip -9 -j ./release/starboost-win.zip /tmp/starboostwin/starboost.exe /tmp/starboostwin/*.dll
	rm -r /tmp/starboostwin

clean: .PHONY
	rm -rf release
