# Song folder location & structure

Each song is stored within `starboost/songs/`. Each song folder contains the following:

## 1. *data* file

Contains all the song info and charts info for the game, in plain text.

### Song commands

Each piece of info is stored per line. The possible options are:

- `title X`, where **X** is the song's title. *It can include any symbols as long as its within the same line.*
- `artist X`, where **X** is the song's artist. *It can include any symbols as long as its within the same line.*
- `audio_file X`, where **X** is the path to the songs audio file within the song folder.
- `audio_preview X`, where **X** is the ms from which to start to preview the song in song select. 
- `offset X`, where **X** is song offset in miliseconds.
- `chart`, which initializes a new chart.

### Chart commands
These are relative to a previous `chart` command. They won't have any effect if `chart` has not been declared before.

- `name X`, where **X** shows up as difficulty name. *It can include any symbols as long as its within the same line.*
- `author X`, where **X** shows up as the charters' name. *It can include any symbols as long as its within the same line.*
- `difficulty X`, where **X** is a numeric value to infer song difficulty.
- `background_file X` , where **X** is the path to an image within the song folder to use as background. *By default, the game displays the first background.*
- `foreground_file X` , where **X** is the path to an image within the song folder to use as foreground. *By default, the game displays no foreground until it is declared to do so.*
- `assign X V` , where **X** is the variable name that is to be assigned the value **V**. Chart variables can be used in the chart's map commands invoking them preceded by `%` as `%X`.
- `X map_command Y`, where:
    - **X** is an integer numeric value representing ms within the song,
    - **map_command** is one of the several map commands that represent ingame behaviour, and
    - **Y** are the options required per command.

### Chart Map commands
Each one of these represents ingame behaviour like the presence of notes, bpm changes, background changes, etc. These all follow a number representing the ms in the song they take effect. As they are technically chart commands, they won't have any effect if `chart` has not been declared before.

-  `a_bg N`, which changes the background to the **Nth** loaded background image. *By default, the game displays the first background.*
-  `a_beat X`, which sets the song's measures to **X** beats each.
-  `a_bpm X`, which tells the song to change to **X** bpm. *This only takes effect as a measure is over*
- `a_color L R G B`, where:
    - **L** is the lane in which notes will subsequently be colored. 
    - **R**, **G** and **B** are the RGB components of the color, *from 1 to 255*.
- `a_fg N X Y`, which toggles the **Nth** loaded foreground image. *By default, the game displays no overlay until it is declared to do so.*
    - If the command is called without **X** or **Y** arguments, the overlay is always toggled on and moves the image to the **X** and **Y** coordinates.
    - On the contrary case, it is toggled off and on. 
- `a_spin L X`, which changes the spin velocity to **X** for lane **L**.
- `a_scroll L X`, which changes the scroll velocity to **X** for lane **L**.

For the commands above, accepted values for **L** include:
- `none`, which returns all lanes to the default setting
- `all` which modifies the setting for all lanes
- `comet` which modifies the setting for all comets
- `comet_left` which modifies the setting for comets that move to the left
- `comet_center` which modifies the setting for comets that dont move to the sides
- `comet_right` which modifies the setting for comets that move to the right

Aditionally, you can specify if you want to color comets that *_hold* or comets that you only *_hit* by adding it to the end of the movement direction.
- integer number to represent lanes 1 to 5 *from left to right*.

For the commands below, only the latter is valid.

- `z_star L`, which represents the presence of a hit note (star) at lane **L**.
- `z_moon L W`, which represents the presence of a wide hit note (moon) at lane **L** of width **W**.
- `z_comet D A B F S`, which represents the start of a hit and hold note (comet) of duration **D** from columns **A** to **B** of function **F** and style **S**.

Columns values can be any number from -10 to 10. Some of those values represent:
- `-10` (left edge of the screen) 
- `-5` (left border of the stage) 
- `0` (center of the stage)
- `5` (right border of the stage) 
- `10` (right edge of the screen) 

The available functions for comets are:
- `normal` (by default) behaves normally.
- `line` does not draw comet start or comet end, merely visual.
- `hold` only has the hold component -- it gets rid of the hit component.

The available styles for comets are:
- `straight` is drawn as a straight line.
- `bezier_curve_top` is drawn adjusted to a bézier curve to the top corner. *Non-functional*
- `bezier_curve_bottom` is drawn adjusted to a bézier curve to the bottom corner. *Non-functional*

## 2. audio files

Each song requires, at minimum, one audio file: the song itself, declared in **data** with the `audio_file` command.

## 3. image files

Songs can also contain image files to be used as background when defined *per chart* in **data** with the `background_file`. By default, the game displays the first background file declared, if any.
 
Other image files to be used with `foreground_file` and `a_fg` chart command should be present too.
