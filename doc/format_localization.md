# Localization

The game stores as many files as languages it is localized to. Each file is named accordingly to the IETF language tag for the language it represents.

## Structure
The file consists of lines following a simple `key = "value"` structure.

### Variables
Specific arbitrary keys might require to be specified a variable position within the value. This is done by using the `%` symbol before the name of the variable.

Aditionally, there are a few predefined variables that can be used on any key:

- `%Q`  the `%` symbol
- `%a`	current abbreviated weekday name *e.g. Wed*
- `%A`	current full weekday name *e.g., Wednesday*
- `%b`	current abbreviated month name *e.g., Sep*
- `%B`	current full month name *e.g., September*
- `%c`	current date and time *e.g., 09/16/98 23:48:10*
- `%d`	current day of the month *e.g., 16* 
- `%H`	current hour, using a 24-hour clock *e.g., 23* 
- `%I`	current hour, using a 12-hour clock *e.g., 11* 
- `%M`	current minute *e.g., 48* 
- `%m`	current month *e.g., 09*
- `%p`	current day and night division *either "am" or "pm"*
- `%S`	current second *e.g., 10* 
- `%w`	current weekday (as a numeric value) *e.g., 3*
- `%x`	current date *e.g., 09/16/98*
- `%X`	current time *e.g., 23:48:10*
- `%Y`	current full year *e.g., 1998*
- `%y`	current two-digit year *e.g., 98* 


