heads up this file isnt very updated anymore and should probably be deleted

look at codeberg issues and release notes instead

* EDITOR
    - DONE waveform visualization on backgroun
    - DONE waveform filtering 
    - DONE add filtering xD
    - DONE and the ability to slow down the song while editing
    - DONE and zoom in n out
    - DONE the ability to place down notes (on current ms) using your own keybinds
    - DONE placing notes
    - DONE and snappings
    - DONE placing wide notes
    - DONE recoloring notes in editor
    - DONE placing laterals
    - PENDING oh yea and the ability to make the song louder 
    - PENDING (replaygain algorithm? --remi)
    - PENDING BPM detector (Why --remi)
    - PENDING ability to export songs as .stars (renamed zip file)
    - ???

* PLAYFIELD
    - DONE scroll speed variable
    - ???

* GAMEPLAY
    - DONE ability to hit notes
    - DONE judgement on hit
    - DONE scoring
    - DONE..? combo
    - PENDING 
    - ...

* MENU
    PENDING
    - ...
    
* OTHER
    - PENDING ability to drop songs as .stars (renamed zip file)
    - ???