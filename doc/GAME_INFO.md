
# Songs

Songs in starboost represent the (generally) music and collection of charts. They hold the files that make gameplay possible. Each song holds specific data:

**Song Data:**
- **Title**
    Title of the song.
- **Artist**
    Song's Artist.
- **Chart**
    A list of gameplay elements mappings.

## Chart

A chart is a mapping of expect inputs designed to play along an audio file.  Multiple charts can exist per song. In starboost, charts hold several data and gameplay elements:

**Chart Data:**
- **Name**
    Name of the chart. Can be used to further convey difficulty or to enhance the theme.
- **Author**
    Who is responsible for this chart's design.
- **Difficulty**
    Represented with a numerical value intended to convey, in relation to other charts, it's difficulty.


# Performance
## Score

The **perfect score** for any given chart always is ```1.000.000 + amount of hit notes```. 

The score calculation method can be personalized from a list of modes:
- **Additive:** Starts at 0, each note adds on the score towars the perfect score.
- **Subtractive:** Starts at the perfect score. Any non-optimal hit note removes from the score towards 0.

## Accuracy

Hitting a note modifies the current score according to the accuracy.    

The better the hit time correlates to the note time, the better, described as:

| Name | Timing | score worth ()| combo break? |
|---|---|---|---|
| ⭐ABSOLUTE⭐ | 25ms | 1x + 1 | no |
| ABSOLUTE | 50ms | 1x | no |
| FRAGMENT | 100ms | 0.5x | no |
| COLLAPSE | 150ms | 0x + 1 | yes |
| LOSS  | (miss) | 0x | yes |

*Each notes value is related to the amount of notes in the chart, as ```x = 1.000.000 / notesInMap```*

## Grades

Depending on overall performance, the player will be awarded a grade at the end of each play

The value is in relation to the perfect chart score. 

| Name | Value |
|---|---|
| X | 100% |
| S | 98% or better |
| A | 95% or better |
| B | 90% or better |
| C | 80% or better |
| D | 70% or better |
| F | Any% |

Aditionally, if the player manages to complete the song without losing the combo score, they are awarded with the "Full Constellation" (FC) status. *This status is unique to each chart.*

If the player manages to complete a song with only "collapse" accuracy, they are awarded the "Black Hole" secret status. *This status is unique to each chart.*