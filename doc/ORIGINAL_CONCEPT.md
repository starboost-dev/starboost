
# difficulties

so like. how many would we have

would they have restrictions???  
(like, maybe . easy diffs cant have laterals go between the halves.. or like, just one or two per map yk)  
(and other such restrictions to just make it. not too hard for noobs)

also what are the diff names  
we have to come up with some silly bullshit


# scoring

configurable additive/subtractive scoring  
maxes out at 1mil, just accuracy based  
(*maybe pull an arcaea and have like, "perfect+" and "perfect", both give same points except perfect+ is .. .one extra score, so you get nice leaderboards but it's not impossible to . "perfect" a song for the average joe (nobody wants to grind out a map when the judges are like, 15ms))


# grades

uhhhhh idk  
this is like the least important part save it for later  
but yeah like,  
probably something simple, 70%/80%/90%/95%/98%/100%  
D/C/B/A/S/X  
(or come up with cool names for them)

full combo is called "full constellation" --ari

if u complete a song with only collapse judgements u get the secret BLACK HOLE grade --ari

# judges, anyways

| name | timing(+/-ms) | score worth (x=1000000/notesInMap)| combo break? |
|---|---|---|---|
| ⭐ABSOLUTE⭐ | 15ms | 10x + 1 | no |
| ABSOLUTE | 30ms | 10x | no |
| FRAGMENT | 60ms | 5x | no |
| REMNANT | 125ms | 1x | no |
| COLLAPSE | 140ms | 0x + 1 | yes |
| LOSS  | (miss) | 0x | yes |

# "lore"

game is about weaving stars together  
~~what is this, katamari???~~

characters?? 
https://naturenoon.com/star-constellations-names-meanings/   i guess
since we are waving stars together --ari

name potential waifus after star constellations  
(ari plz pixel art :pleading:)  
> people think we're ripping off that game my old twitter oomfies are making

# note types

note: stars and moons can be color-coded if they want.  
this is to indicate what hand they "should" be hit with... but only when it's not obvious (read: if they want)  
it may make some patterns nicer to read, but ... for the "purpously painful tech reading challenges" you dont want these, it's more of a "hey look put ur hands over here"  
...can also just be used for decoration if we feel like it...

(pov: there is a heart shaped pattern that's all red)

meteor showers can be any color they want to be n_n

stars:      
* average "tap notes"

moons:  
* like stars, but variable size. can be hit with any key they're falling over
* basically we're ripping off chunithm/dancerail/pjsekai
* makes for fun technical charting !!! xd

meteor showers: aka Laterals;
* [how do i even describe these]
* so imagine sdvx lazers but, they one color but control is split into what side of the screen they're on. also they're not analog since >keyboard game
* uhhhhh yeah hhh !!!1
* unlike in sdvx you cant just hold the "knob" down to hit a lot of them in a row though
* holding them is still a concept though (i.e meteors that arent just slams)
* see: last point on hold notes. theyre called meteor showers cos they fall onto the . stars and moons
* LINES BETWEEN METEORS (FOLLOW POINTS)
* help me i am so sick is this corona 2 lol

hold notes:  
* mothers against hold notes
* seriously these suck
* hold notes are cancelled
* you can use meteor showers for this purpose anyway
* oh actually - what if stars could LATCH ON TO METEOR SHOWERS
* SO YOU PRESS A STAR AND A [meteor button] AT THE SAME TIME (and hold the meteor one down)
* AND ITS KIND OF A HOLD NOTE BUT COOLER

# funny mapping tech i thought off

* meteor showers latching onto notes
* star with position of x.5 = funny looking two-key stream (can also be done like, 1-1.5-2-1.5 for gallops, ig???)

# constellations characters

bold the ones i like --ari

**#1 Andromeda – Royal Sea Monster Bait**

#2 Antlia – Air Pump

**# 3 Apus – Bird of Paradise**

**#4 Aquarius – Water-Bearer**

#5 Aquila – Thunderbolt Eagle

**#6 Ara – Altar**

**#7 Aries – Ram**

#8 Auriga – Charioteer

#9 Boötes – Herdsman

#10 Caelum – Chisel

#11 Camelopardalis – Giraffe

**#12 Cancer – Crab**

#13 Canes Venatici – Hunting Dogs

**#14 Canis Major – Big Dog**

#15 Canis Minor – Small Dog

**#16 Capricornus – Sea Goat**

#17 Carina – Keel of Argo Navis

**#18 Cassiopeia – Vain Queen**

#19 Centaurus – Centaur

**#20 Cepheus – King**

#21 Cetus – Whale

#22 Chamaeleon – Chameleon

#23 Circinus – Compass

**#24 Columba – Dove**

#25 Coma Berenices – Berenice’s Hair

**#26 Corona Australis – Southern Crown**

**#27 Corona Borealis – Northern Crown**

#28 Corvus – Raven

#29 Crater – Cup

#30 Crux – Southern Cross

#31 Cygnus – Swan

#32 Delphinus – Dolphin

#33 Dorado – Fish

**#34 Draco – Dragon**

#35 Equuleus – Little Horse

#36 Eridanus – River

#37 Fornax – Furnace

**#38 Gemini – Twins**

#39 Grus – Crane

#40 Hercules – Strong Man

**41 Horologium – Pendulum Clock**

#42 Hydra – Water Serpent

#43 Hydrus – Watersnake

#44 Indus – Indian

**#45 Lacerta – Lizard**

**#46 Leo – Lion**

**#47 Leo Minor – Little Lion**

#48 Lepus – Hare/Rabbit

**#49 Libra – Scales**

#50 Lupus – Wolf

**#51 Lynx – Lynx**

**#52 Lyra – Harp**

#53 Mensa – Table Mountain

#54 Microscopium – Microscope

#55 Monoceros – Unicorn

#56 Musca – Fly

**#57 Norma – Level**
    Precision focused?

#58 Octans – Octant

**#59 Ophiuchus – Serpent-Bearer**

**#60 Orion – Hunter**

#61 Pavo – Peacock

#62 Pegasus – Winged horse

#63 Perseus – Greek Hero

**#64 Phoenix – Firebird**
    Hard character or smth lol

#65 Pictor – Painter’s Easel

**#66 Pisces – Fishes**

#67 Piscis Austrinus – Southern Fish

#68 Puppis – Stern of Argo Navis

**#69 Pyxis – Compass**

#70 Reticulum – Reticle

#71 Sagitta – Arrow

**#72 Sagittarius – Archer**

**#73 Scorpius – Scorpion**

#74 Sculptor – Sculptor

#75 Scutum – Shield

#76 Serpens – Serpent

#77 Sextans – Sextant

**#78 Taurus – Bull**

#79 Telescopium – Telescope

#80 Triangulum – Triangle

#81 Triangulum Australe – Southern triangle

#82 Tucana – Toucan

**#83 Ursa Major – Big bear**

**#84 Ursa Minor – Small bear**

#85 Vela – Sails of Argo Navis

**#86 Virgo – Young Maiden**

#87 Volans – Flying Fish

**#88 Vulpecula – Little Fox**

## idk remis notes about smth

fdkfds

60px, medium hinting, centered other stuff iDK xdd  
lens blur 4.0px  
1px border smooth > mean curvature blur 1px > dupe > gaussian blur 2.5 > merge down

idk like, end results look kinda cursed but a good start