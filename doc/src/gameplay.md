
### init.lua
- shine_anim_holder - controls the hitexplosion animation by counting down its value
- f_combo - font used for the combo message - unused
- judge_timing - the values of each timing
- judge_coloring - the colors of each judgement
- lateral_tick - the time in ms of each tick
- Keybind.ingame - gameplay keybinds

### draw.lua
- local ghost_mode - whether to render playfield or not

### input.lua
- local number - timer until tick end
    - lateral1_hold_timer 
    - lateral2_hold_timer 

- local bool - whether lateral was held for this tick
    - lateral1_held
    - lateral2_held 

- local constant number - time without holding which will be considered a miss
    - lateral1_miss_time 
    - lateral2_miss_time 

- local number - timer that counts up misses 
    - lateral1_miss_timer
    - lateral2_miss_timer 

- input_state - local table - whether the key is hit (1) held (2) or none (0)

- smashes - local table - used to get a keys per second counter - unused

### intro.lua 
- intro_time - local constant number - maximum intro time in ms. this number is reduced if theres leway to show the intro before any input. e.g. if there first input is at ms 0. it will be the full value. if the first input is at ms 1000. it will be 4000 ms. this variable is used in the calculation to ensure its value in ms before any input.

- intro_timer - local number - used to countdown the time for the intro to start playing the song.

- intro_text - local Text - text shown during the intro time.

### judges.lua
- judgements - local table - keeps track of the hit timings. used for accuracy feedback (ur bar)

- judge_size - local number - judge message size
- judge_message - local Image - the imaged used showing the last judge hit
- judge_message_time - time shown the judgemessage at full opacity  (seconds)
- judge_message_fadeout - time that representes the amount to fade out the judge message (seconds)
- judge_message_timer - time counting down in showing the judge message (seconds)

- combo_message = local number - unused

### render.lua
- standard_star_color - local Color - standard color used if hitnotes havent been asigned a color
- render_distance - constant number - early cut off of rendering to improve performance
- lower_render_distance - same as above, but in the opposite direction
