# [**c_bg**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/color/c_bg.md) 
[`color`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/color.md) The default background color.

---
constant
: {`0`, `0`, `28/255`}
