# [**c_main**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/color/c_main.md) 
[`color`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/color.md) The main primary color. It is used for default gameplay element color

---
constant
: { `170/255`, `196/255`, `250/255`}
