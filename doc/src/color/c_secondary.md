# [**c_secondary**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/color/c_secondary.md) 
[`color`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/color.md) The main secondary color.

---
constant
: {`0`, `0`, `28/255`}
