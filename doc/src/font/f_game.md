# [**f_game**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/font/f_game.md) 
[`font`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/font.md) Lame default font.

---
constant
: Default font.