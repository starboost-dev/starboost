# [**img**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/table/img.md) 

[`table`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/table.md)  Holds several tables, each of which hold in turn images for different aspects of the game.

All the values are stored with [`string`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/string.md) keys.

---
### [`table`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/table.md) song_select
Used during the song_select game state. 

background
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md)  used as background in song select.

header
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used as background header in song select.

footer
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used as background foot in song select.

image_container
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md)  used as border for the song background preview.

infobox1
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used to show the selected song's difficulties.

infobox2
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used to show the leaderboards for a selected chart.

infobox3
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used to show the different options a player can personalized when selecting a song.

quickinfo
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used to show the different statistics of a chart such as note count, bpm, and each different note type.

partner_gradient
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used as a gradient behind the selected partner.

song_active
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used for the song list when a song is selected.

song_inactive
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used for the song list when a song is not selected.

start_button
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) for the button that starts playing the selected song.

---
### [`table`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/table.md) playfield
Used whenever the ingame playfield is to be rendered.

stage_glow
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) which is rendered at the rhythm of the bpm, glowing the most at the start of each measure.

stage_sides
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used to render the center and sides of the stage.

stage_bottom
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used to render the bottom of the stage, showing where the hitnotes have to hit.

judgement_star
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used in the ur bar, signifying a perfectly timed hit.

judgement_line
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used in the ur bar, showing the different timings.

---
### [`table`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/table.md) gameplay
Used during the ingame game state.

judge_loss
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) shown whenever the player misses a hitnote.

judge_collapse
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) shown whenever the player achieves a hit with the collapse judgement.

judge_fragment
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) shown whenever the player achieves a hit with the fragment judgement.

judge_absolute
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) shown whenever the player achieves a hit with the absolute judgement.

judge_exact
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) shown whenever the player achieves a hit with the exact judgement.

lane_glow
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) shown whenever the player presses a lane input key.

side_glow_left
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) shown whenever the player presses the left comet key.

side_glow_right
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) shown whenever the player presses the right comet key.

score_number
: A [`table`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/table.md) storing several [`images`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) to use in the score display. The characters `0-9`, `.` and `%`.

star_color
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used as coloring for the star hitnotes.

star_border
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used as an overlay for the color of star hitnotes.

moon_color1
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used as coloring for the moon hitnotes left piece.

moon_color2
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used as coloring for the moon hitnotes center piece.

moon_color3
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used as coloring for the moon hitnotes right piece.

moon_border1
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used as an overlay for the color of the moon hitnotes left piece.

moon_border2
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used as an overlay for the color of the moon hitnotes center piece.

moon_border3
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used as an overlay for the color of the moon hitnotes right piece.

comet_color1
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used as coloring for the comet hitnotes.

comet_color2
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used as coloring for the end of the comets holdlines.

comet_border1
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used as an overlay for the end of the comets holdlines.

comet_border2
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used as an overlay for the end of the comets holdlines.

comet_line_color
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used as coloring for the pieces used as holdline for the comets.

comet_line_border
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) used as an overlay for the pieces used as holdline for the comets.

comet_shine
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) rendered along the comets holdlines when expected to be held.

---
### [`table`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/table.md) editor
Used during the editor game state.

star_ghost
: The [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) shown as a preview of placing a star hitnote.


