# [**object**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/table/object.md) 
[`table`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/table.md) The different gameplay objects. Used to read objects from a song's data file.

All the values are stored with [`string`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/string.md) keys.

---
### [`string`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/string.md) Values

background
: `a_bg` - Sets a background change

beat
: `a_beat` - Sets the number of beats per measure.

bpm
: `a_bpm` - Sets a beats per minute (bpm) change

color
: `a_color` - Sets a color property change.

foreground
: `a_fg` - Sets a foreground change

spin
: `a_spin` - Describes a spin property change

scroll
: `a_scroll` - Describes a scroll property change

star
: `z_star` - Describes the presence of a star hitnote (single lane).

moon
: `z_moon` - Describes the presence of a moon hitnote (multiple lane).

comet
: `z_comet` - Describes the presence of a comet hitnote (sound voltex ripoff `/j`)

