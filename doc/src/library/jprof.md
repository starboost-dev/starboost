# [**jprof**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/library/jprof.md)
[`library`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/library.md) From the [README file for jprof](https://github.com/pfirsich/jprof/blob/master/README.md):

Usually Lua programs are profiled by setting hooks using the built-in function `debug.sethook`, but sadly these hooks are not reliably called in luajit, which makes most profiling libraries for Lua not usable in the current version of löve.

jprof is a semi-makeshift solution for profiling löve applications with some extra work, but while also providing no significant slowdown while profiling.

---
[Documentation](https://github.com/pfirsich/jprof/blob/master/README.md)  
[GitHub](https://github.com/pfirsich/jprof)  
[License](https://github.com/pfirsich/jprof/blob/master/LICENSE)  