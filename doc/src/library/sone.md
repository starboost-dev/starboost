# [**sone**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/library/sone.md)
[`library`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/library.md)  From the LÖVE2D [forum page for sone](https://love2d.org/forums/viewtopic.php?t=82944):

Sone is a sound processing library for LÖVE that allows you to add effects like lowpass or EQ to your sounds. It lets you modify sound effects in real time. 

---
[Documentation](https://camchenry.github.io/sone/)  
[GitHub](https://github.com/camchenry/sone)  
[License](https://github.com/camchenry/sone/blob/master/LICENSE)  