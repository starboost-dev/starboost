# [**utf8**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/library/utf8.md)
[`library`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/library.md) From the Lua 5.3 [reference manual §6.5](https://www.lua.org/manual/5.3/manual.html#6.5):

This library provides basic support for UTF-8 encoding. It provides all its functions inside the table utf8. This library does not provide any support for Unicode other than the handling of the encoding. Any operation that needs the meaning of a character, such as character classification, is outside its scope. 
