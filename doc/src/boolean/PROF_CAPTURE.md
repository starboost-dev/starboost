# [**PROF_CAPTURE**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/boolean/PROF_CAPTURE.md) 
[`boolean`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/boolean.md) Whether the game will profile the code or not. This should always be false in every release.

Part of the [**jprof**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/library/jprof.md) library.

---
constant
: false
