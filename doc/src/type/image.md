# [`image`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/image.md) 
From the LÖVE2D [wiki about Image](https://love2d.org/wiki/Image):

Drawable image type.

