# [`boolean`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/boolean.md) 
From the Lua 5.1 [reference manual §2.2](http://www.lua.org/manual/5.1/manual.html#2.2):

Boolean is the type of the values false and true. Both [`nil`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/nil.md) and `false` make a condition false; *any* other value makes it true. 