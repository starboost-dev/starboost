# [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) 
From the Lua 5.1 [reference manual §2.2](http://www.lua.org/manual/5.1/manual.html#2.2):

Number represents real (double-precision floating-point) numbers. 

