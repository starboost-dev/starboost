# [`color`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/color.md) 
It's just an array for RGB values. 

The possible values for each component goes from 0 to 1.

