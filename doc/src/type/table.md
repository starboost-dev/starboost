# [`table`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/table.md) 
From the Lua 5.1 [reference manual §2.2](http://www.lua.org/manual/5.1/manual.html#2.2):

The type table implements associative arrays, that is, arrays that can be indexed not only with numbers, but with any value (except nil). Tables can be heterogeneous; that is, they can contain values of all types (except nil). Tables are the sole data structuring mechanism in Lua; they can be used to represent ordinary arrays, symbol tables, sets, records, graphs, trees, etc. To represent records, Lua uses the field name as an index. The language supports this representation by providing a.name as syntactic sugar for a["name"]. There are several convenient ways to create tables in Lua (see §2.5.7). 

