# [`font`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/font.md) 
From the LÖVE2D [wiki about Font](https://love2d.org/wiki/Font):

Defines the shape of characters that can be drawn onto the screen. 

