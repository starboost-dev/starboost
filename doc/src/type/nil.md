# [`nil`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/nil.md) 
From the Lua 5.1 [reference manual §2.2](http://www.lua.org/manual/5.1/manual.html#2.2):

Nil is the type of the value nil, whose main property is to be different from any other value; it usually represents the absence of a useful value. 

