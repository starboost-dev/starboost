# [`string`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/string.md) 
From the Lua 5.1 [reference manual §2.2](http://www.lua.org/manual/5.1/manual.html#2.2)

String represents arrays of characters. Lua is 8-bit clean: strings can contain any 8-bit character, including embedded zeros ('\0') (see [§2.1](http://www.lua.org/manual/5.1/manual.html#2.1)). 

