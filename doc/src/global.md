# Global scope

## Constants
Set once, expected not to change.
#### main.lua
||||
|-|-|-|
| [`boolean`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/boolean.md) | [**PROF_CAPTURE**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/boolean/PROF_CAPTURE.md) | Enable or disable profiling. |
| [`color`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/color.md) | [**c_bg**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/color/c_bg.md) | Background color, used for non-image background color ingame. |
| [`color`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/color.md) | [**c_main**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/color/c_main.md) | Main color, used for default gameplay element color. |
| [`color`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/color.md) | [**c_secondary**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/color/c_secondary.md) | Secondary color. |
| [`enum`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/enum.md) | [**game_state_cases**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/enum/game_state_cases.md) | All the posible values for `game_state`. |
| [`font`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/font.md) | [**f_game**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/font/f_game.md) | Lame default font. |
| [`library`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/library.md) | [**jprof**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/library/jprof.md) | Profiling manager. |
| [`library`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/library.md) | [**sone**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/library/sone.md) | Sound processing. |
| [`library`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/library.md) | [**utf8**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/library/utf8.md) | utf8 support. |
| [`string`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/string.md) | [**game_identity**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/string/game_identity.md) | Name of the game folder. | 
| [`string`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/string.md) | [**game_song_folder**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/string/game_song_folder.md) | The path of the songs folder. |
| [`table`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/table.md) | [**img**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/table/img.md) | Holds all the preloaded images. |
| [`table`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/table.md) | [**object**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/table/object.md) | All the ingame objects. |

## Variables
#### main.lua
||||
|-|-|-|
| [`boolean`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/boolean.md) | [**game_input**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/boolean/game_input.md) | Whether the game is expecting text input or not. |
| [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) | [**game_audio_offset**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/number/game_audio_offset.md) | Global audio offset. *Non-functional.* |
| [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) | [**game_background_dim**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/number/game_background_dim.md) | The amount of dim to apply to the backgrounds during gameplay. |
| [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) | [**game_height**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/number/game_height.md) | Height of the game window. |
| [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) | [**game_render**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/number/game_render.md) | Render distance within a chart. Used for optimization. |
| [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) | [**game_width**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/number/game_width.md) | Width of the game window. |
| [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) | [**mousewheelinput**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/number/mousewheelinput.md) | Every frame it updates to any vertical usage of the mouse wheel. |
| [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) | [**scroll_speed**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/number/scroll_speed.md) | |
| [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) | [**song_beat**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/number/song_beat.md) | Unsure if the `song_` variables are currently in use.  |
| [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) | [**song_bpm**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/number/song_bpm.md) | |
| [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) | [**song_count_beat**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/number/song_count_beat.md) | |
| [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) | [**song_count_measure**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/number/song_count_measure.md) | |
| [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) | [**song_delay**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/number/song_delay.md) | |
| [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) | [**song_duration**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/number/song_duration.md) | |
| [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) | [**song_sample**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/number/song_sample.md) | |
| [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) | [**song_samplecount**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/number/song_samplecount.md) | |
| [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) | [**song_samplerate**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/number/song_samplerate.md) | |
| [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) | [**song_speed**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/number/song_speed.md) | |
| [`number`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/number.md) | [**song_time**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/number/song_time.md) | |
| [`string`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/string.md) | [**debug_text**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/string/debug_text.md) | Text used to print debug stuff. |
| [`string`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/string.md) | [**game_state**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/string/game_state.md) | Current game state. |
| [`string`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/string.md) | [**text_input**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/string/text_input.md) | Every frame it updates to any text input. |
#### locale.lua
||||
|-|-|-|-|
| [`table`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/table.md) | [**text_input**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/string/text_input.md) | Contains all the strings for the current locale. |

