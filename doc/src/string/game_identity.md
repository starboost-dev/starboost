# [**game_identity**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/string/game_identity.md) 
[`string`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/string.md) The default background color. Defines the folder the game data is stored in.

---
constant
: `starboost`
