# [**game_song_folder**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/string/game_song_folder.md)
[`string`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/string.md) The path of the folder songs are stored in.

---
constant
: `songs/`
