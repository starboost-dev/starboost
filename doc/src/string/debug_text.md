# [**debug_text**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/string/debug_text.md) 
[`string`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/string.md) This variable acts a buffer for any debug text. It's usage is variable, and it's intended to only be used while debugging. 

---
variable
: Whatever is needed to debug.
