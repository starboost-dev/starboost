# [**text_input**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/string/text_input.md) 
[`string`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/string.md) This variable acts as buffer for the game to process projected text. This varaible is used in consort with [**game_input**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/boolean/game_input.md).

---
variable
: Whatever text has been typed in by the user.
