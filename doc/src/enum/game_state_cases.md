# [**game_state_cases**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/enum/game_state_cases.md) 
[`enum`](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/type/enum.md) It stores the different states or phases the game can be. Utilized alongside [**game_state**](https://codeberg.org/starboost-dev/starboost/src/branch/main/doc/src/string/game_state.md).

---
### Constants

ingame
: The state of the game where players can play charts.

editor
: The state of the game where players can make charts.

song_select
: The state of the game where players can select songs to play or edit.
