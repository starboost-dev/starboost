
snap_ratio = 1
note_size = nil
note_type = nil

local snap_case = 1
local snap_cases = {
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    12,
    16,
    20,
    24,
    32,
    48,
    64,
    96,
    192
}

local _function = ""
local comet_function = 1
local comet_function_cases = table.enum {
    "normal",
    "line",
    "hold"
}
local comet_style = 1
local comet_style_cases = table.enum {
    "straight",
    "bezier_curve_bottom",
    "bezier_curve_top"
}

function editorChangeCometFunction()
    comet_function = comet_function + 1
    if comet_function > #comet_function_cases then comet_function = comet_function - #comet_function_cases end
    setUIMessage("Comet Function: ".. comet_function_cases[comet_function])   
end

function editorChangeCometStyle()
    comet_style = comet_style + 1
    if comet_style > #comet_style_cases then comet_style = comet_style - #comet_style_cases end
    setUIMessage("Comet Style: ".. comet_style_cases[comet_style])   
end

function editorPlaceComet1(_pos)
    local _x = _pos[1]
    local _y = _pos[2]

    local chart = LoadedChartData
    local _speed = scroll_speed
    local _snap_ratio = snap_ratio

    local _offset_x = game_width/2-image.playfield.stage_sides:getWidth()/2
    local _offset_y = 0

    local _lane_count = 5
    local _stage_w = lane_w * _lane_count

    local _ms = math.floor(song_time - song_delay + (stage_h-_y) * 1000/stage_h * 1/_speed)
    local _snapped_ms = snapToBeat(_ms, _snap_ratio)

    _column = getColumn(_x)

    return _snapped_ms, _column
end

function editorPlaceComet2(_pos, _ms1, _column1)
    local _x = _pos[1]
    local _y = _pos[2]

    local chart = LoadedChartData
    local _speed = scroll_speed
    local _snap_ratio = snap_ratio

    local _offset_x = game_width/2-image.playfield.stage_sides:getWidth()/2
    local _offset_y = 0

    local _lane_count = 5
    local _stage_w = lane_w * _lane_count

    local _ms = math.floor(song_time - song_delay + (stage_h-_y) * 1000/stage_h * 1/_speed)
    local _ms2 = snapToBeat(_ms, _snap_ratio)
    
    local _column2 = 0
    
    local _column2 = getColumn(_x)
    local _data = {
        ms = _ms1, 
        command = object.comet .. 
        " " .. _ms2-_ms1 .. 
        " " .. _column1 ..
         " ".. _column2 .. 
         " " .. comet_function_cases[comet_function] .. 
         " " .. comet_style_cases[comet_style]
    }
    
    if _ms2-_ms1 >= 0 then 
        table.insert(LoadedChartData.Map, _data) 
        updateChartProperties()
        optimizeIngameMap(chart)
    end
end

function editorPlaceObject(_pos, _type)        
-- places a note in a position, snapped accordingly
    local _placed = false
    local _type = _type

    local _x = _pos[1]
    local _y = _pos[2]

    local chart = LoadedChartData
    local _speed = scroll_speed
    local _snap_ratio = snap_ratio

    local _offset_x = game_width/2-image.playfield.stage_sides:getWidth()/2
    local _offset_y = 0

    local _lane_w = 72
    local _lane_count = 5
    local _stage_w = _lane_w * _lane_count

    local _ms = math.floor(song_time - song_delay + (stage_h-_y) * 1000/stage_h * 1/_speed)
    local _snapped_ms = snapToBeat(_ms, _snap_ratio)

    -- keep inside playfield for stars and moons
    if (_type == object.star
    or _type == object.moon) then
        if _x > _offset_x
        and _x < _offset_x + _stage_w
        and _y > _offset_y 
        and _y < _offset_y + stage_h then

            local _width = note_size
            local _lane = 1 + math.floor((_x - _offset_x) / _lane_w)

            while _lane-1+_width > 5 do
                _lane = _lane -1
            end

            local _data = {ms = _snapped_ms, command = _type .. " " .. _lane .. " ".. _width}
        
            if not isHitOccupied(_snapped_ms,_lane) then 
                table.insert(LoadedChartData.Map, _data) 
                _placed = true
            end
        end    
    elseif _type == object.background
    or _type == object.beat
    or _type == object.bpm
    or _type == object.color
    or _type == object.scroll 
    or _type == object.spin 
    or _type == object.foreground then
        if _x > _offset_x
        and _x < _offset_x + _stage_w
        and _y > _offset_y 
        and _y < _offset_y + stage_h then
            local _data = {ms = _snapped_ms}
            if _type == object.background then
                _data.command = _type .. " none"
            elseif _type == object.foreground then
                _data.command = _type .. " none"
            elseif _type == object.beat then
                _data.command = _type .. " " .. song_beat
            elseif _type == object.bpm then
                _data.command = _type .. " " .. song_bpm
            elseif _type == object.color then
                _data.command = _type .. " all 0 0 0"
            elseif _type == object.scroll
                or _type == object.spin then
                _data.command = _type .. " all 1"
            end
            table.insert(LoadedChartData.Map, _data)
            _placed = true
        end
    else 
        return
    end
    if _placed then 
        updateChartProperties()
        optimizeIngameMap(chart)
    end
end
 
function editorRemoveComet(_pos)
    local _x = _pos[1]
    local _y = _pos[2]

    local chart = LoadedChartData
    local _speed = scroll_speed
    local _snap_ratio = snap_ratio

    local _offset_x = game_width/2-image.playfield.stage_sides:getWidth()/2
    local _offset_y = 0

    local _lane_count = 5
    local _stage_w = lane_w * _lane_count

    local _ms = math.floor(song_time - song_delay + (stage_h-_y) * 1000/stage_h * 1/_speed)
    local _snapped_ms = snapToBeat(_ms, _snap_ratio)

    _column = getColumn(_x)

    local _note = isCometOccupied(_snapped_ms, _column)
    if _note then table.remove(LoadedChartData.Map, _note) end
    optimizeIngameMap(chart)
end

function editorRemoveObject(_pos)
-- removes a note in a position, snapped accordingly
    local _x = _pos[1]
    local _y = _pos[2]

    local _lane_w = 72
    local _lane_count = 5

    local _offset_x = 304
    local _offset_y = 0
    local _stage_w = _lane_w * _lane_count

    local chart = LoadedChartData

    -- keep inside playfield
    if  _x > _offset_x
    and _x < _offset_x + _stage_w
    and _y > _offset_y 
    and _y < _offset_y + stage_h then

        local _speed = scroll_speed
        local _snap_ratio = snap_ratio
        
        local _lane = 1 + math.floor((_x - _offset_x) / _lane_w)

        local _ms = math.floor(song_time - song_delay + (stage_h-_y) * 1000/stage_h * 1/_speed)
        local _snapped_ms = snapToBeat(_ms, _snap_ratio)
        
        local _note = isHitOccupied(_snapped_ms,_lane)
        if _note then table.remove(LoadedChartData.Map,_note) end
    end    
    optimizeIngameMap(chart)
end

function editorResizeMoon(_direction)
    note_size = note_size or 2

    if _direction == "up" then
        note_size = note_size + 1
    elseif _direction == "down" then
        note_size = note_size - 1
    end

    note_size = math.min(5,note_size)
    note_size = math.max(2,note_size)
    setUIMessage("Note size to".. note_size)   
end

function editorSetSnap(_direction)
    if _direction == "up" then
        snap_case = snap_case + 1
    elseif _direction == "down" then
        snap_case = snap_case - 1
    end

    snap_case = math.max(1,math.min(snap_case,#snap_cases))
    snap_ratio = 1/snap_cases[snap_case]
    setUIMessage("Note snap set to 1/".. snap_cases[snap_case].. ".")   
end