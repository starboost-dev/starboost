local song_speed_step = 0.05
local song_speed_max = 2

function editorLimitSongSpeed(v)
-- song speed limits
    local _factor = 1/song_speed_step
    song_speed = math.min(song_speed_max,math.max(song_speed_step,v))
    song_speed = math.floor(song_speed_step+(_factor*song_speed))/_factor
end

function editorSetSongSpeed(_direction)
    if _direction == "up" then
        editorLimitSongSpeed(song_speed + song_speed_step) 
    elseif _direction == "down" then
        editorLimitSongSpeed(song_speed - song_speed_step)
    end
    LoadedData.Song:setPitch(song_speed) 
    setUIMessage("Playback speed set to " .. song_speed .. ".")
end

function editorSetPosition(_direction)
    -- move the game view up or down
    local _ratio = snap_ratio
    local _beat_separation = 0.25 -- in seconds
    if song_bpm ~= 0 then
        _beat_separation = 1/(song_bpm/60)*1000 -- in seconds
    end
        
    local _t = song_time -- current pos to seconds

    -- move back by snap distance
    if _direction == "up" then
        _t = _t + _beat_separation * _ratio  + 1 -- move back by snap distance
    elseif _direction == "down" then
        _t = _t - 1 
    elseif _direction == "start" then 
        _t = 0
    end
    
    _t = snapToBeat(_t,snap_ratio) + LoadedData.SongData.Offset
    _t = math.max(0,math.min(_t,LoadedData.SongData.Duration))


    LoadedData.Song:seek(_t/1000,"seconds")
    LoadedData.Song:pause()
end

function editorReload()
    local _play = LoadedData.Song:isPlaying()
    LoadedData.Song:pause() 
    local _pos = LoadedData.Song:tell()
    
    editorRecalculateWaveform()

    LoadedData.Song:seek(_pos)
    if _play then LoadedData.Song:play() end
end

function editorPlayPause()
    if LoadedData.Song:isPlaying() then
        LoadedData.Song:pause() 
        setUIMessage("Song paused.")
    else 
        LoadedData.Song:play() 
        setUIMessage("Song playing.")
    end 
end