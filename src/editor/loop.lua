function editorLoop(_dt)
    playfieldInput(_dt) 
    updateSongData(_dt)
    updateUIMessage(_dt)
    editorInput(_dt)
end