local ui_message = ""
local ui_message_time = 1.5
local ui_message_timer = 0

function updateUIMessage(dt)
-- since UI messsage dissapears after some time, this counts that down
    ui_message_timer = ui_message_timer - dt
    if ui_message and ui_message_timer <= 0 then 
        ui_message = nil
    end
end

function setUIMessage(_message)
-- sets the User Interface message for the editor (shown in center top)
    ui_message = _message
    ui_message_timer = ui_message_time
end

function editorDrawUIMessage()
-- draws the UI message on editor screen
    local _preColor = pack(love.graphics.getColor())

    if ui_message then
        local _message = love.graphics.newText(f_game, ui_message)
        local _message_w =  _message:getWidth()
        local _message_h =  _message:getHeight()
        local _offset_x = game_width/2 - _message_w/2
        local _offset_y = 10
        local border = 3

        love.graphics.setLineWidth(1)
        
        love.graphics.setColor(unpack(c_bg))
        love.graphics.rectangle(
            "fill",
            _offset_x-border,
            _offset_y-border,
            _message_w+border*2,
            _message_h+border*2
        )
        love.graphics.setColor(unpack(c_main))
        love.graphics.rectangle(
            "line",
            _offset_x-border,
            _offset_y-border,
            _message_w+border*2,
            _message_h+border*2
        )
        
        love.graphics.setColor(1,1,1)
        love.graphics.draw(
            _message,
            _offset_x,
            _offset_y
        )
    end
    love.graphics.setColor(unpack(_preColor))
end
