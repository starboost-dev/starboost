function editorDraw()
    prof.push("background") 
    playfieldDrawBackground(c_bg)
    prof.pop("background")

    prof.push("beat glow") 
        playfieldDrawStageGlow() 
    prof.pop("beat glow")

    prof.push("waveform") 
        editorDrawWaveform()
    prof.pop("waveform")

    prof.push("lane glow") 
        gameplayDrawLaneGlow() 
    prof.pop("lane glow")

    prof.push("beats") 
        playfieldDrawBeats()
    prof.pop("beats")

    prof.push("stage sides") 
        playfieldDrawStageSides() 
    prof.pop("stage sides")

    prof.push("editor") 
        editorDrawMain() 
    prof.pop("editor")

    prof.push("snap position") 
        editorDrawSnapPosition(love.mouse.getPosition()) 
    prof.pop("snap position")
    
    prof.push("gameplay")
        gameplayDraw()
    prof.pop("gameplay")

    prof.push("snap ghost") 
        editorDrawSnapGhost(love.mouse.getPosition())
    prof.pop("snap ghost")

    prof.push("stage bottom") 
        playfieldDrawStageBottom()
    prof.pop("stage bottom")

    prof.push("overlay")
        playfieldDrawOverlay()
    prof.pop("overlay")

    prof.push("keybinds") 
        editorDrawKeybinds()
    prof.pop("keybinds")

    prof.push("ui message") 
        editorDrawUIMessage()
    prof.pop("ui message")

    prof.push("foreground") 
        playfieldDrawForegrounds() 
    prof.pop("foreground")
end