local render_distance = game_render

function editorDrawSnapPosition(_x, _y)        
    local _preColor = pack(love.graphics.getColor())

    local _lane_w = 72
    local _lane_count = 5

    local _type = note_type
    local _width = note_size

    local _speed = scroll_speed
    local _snap_ratio = snap_ratio
    local _relative_factor = stage_h/1000 * _speed    

    local _ms = math.floor(song_time - song_delay + (stage_h-_y) * 1000/stage_h * 1/_speed)
    local _snapped_ms, _snap_separation = snapToBeat(_ms, _snap_ratio)
    local _beat_ms = snapToBeat(_ms, 1)

    local _beat_duration = 1/(song_bpm/60)*1000

    local _offset_x = game_width/2 - image.playfield.stage_sides:getWidth()/2
    local _offset_y = 0

    love.graphics.setColor(0.5,0.5,0)
    for i=0, 1/_snap_ratio do
        local position = (song_time - song_delay - _beat_ms - i * _snap_separation)* _relative_factor
        if i == 1/_snap_ratio then break end
        love.graphics.line(
            _offset_x + 3,
            _offset_y + stage_h + position,
            _offset_x + image.playfield.stage_sides:getWidth() - 3,
            _offset_y + stage_h + position
        )
    end

    love.graphics.setColor(unpack(_preColor))
end

function editorDrawNotesJudges()
    for _, data in ipairs(LoadedChartData.Map) do
        -- get the ms 
        local _ms = data.ms
        
        local _speed = scroll_speed         
        if _ms > song_time + render_distance/_speed then break end

        local _command = data.command
        local _hit = data.hit
        local _next_element = string.gmatch(_command,"%s(%S+)")   

        if string.find(_command,object.star,1,4) then
            local _lane = _next_element()
            if not _hit then editorDrawNoteJudge(_ms, tonumber(_lane)) end
        end
        if string.find(_command,object.moon,1,4) then
            _lane = _next_element()
            _width = _next_element()
            if not _hit then editorDrawNoteJudge(_ms, tonumber(_lane),tonumber(_width)) end
        end
    end
end

function editorDrawNoteJudge(_ms,_lane,_width)
    local _preColor = pack(love.graphics.getColor())

    local _lane = _lane or 1
    local _width = _width or 1

    local _lane_w = 73 --exception!!! (go figure)
    local _speed = scroll_speed
    local _relative_factor = stage_h/1000 * _speed    

    local _offset_x = game_width/2 - image.playfield.stage_sides:getWidth()/2 + 2
    local _offset_y = stage_h

    for i=1, #judge_coloring do
        local _position = (song_time - _ms - judge_coloring[i].ms - song_delay) * _relative_factor - 1
        local _color = judge_coloring[i].color
        _color[4] = 0.3

        love.graphics.setColor(unpack(_color))
        love.graphics.rectangle(
            "fill",
            _offset_x + (_lane-1) * _lane_w,
            _offset_y + _position,
            _lane_w * _width - 1,
            judge_coloring[i].ms*2*_relative_factor
        )
    end

    love.graphics.setColor(unpack(_preColor))
end

function editorDrawMain()
    local _current_ms = -1
    local _flag_offset = 0

    editorDrawNotesJudges()
    editorDrawSnapComet1()
    
    for i=1, #LoadedChartData.Map do
        if LoadedChartData.Map[i] then
            local data = LoadedChartData.Map[i] 
            -- get the ms 

            local _ms = data.ms

            local _speed = scroll_speed         
            if _ms > song_time + render_distance/_speed then break end
            
            if _ms > _current_ms then 
                _current_ms = _ms
                _flag_offset = 0
            end

            local _command = data.command
            local _hit = data.hit
            local _value = string.match(_command,"%s(.+)")  
            
            if string.match(_command,object.beat) then
                local _valuecheck = _value
            
                local _offset, _isModifying, _value = editorDrawFlag(_ms, _valuecheck, "BEAT: ",_flag_offset, data.modifying)

                data.modifying = _isModifying
                _flag_offset = _flag_offset + _offset

                if _value == "" then 
                    table.remove(LoadedChartData.Map,i)
                elseif _value ~= _valuecheck then
                    data.command = object.beat .. " " .. _value
                    LoadedChartData.Beats = getChartMeasures(LoadedChartData)
                end
            elseif string.match(_command,object.bpm) then
                local _valuecheck = _value
            
                local _offset, _isModifying, _value = editorDrawFlag(_ms, _valuecheck, "BPM: ",_flag_offset, data.modifying)

                data.modifying = _isModifying
                _flag_offset = _flag_offset + _offset

                if _value == "" then 
                    table.remove(LoadedChartData.Map,i)
                elseif _value ~= _valuecheck then
                    data.command = object.bpm .. " " .. _value
                    LoadedChartData.Beats = getChartMeasures(LoadedChartData)
                end
            elseif string.match(_command,object.spin) then
                local _valuecheck = _value
            
                local _offset, _isModifying, _value = editorDrawFlag(_ms, _valuecheck, "SPIN: ",_flag_offset, data.modifying)

                data.modifying = _isModifying
                _flag_offset = _flag_offset + _offset

                if _value == "" then 
                    table.remove(LoadedChartData.Map,i)
                elseif _value ~= _valuecheck then
                    data.command = object.spin .. " " .. _value
                end
            elseif string.match(_command,object.scroll) then
                local _valuecheck = _value
                
                local _offset, _isModifying, _value = editorDrawFlag(_ms, _valuecheck, "VELOCITY: ",_flag_offset, data.modifying)

                data.modifying = _isModifying
                _flag_offset = _flag_offset + _offset

                if _value == "" then 
                    table.remove(LoadedChartData.Map,i)
                elseif _value ~= _valuecheck then
                    data.command = object.scroll .. " " .. _value
                end
            elseif string.match(_command,object.color) then
                local _valuecheck = _value
                
                local _offset, _isModifying, _value = editorDrawFlag(_ms, _valuecheck, "COLOR: ",_flag_offset, data.modifying)

                data.modifying = _isModifying
                _flag_offset = _flag_offset + _offset

                if _value == "" then 
                    table.remove(LoadedChartData.Map,i)
                elseif _value ~= _valuecheck then
                    data.command = object.color .. " " .. _value
                end
            elseif string.match(_command,object.background) then
                local _valuecheck = _value
                
                local _offset, _isModifying, _value = editorDrawFlag(_ms, _valuecheck, "BG: ",_flag_offset, data.modifying)

                data.modifying = _isModifying
                _flag_offset = _flag_offset + _offset

                if _value == "" then 
                    table.remove(LoadedChartData.Map,i)
                elseif _value ~= _valuecheck then
                    data.command = object.background .. " " .. _value
                end
            elseif string.match(_command,object.foreground) then
                local _valuecheck = _value
                
                local _offset, _isModifying, _value = editorDrawFlag(_ms, _valuecheck, "FG: ",_flag_offset, data.modifying)

                data.modifying = _isModifying
                _flag_offset = _flag_offset + _offset

                if _value == "" then 
                    table.remove(LoadedChartData.Map,i)
                elseif _value ~= _valuecheck then
                    data.command = object.foreground .. " " .. _value
                end
            end 
        end
    end 
end

function editorDrawFlag(_ms, _value, _text, _offset, _modif)
    
    love.graphics.setLineWidth(1)

    local _preColor = pack(love.graphics.getColor())
    local _modifying = _modif or false
    local _text = love.graphics.newText(f_game, {{1,1,0}, _text})

    local _value_to_draw = _value
    if _modif then 
        _modifying = true 
        _value_to_draw = textinput
    end

    local _value_text = love.graphics.newText(f_game, _value_to_draw) 
    local _text_w =  _text:getWidth()
    local _w =  _text_w + _value_text:getWidth()
    local _h =  _text:getHeight()
    local _border = 3
    
    local _mouse_x, _mouse_y = love.mouse.getPosition()

    local _stage_ox = 300
    local _offset_x = _stage_ox - (_w + _border * 2) - 1 + _offset
    local _offset_y = stage_h
    local _lane_w = 72
    local _speed = scroll_speed
    local _relative_factor = stage_h/1000 * _speed    
    local _position = (song_time - _ms) * _relative_factor
        
    love.graphics.setColor(unpack(c_bg))
    love.graphics.rectangle(
        "fill",
        _offset_x - _border,
        _offset_y - _border + _position,
        _w + _border * 2,
        _h + _border * 2
    )
    love.graphics.setColor(unpack(c_main))
    love.graphics.rectangle(
        "line",
        _offset_x - _border,
        _offset_y - _border + _position,
        _w + _border * 2,
        _h + _border * 2
    )
    
    love.graphics.setColor(1,1,1)
    love.graphics.draw(
        _text,
        _offset_x,
        _offset_y + _position
    )

    
    if _modifying then 
        love.graphics.setColor(1,1,0)

        if love.keyboard.isScancodeDown("escape") then 
            _modifying = false
            game_input = false
            love.graphics.setColor(1,0,0)
        elseif love.keyboard.isScancodeDown("return") then 
            _modifying = false
            game_input = false
            love.graphics.setColor(1,0,0)
            _value = textinput
        end
    end

    if  _mouse_x > _offset_x + _text_w
    and _mouse_x < _offset_x + _w 
    and _mouse_y > _offset_y + _position 
    and _mouse_y < _offset_y + _position + _h  then    
        
        love.graphics.setColor(1,1,0)

        if love.mouse.isDown(1) then

            for _, data in ipairs(LoadedChartData.Map) do
                data.modifying = false    
            end
            _modifying = true
            game_input = true
            textinput = _value
            love.graphics.setColor(0,1,0)
        end
    end

    love.graphics.draw(
        _value_text,
        _offset_x + _text_w,
        _offset_y + _position
    )

    love.graphics.setColor(unpack(_preColor))
    
    return -(_w + _border * 4), _modifying, _value
end
