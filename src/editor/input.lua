local placing_comet = false
local object_placed = false
local comet_column = nil
local comet_ms = nil

local render_distance = game_render

function editorInput(_dt)
-- all editor inputs
    if LoadedData.Song and not game_input then
        if love.mouse.isDown(1) then
            if not object_placed then
                if object_type == object.comet then 
                    if placing_comet then 
                        editorPlaceComet2(pack(love.mouse.getPosition()), comet_ms, comet_column)
                        placing_comet = false
                        comet_column = nil
                        comet_ms = nil
                    else
                        comet_ms, comet_column = editorPlaceComet1(pack(love.mouse.getPosition()))
                        placing_comet = true
                    end
                else
                    editorPlaceObject(pack(love.mouse.getPosition()), object_type)
                end
            end
            object_placed = true
        else
            object_placed = false
        end

        if love.mouse.isDown(2) then 
            if placing_comet then
                placing_comet = false
                comet_column = nil
                codmet_ms = nil
            else
                if object_type == object.comet then 
                    editorRemoveComet(pack(love.mouse.getPosition()))
                elseif object_type == object.star
                or object_type == object.moon then 
                    editorRemoveObject(pack(love.mouse.getPosition())) 
                end
            end
        end

        if Keybind.Press(Keybind.editor.placeTypeStar) then
            object_type = object.star
            note_size = 1
        end
        if Keybind.Press(Keybind.editor.placeTypeMoon) then
            object_type = object.moon 
            note_size = 2
        end
        if Keybind.Press(Keybind.editor.placeTypeComet) then 
            object_type = object.comet 
            note_size = nil
        end
        if Keybind.Press(Keybind.editor.placeTypeBPM) then 
            object_type = object.bpm 
            note_size = nil 
        end
        if Keybind.Press(Keybind.editor.placeTypeBeat) then
            object_type = object.beat 
            note_size = nil
        end
        if Keybind.Press(Keybind.editor.placeTypeColor) then
            object_type = object.color 
            note_size = nil 
        end
        if Keybind.Press(Keybind.editor.placeTypeScroll) then 
            object_type = object.scroll 
            note_size = nil 
        end
        if Keybind.Press(Keybind.editor.placeTypeSpin) then 
            object_type = object.spin 
            note_size = nil 
        end
        if Keybind.Press(Keybind.editor.placeTypeBackground) then 
            object_type = object.background 
            note_size = nil 
        end
        if Keybind.Press(Keybind.editor.placeTypeForeground) then 
            object_type = object.foreground 
            note_size = nil 
        end
        
        if Keybind.Press(Keybind.editor.quickload) then editorReload() end
        
        if Keybind.Down_delayed(Keybind.editor.objectModify1, 0.1, _dt) then 
            if object_type == object.moon then editorResizeMoon("up") end
            if object_type == object.comet then editorChangeCometStyle() end
        
        end
        if Keybind.Down_delayed(Keybind.editor.objectModify2, 0.1, _dt) then 
            if object_type == object.moon then editorResizeMoon("down") end
            if object_type == object.comet then editorChangeCometFunction() end
        end
        

        if Keybind.Down_delayed(Keybind.editor.positionUp, 0.15, _dt) then editorSetPosition("up") end
        if Keybind.Down_delayed(Keybind.editor.positionDown, 0.15, _dt) then editorSetPosition("down") end
        if mousewheelinput > 0 then
            mousewheelinput = 0 
            editorSetPosition("up")
        end
        if mousewheelinput < 0 then 
            mousewheelinput = 0
            editorSetPosition("down") 
        end

        if Keybind.Press(Keybind.editor.positionStart, _dt) then editorSetPosition("start") end
        
        if Keybind.Down_delayed(Keybind.editor.snapIncrease, 0.1, _dt) then editorSetSnap("up") end
        if Keybind.Down_delayed(Keybind.editor.snapDecrease, 0.1, _dt) then editorSetSnap("down") end

        if Keybind.Down_delayed(Keybind.editor.speedIncrease, 0.1, _dt) then editorSetSongSpeed("up") end
        if Keybind.Down_delayed(Keybind.editor.speedDecrease, 0.1, _dt) then editorSetSongSpeed("down") end

        if Keybind.Down_delayed(Keybind.editor.waveformToggle, 0.1, _dt) then editorToggleWaveformCase() end 
        
        if Keybind.Press(Keybind.editor.waveformQualityToggle) then editorToggleWaveformQuality() end
        if Keybind.Down_delayed(Keybind.editor.waveformQualityUp, 0.1, _dt) then editorSetWaveformQuality("up") end
        if Keybind.Down_delayed(Keybind.editor.waveformQualityDown, 0.1, _dt) then editorSetWaveformQuality("down") end

        if Keybind.Press(Keybind.editor.waveformStrengthDefault) then editorSetWaveformStrength("default") end
        if Keybind.Down_delayed(Keybind.editor.waveformStrengthUp, 0.1, _dt) then editorSetWaveformStrength("up") end
        if Keybind.Down_delayed(Keybind.editor.waveformStrengthDown, 0.1, _dt) then editorSetWaveformStrength("down") end

        if Keybind.Press(Keybind.editor.save) then
            local _, _reply = SaveSong()
            if not _reply then _reply = "Save succesful." end
            setUIMessage(_reply)
        end

        if Keybind.Press(Keybind.editor.playPause) then editorPlayPause() end
        editorRecalculateWaveform()
    end
end

function editorDrawKeybinds()
-- presents the keybinds to the screen in editor mode, and highlights them in yellow if pressed
    local _offset_x = 675
    local _offset_y = 75
    local _print = {}

    table.insert_batch(_print, {1,1,1}, "debug: " .. debug_text) 
    table.insert_batch(_print, {1,1,1}, "KEYBINDS" .. "\n")
    table.insert_batch(_print, {1,1,1}, "\n")
    table.insert_batch(_print, {1,1,1}, "Waveform" .. "\n") 
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.waveformToggle))
    table.insert_batch(_print, {1,1,1}, " | Toggle On/Off\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.waveformStrengthUp))
    table.insert_batch(_print, {1,1,1}, "/")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.waveformStrengthDown))
    table.insert_batch(_print, {1,1,1}, "/")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.waveformStrengthDefault))
    table.insert_batch(_print, {1,1,1}, " | Intensity Up/Down/Default\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.waveformQualityToggle))
    table.insert_batch(_print, {1,1,1}, " | Quality Mode Toggle\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.waveformQualityUp))
    table.insert_batch(_print, {1,1,1}, "/")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.waveformQualityDown))
    table.insert_batch(_print, {1,1,1}, " | Quality Up/Down\n")
    table.insert_batch(_print, {1,1,1}, "\n")
    table.insert_batch(_print, {1,1,1}, "Song Control" .. "\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.playPause))
    table.insert_batch(_print, {1,1,1}, " | Pause/Unpause\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.positionUp))
    table.insert_batch(_print, {1,1,1}, "/")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.positionDown))
    table.insert_batch(_print, {1,1,1}, "/")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.positionStart))
    table.insert_batch(_print, {1,1,1}, " | Move Up/Down/Start\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.speedIncrease))
    table.insert_batch(_print, {1,1,1}, "/")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.speedDecrease))
    table.insert_batch(_print, {1,1,1}, " | Song Speed Up/Down\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.playfield.zoomIn))
    table.insert_batch(_print, {1,1,1}, "/")
    table.insert_batch(_print, Keybind.GASP(Keybind.playfield.zoomOut))
    table.insert_batch(_print, {1,1,1}, " | Playfield Zoom In/Out\n")
    table.insert_batch(_print, {1,1,1}, "\n")
    table.insert_batch(_print, {1,1,1}, "Chart Control" .. "\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.save))
    table.insert_batch(_print, {1,1,1}, " | Save    \n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.undo))
    table.insert_batch(_print, {1,1,1}, "/")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.redo))
    table.insert_batch(_print, {1,1,1}, " | Undo/Redo\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.snapIncrease))
    table.insert_batch(_print, {1,1,1}, "/")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.snapDecrease))
    table.insert_batch(_print, {1,1,1}, " | Snap Increase/Decrease\n")

    table.insert_batch(_print, {1,1,1}, "\n")
    table.insert_batch(_print, {1,1,1}, "Input Control" .. "\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.placeTypeStar))
    table.insert_batch(_print, {1,1,1}, " | Place Star note\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.placeTypeMoon))
    table.insert_batch(_print, {1,1,1}, " | Place Moon note\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.placeTypeComet))
    table.insert_batch(_print, {1,1,1}, " | Place Comet Note\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.placeTypeBPM))
    table.insert_batch(_print, {1,1,1}, " | Place BPM change\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.placeTypeBeat))
    table.insert_batch(_print, {1,1,1}, " | Place Beat change\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.placeTypeColor))
    table.insert_batch(_print, {1,1,1}, " | Place Note Color change\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.placeTypeSpin))
    table.insert_batch(_print, {1,1,1}, " | Place Spin Speed change\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.placeTypeScroll))
    table.insert_batch(_print, {1,1,1}, " | Place Scroll Speed change\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.placeTypeBackground))
    table.insert_batch(_print, {1,1,1}, " | Place Background change\n")
    table.insert_batch(_print, Keybind.GASP(Keybind.editor.placeTypeForeground))
    table.insert_batch(_print, {1,1,1}, " | Place Foreground change\n")

    table.insert_batch(_print, {1,1,1}, "\n")
    if object_type == object.moon
    or  object_type == object.comet then
        table.insert_batch(_print, Keybind.GASP(Keybind.editor.objectModify1))
        table.insert_batch(_print, {1,1,1}, "/")
        table.insert_batch(_print, Keybind.GASP(Keybind.editor.objectModify2))
        if  object_type == object.moon then
            table.insert_batch(_print, {1,1,1}, " | Resize Moon Size Down/Up\n")
        end
        if  object_type == object.comet then
            table.insert_batch(_print, {1,1,1}, " | Change comet Style/Function\n")
        end
    end

    love.graphics.print(
        _print,
        f_game,
        _offset_x,
        _offset_y
    )
end

function editorDrawSnapComet1(_column, _ms)

    local _column = _column or comet_column
    local _ms = _ms or comet_ms

    if _column and _ms then 
        local _preColor = pack(love.graphics.getColor())

        local _from = comet_column

        local _start_offset_x = image.gameplay.comet_border1:getWidth()
        local _start_offset_y = image.gameplay.comet_border1:getHeight()

        local _offset_x = game_width/2
        local _offset_y = stage_h
        local _lane_w = 72
        local _column_w = _lane_w/2
        
        local _speed = scroll_speed
        local _relative_factor = stage_h/1000 * _speed  

        local _position = (song_time - song_delay - _ms)* _relative_factor

        love.graphics.setColor(1,1,1,1)

        love.graphics.draw(
            image.gameplay.comet_border1,
            _offset_x + _column_w * _column,
            _offset_y + _position ,
            0,
            1, 1,   
            _start_offset_x/2, _start_offset_y/2
        )

        love.graphics.setColor(unpack(_preColor))
    end
end

function editorDrawSnapGhost(_x, _y)
    local _preColor = pack(love.graphics.getColor())
    
    local _lane_w = 72
    local _lane_count = 5

    local _type = object_type
    local _width = note_size

    local _speed = scroll_speed
    local _snap_ratio = snap_ratio
    local _relative_factor = stage_h/1000 * _speed

    local _ms = math.floor(song_time - song_delay + (stage_h-_y) * 1000/stage_h * 1/_speed)
    local _snapped_ms, _snap_separation = snapToBeat(_ms, _snap_ratio)

    local _offset_x = game_width/2 - image.playfield.stage_sides:getWidth()/2 + 2
    local _offset_y = 0

    if _type == object.comet then 
        editorDrawSnapComet1(getColumn(_x), _snapped_ms)
        if placing_comet then 
            gameplayDrawCometLine(comet_ms, _snapped_ms-comet_ms, comet_column, getColumn(_x), {color = {0,0,0,0}})
        end
    else
        if  _x > _offset_x
        and _x < _offset_x + stage_w
        and _y > _offset_y 
        and _y < _offset_y + stage_h then
    
        local _note_offset_y = image.editor.star_ghost:getHeight()
        local _lane = 1 + math.floor((_x - _offset_x) / _lane_w)

        local _position = (song_time - song_delay - _snapped_ms)* _relative_factor

        if _type == "z_star" then 
            love.graphics.setColor(1,1,1)
            love.graphics.draw(
                image.editor.star_ghost,
                _offset_x + (_lane-1) * _lane_w + (_lane-1),
                _offset_y - _note_offset_y + stage_h + _position
            )
        elseif _type == "z_moon" then 
                -- keep inside playfield
                while _lane-1+_width > 5 do
                    _lane = _lane -1
                end
                love.graphics.setColor(1,1,1)
                love.graphics.draw(
                    image.gameplay.moon_border1,
                    _offset_x + (_lane-1) * _lane_w + _width/2 + (_lane-1),
                    _offset_y - _note_offset_y + stage_h + _position
                )          
                for i=1, _width-2 do
                    love.graphics.setColor(1,1,1,1)
                    love.graphics.draw(
                        image.gameplay.moon_border2,
                        _offset_x + (_lane+i-1) * _lane_w + _width/2 + (_lane-1),
                        _offset_y - _note_offset_y + stage_h + _position
                    )
                end
                love.graphics.draw(
                    image.gameplay.moon_border3,
                    _offset_x + (_lane+_width-2) * _lane_w + _width/2 + (_lane-1),
                    _offset_y - _note_offset_y + stage_h + _position
                )
            end
        end
    end
    love.graphics.setColor(unpack(_preColor))
end