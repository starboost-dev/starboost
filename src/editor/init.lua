image.editor = {}
image.editor.star_ghost = love.graphics.newImage("res/image/star_ghost.png")

require "src/editor.input"
require "src/editor.objects"
require "src/editor.render"
require "src/editor.song"
require "src/editor.ui"
require "src/editor.waveform"
require "src/editor.loop"
require "src/editor.draw"

Keybind.editor = {
    lockSetting = { input = ";"},
    waveformToggle = { input = "w"}, -- toggle between no waveform/normal/lowpass/hipass/normal+lowpass/normal+hipass
    waveformStrengthDown = { input = "q"},  -- adjust intenstiy of filter
    waveformStrengthUp = { input = "e"}, 
    waveformStrengthDefault = { input = "r"}, 
    waveformQualityToggle = { input = "f"},  -- adjust quality of filter
    waveformQualityDown = { input = "a"},
    waveformQualityUp = { input = "d"}, 
    playPause = { input = "space"}, -- toggle playing or pausing the song
    positionUp = { input = "up"}, -- move 1 snap
    positionDown = { input = "down"},
    positionStart = { input = "l"}, -- jump to start
    snapIncrease = { input = "right"}, -- adjust snap (i.e 1/1 > 1/2 > 1/3 > 1/4 etc)
    snapDecrease = { input = "left"},
    objectModify1 = { input = "p"}, -- resize moons, change comet style
    objectModify2 = { input = "o"}, -- and function
    speedDecrease = { input = ","}, -- change music speed
    speedIncrease = { input = "."},
    save = { input = "s"}, -- save, undo, redo
    undo = { input = "z"},
    redo = { input = "y"},
    quickload = { input = "f1"},
    placeTypeStar = { input = "1"}, -- change placing note type 
    placeTypeMoon = { input = "2"},
    placeTypeComet = { input = "3"},
    placeTypeBPM = { input = "4"}, 
    placeTypeBeat = { input = "5"},
    placeTypeColor = { input = "6"},
    placeTypeSpin = { input = "7"},
    placeTypeScroll = { input = "8"},
    placeTypeBackground = { input = "9"},
    placeTypeForeground = { input = "0"}
}
