local waveform_quality = 128
local waveform_quality_factor = 2
local waveform_quality_min = 1
local waveform_quality_max = 4096

local waveform_greedy = true 

local waveform_mode = "normal"
local waveform_case = 2
local waveform_cases = {
    "none",
    "normal",
    "lowpass",
    "highpass",
    "normal lowpass",
    "normal highpass"
}

local waveform_recalc = false

local waveform_filter_step = 1000
local waveform_filter_lowpass_default = 80
local waveform_filter_lowpass_intensity = waveform_filter_lowpass_default 
local waveform_filter_highpass_default = 15000
local waveform_filter_highpass_intensity = waveform_filter_highpass_default

function getAverageSample(_data, _sample, _ch, _step, _offset)
-- averages all the samples for a range
    local _offset = _offset or 0
    local _avg = 0
    for i=1, _step do
        _avg = _avg + _data:getSample(_sample + i + _offset, _ch)
    end
    return _avg/_step
end

function getHighestSample(_data, _sample, _ch, _step, _offset)
-- only cares ab the highest one. called GREEDY in editor. better for lower quality, and in general IMO.
    local _offset = _offset or 0
    local _high = 0
    for i=1, _step do
        local _sample = _data:getSample(_sample + i + _offset, _ch)
        if math.abs(_sample) > math.abs(_high)  then 
            _high = _sample 
        end
    end
    return _high
end

function editorLimitWaveformQuality(v)
-- waveform quality limits
    waveform_quality = math.min(waveform_quality_max,math.max(waveform_quality_min,v))
end

function editorLimitWaveformIntensity(v)
-- waveform intensity limits
    return math.max(0,v)
end

function doWaveformFilter(_filter, _intensity)
    local _play = LoadedData.Song:isPlaying()
    -- clones the normal waveform data and recalculates a filter to an intensity, then returns it
    -- very expensive operation to be done on the fly. but i dont see any other way of doing it
    -- since it is, we are pausing the song and playing it again once the operation is done to avoid skips in editor
    LoadedData.Song:pause() 

    local _sad = LoadedData.SongData.normal:clone() 
    
    sone.filter(_sad, {
        type = _filter,
        frequency = _intensity
    })  
    
    if _play then LoadedData.Song:play() end
    return _sad
end

function editorDrawWaveform()
    if LoadedData.SongData then
    -- calls to draw all the waveform samples that are needed and prints a bunch of related data
        local debug_text = ""
        debug_text = debug_text .. waveform_mode 

        if waveform_greedy then 
            debug_text = debug_text .. " [GREEDY]"
        end 

        if string.find(waveform_mode,"normal") then
            debug_text = debug_text .. "\n Drawing normal waveform"
            debug_text = debug_text .. "\n quality: 1/" .. waveform_quality
            debug_text = debug_text .. "\n steps: " .. editorDrawSamples(LoadedData.SongData.normal, {1,0,0})
        end
        if string.find(waveform_mode,"lowpass") then
            debug_text = debug_text .. "\n Drawing lowpass waveform @" .. waveform_filter_lowpass_intensity .. " hz."
            debug_text = debug_text .. "\n quality: 1/" .. waveform_quality
            debug_text = debug_text .. "\n steps: " .. editorDrawSamples(LoadedData.SongData.filtered, {0,1,0})
        end
        if string.find(waveform_mode,"highpass") then  
            debug_text = debug_text .. "\n Drawing highpass waveform @" .. waveform_filter_highpass_intensity .. " hz."
            debug_text = debug_text .. "\n quality: 1/" .. waveform_quality
            debug_text = debug_text .. "\n steps: " .. editorDrawSamples(LoadedData.SongData.filtered, {0,0,1})
        end
        love.graphics.print(debug_text,700)
    end
end

function editorDrawSamples(data, color)
-- draws waveform samples to the screen, on both sides of the stage
    local _preColor = pack(love.graphics.getColor())
    local _speed = scroll_speed

    local _sampling_function = getAverageSample
    if waveform_greedy then 
        _sampling_function = getHighestSample
    end

    local _offset_x = game_width/2
    local _offset_y = stage_h

    local _step = waveform_quality*_speed
    local _length = LoadedData.SongData.SampleRate/_speed
    
    local _steps = 0
    for i=0, _length, _step do
        _steps =_steps  + 1
        love.graphics.setColor(color)
        if (song_sample + _step + i)  < LoadedData.SongData.SampleCount then

            _amplitude = _sampling_function(data, song_sample + i, 2, _step, game_audio_offset * LoadedData.SongData.SampleRate/1000) 
            love.graphics.line(
                _offset_x - stage_w/4,
                _offset_y - i * stage_h/LoadedData.SongData.SampleRate * _speed,
                _offset_x - stage_w/4 + _amplitude * stage_w/4,
                _offset_y - i * stage_h/LoadedData.SongData.SampleRate * _speed
            )

            _amplitude = _sampling_function(data, song_sample + i, 2, _step, game_audio_offset * LoadedData.SongData.SampleRate/1000) 
            love.graphics.line(
                _offset_x + stage_w/4,
                _offset_y - i * stage_h/LoadedData.SongData.SampleRate * _speed,
                _offset_x + stage_w/4 + _amplitude * stage_w/4,
                _offset_y - i * stage_h/LoadedData.SongData.SampleRate * _speed
            )
        else
            break
        end
    end
    love.graphics.setColor(unpack(_preColor))
    return _steps
end

function editorToggleWaveformCase()
    -- increments waveform case from list
    waveform_case = waveform_case + 1
    while waveform_case > #waveform_cases do 
        waveform_case = waveform_case - #waveform_cases
    end
    waveform_mode = waveform_cases[waveform_case]
    waveform_recalc = true

    setUIMessage("Waveform toggled to " .. waveform_mode .. ".")
end

function editorSetWaveformStrength(_direction)
    -- increments, decrements or sets strength to default
    if string.find(waveform_mode,"lowpass") then
        if _direction == "default" then
            waveform_filter_lowpass_intensity = waveform_filter_lowpass_default 
        elseif _direction == "up" then
            waveform_filter_lowpass_intensity = waveform_filter_lowpass_intensity + waveform_filter_step / 10
            waveform_filter_lowpass_intensity = editorLimitWaveformIntensity(waveform_filter_lowpass_intensity)
        elseif _direction == "down" then
            waveform_filter_lowpass_intensity = waveform_filter_lowpass_intensity - waveform_filter_step / 10
            waveform_filter_lowpass_intensity = editorLimitWaveformIntensity(waveform_filter_lowpass_intensity)
        end

        setUIMessage("Waveform strength set to ".. waveform_filter_lowpass_intensity .. ".")
    elseif string.find(waveform_mode,"highpass") then
        if _direction == "default" then
            waveform_filter_highpass_intensity = waveform_filter_highpass_default 
        elseif _direction == "up" then
            waveform_filter_highpass_intensity = waveform_filter_highpass_intensity + waveform_filter_step / 10
            waveform_filter_highpass_intensity = editorLimitWaveformIntensity(waveform_filter_highpass_intensity)
        elseif _direction == "down" then
            waveform_filter_highpass_intensity = waveform_filter_highpass_intensity - waveform_filter_step / 10
            waveform_filter_highpass_intensity = editorLimitWaveformIntensity(waveform_filter_highpass_intensity)
        end

        setUIMessage("Waveform strength set to ".. waveform_filter_highpass_intensity .. ".")
    end
    waveform_recalc = true
end

function editorToggleWaveformQuality()
    if waveform_greedy then
        waveform_greedy = nil 
        setUIMessage("Disabled greedy amplitude for waveform.")
    else 
        waveform_greedy = true 
        setUIMessage("Enabled greedy amplitude for waveform.")
    end 
end

function editorSetWaveformQuality(_direction)
    -- increments or decrements quality of waveform
    local _power
    if _direction == "up" then
        _power = 1
    elseif _direction == "down" then
        _power = -1
    end
    
    editorLimitWaveformQuality(waveform_quality / waveform_quality_factor ^ _power) 
    setUIMessage("Waveform quality set to 1/".. waveform_quality .. ".")
end

function editorRecalculateWaveform()
    -- recalculate waveform filter
    if waveform_recalc 
    and not Keybind.Down(Keybind.editor.waveformStrengthUp)
    and not Keybind.Down(Keybind.editor.waveformStrengthDown) 
    then 
        if string.find(waveform_mode,"lowpass") then
            LoadedData.SongData.filtered = doWaveformFilter("lowpass",waveform_filter_lowpass_intensity)
        end
        if string.find(waveform_mode,"highpass") then
            LoadedData.SongData.filtered = doWaveformFilter("highpass", waveform_filter_highpass_intensity)
        end     
        waveform_recalc = false   
    end
end
