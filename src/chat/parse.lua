local developer = {
    "admin",
    "lustlion",
    "remi"
}

function parseChatMessage(_message)
    local _data = string.gmatch(_message, "\t([^\t]+)")
    local _nick = _data()
    local _text = _data()
    local _user = _data()

    local _color = {1,1,1}
    local _display_name = "*".._nick
    if _user == _nick then
        _color = {1,1,1}
        for i=1, #developer do
            if developer[i] == _user then
                _color = {1,0,0}
            end
        end
        _display_name = _user
    end
    networkmessage = {}
    table.insert_batch(networkmessage, c_main, "CHAT | ")
    table.insert_batch(networkmessage, _color, _display_name)
    table.insert_batch(networkmessage, {1,1,1}, ": " .. _text)
end