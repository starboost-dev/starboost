-- draws a colored background
function playfieldDrawBackground(_color)
    local _preColor = pack(love.graphics.getColor())
    prof.push("color")
    love.graphics.setColor(unpack(_color))
    love.graphics.rectangle(
        "fill",
        0,
        0,
        game_width,
        game_height
    )
    prof.pop("color")

    love.graphics.setColor(1,1,1,1)
    prof.push("image")
    prof.push("file")
    if LoadedChartData then
        if #LoadedChartData.Backgrounds > 0 then
            local _background = CurrentBackground.img
            local _w = _background:getWidth()
            local _h = _background:getHeight()
            local _size
            if (_w/game_width) < (_h/game_height) then
                _size = game_width/_w
            else
                _size = game_height/_h
            end
            local _offset_x = (game_width - _w*_size)/2
            local _offset_y = (game_height - _h*_size)/2
    
            love.graphics.draw(
                _background,
                _offset_x,
                _offset_y,
                0,
                _size
        )
        end
    prof.pop("file")
    prof.push("dim")
    local _color = table.dcopy(c_bg)
    _color[4] = game_background_dim
     
    love.graphics.setColor(_color)
    love.graphics.rectangle(
        "fill",
        0,
        0,
        game_width,
        game_height
    )
    prof.pop("dim")
    prof.pop("image")
    end
    love.graphics.setColor(unpack(_preColor))
end

function playfieldDrawForegrounds()
    local _preColor = pack(love.graphics.getColor())
    
    love.graphics.setColor(1,1,1,1)
    for i=1, #LoadedChartData.Foregrounds do 
        if LoadedChartData.Foregrounds[i].status then
            local _x = LoadedChartData.Foregrounds[i].x or 0
            local _y = LoadedChartData.Foregrounds[i].y or 0
            local _foreground = LoadedChartData.Foregrounds[i].img
            love.graphics.draw(
                _foreground,
                _x,
                _y
            )
        end
    end
    love.graphics.setColor(unpack(_preColor))
end

-- draws stage glow, according to the song beat)
function playfieldDrawStageGlow()
    local _bpm = song_bpm
    local _time = song_time - song_delay
    local _beat = song_beat

    local _offset_x = game_width/2-image.playfield.stage_glow:getWidth()/2
    local _offset_y = game_height-image.playfield.stage_bottom:getHeight()-image.playfield.stage_glow:getHeight()+3

    local _preColor = pack(love.graphics.getColor())
    if LoadedChartData then
        love.graphics.setColor(1,1,1,1-_time/1000*(_bpm/60)%1)
        love.graphics.draw(image.playfield.stage_glow,
        _offset_x,
        _offset_y,
        0,
        1)
        love.graphics.setColor(1,1,1,1-_time/1000*(_bpm/60)%_beat)
        love.graphics.draw(image.playfield.stage_glow,
        _offset_x,
        _offset_y,
        0,
        1)
        love.graphics.draw(image.playfield.stage_glow,
        _offset_x,
        _offset_y,
        0,
        1)
    end
    love.graphics.setColor(unpack(_preColor))
end

-- draws the stage
function playfieldDrawStageSides()
    local _preColor = pack(love.graphics.getColor())

    love.graphics.setColor(1,1,1,0.5)

    local _offset_x = game_width/2-image.playfield.stage_sides:getWidth()/2
    local _offset_y = game_height-image.playfield.stage_bottom:getHeight()-image.playfield.stage_sides:getHeight()+3
    
    love.graphics.draw(image.playfield.stage_sides,
    _offset_x,
    _offset_y,
    0,
    1)

    love.graphics.setColor(unpack(_preColor))
end

-- draws the stage
function playfieldDrawStageBottom()
    local _preColor = pack(love.graphics.getColor())

    love.graphics.setColor(1,1,1,1)

    local _offset_x = game_width/2-image.playfield.stage_bottom:getWidth()/2
    local _offset_y = game_height-image.playfield.stage_bottom:getHeight()

    love.graphics.draw(
        image.playfield.stage_bottom,
        _offset_x,
        _offset_y,
        0,
        1
    )

    love.graphics.setColor(unpack(_preColor))
end

-- draws the judge gradient for past notes
function playfieldDrawOverlay()
    local _preColor = pack(love.graphics.getColor())
    
    local _anchor_x = game_width/2
    local _anchor_y = game_height-image.playfield.stage_bottom:getHeight()/2+3
    
    local _judgement_w = 356
    local _judgement_h = 40

    love.graphics.setColor(1,1,1,1)

    local _offset_x = _anchor_x - image.playfield.judgement_line:getWidth()/2
    local _offset_y = _anchor_y - image.playfield.judgement_line:getHeight()/2
    love.graphics.draw(image.playfield.judgement_line,_offset_x,_offset_y)
    
    local _offset_x = _anchor_x - image.playfield.judgement_star:getWidth()/2
    local _offset_y = _anchor_y - image.playfield.judgement_star:getHeight()/2
    love.graphics.draw(image.playfield.judgement_star,_offset_x,_offset_y)

    love.graphics.setColor(unpack(_preColor))
end

-- draws all the measures and beats
function playfieldDrawBeats()

    local _beat = 0
    local _song_beat = song_beat
    local _color = c_main
    
    love.graphics.setLineWidth(4)
    for i=1, #LoadedChartData.Beats do
        playfieldDrawBar(LoadedChartData.Beats[i],_color)
    end
end

-- draws a bar on the stage, according to scrolling speed, a specific color and alpha value
function playfieldDrawBar(_beat,_color)
    local _ms = _beat.ms
    local _measure = _beat.measure or false

    local _preColor = pack(love.graphics.getColor())
    
    local _a = 0.25
    if _measure then 
        _a = 1
    end
    _color[4] = _a

    local _offset_x = game_width/2-image.playfield.stage_sides:getWidth()/2
    local _offset_y = stage_h
    local _speed = scroll_speed
    local _relative_factor = stage_h/1000 * _speed
    local _position = (song_time - song_delay - _ms) * _relative_factor

    _width = tonumber(_width) or 1
    love.graphics.setColor(unpack(_color))
    love.graphics.line(
        _offset_x + 3,
        _offset_y + _position,
        _offset_x + image.playfield.stage_sides:getWidth()-3,
        _offset_y + _position
    )
    love.graphics.setColor(unpack(_preColor))
end
    
