local scroll_speed_step = 0.1
local scroll_speed_max = 8

-- handles the inputs for modifiyng scrolling speed (and for development, switching between ingame and editor)
function playfieldInput(_dt) 
    if not game_input then
        if Keybind.Down_delayed(Keybind.playfield.zoomIn, 0.1, _dt) then
            editorLimitScrollSpeed(scroll_speed - scroll_speed_step)
        end

        if Keybind.Down_delayed(Keybind.playfield.zoomOut, 0.1, _dt) then
            editorLimitScrollSpeed(scroll_speed + scroll_speed_step)
        end

        if Keybind.Press(Keybind.playfield.switchMode) then

            song_delay = 0
            song_delayed = false

            if game_state == game_state_cases.editor then 
                game_state = game_state_cases.ingame
                LoadedData.Song:play()
            elseif game_state == game_state_cases.ingame then
                game_state = game_state_cases.editor
                LoadedData.Song:pause()
            end

            optimizeIngameMap(LoadedChartData)
            gameplayResetCounts()
        end
    end 
end


-- playfield scroll speed limits
function editorLimitScrollSpeed(v)
    local _factor = 1/scroll_speed_step
    scroll_speed = math.min(scroll_speed_max,math.max(scroll_speed_step,v))
    scroll_speed = math.floor(scroll_speed_step+(_factor*scroll_speed))/_factor
end
