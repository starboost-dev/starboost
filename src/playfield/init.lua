image.playfield = {}
image.playfield.stage_glow = love.graphics.newImage("res/image/playfield_beat_glow.png")
image.playfield.stage_sides = love.graphics.newImage("res/image/playfield_stage_sides.png")
image.playfield.stage_bottom = love.graphics.newImage("res/image/playfield_stage_bottom.png")

image.playfield.judgement_star = love.graphics.newImage("res/image/judgement_star.png")
image.playfield.judgement_line = love.graphics.newImage("res/image/judgement_line.png")

lane_w = 72
stage_h = game_height - image.playfield.stage_bottom:getHeight()
stage_w = lane_w*5 + 1

require "src/playfield.input"
require "src/playfield.render"

Keybind.playfield = {
    switchMode = { input = "tab"}, -- switch ingame <-> editor
    zoomIn = { input = "g"}, -- change scroll speed
    zoomOut = { input = "h"},
    hitsoundToggle = { input = "h"}, -- toggle clap sound
}