function client:onmessage(_message)
    local _type = string.gmatch(_message, "([^\t]+)\t")() 
    local _content = string.gmatch(_message, "(\t.+)")()

    if _type == "chat" then parseChatMessage(_content) end
end
function client:onopen()
end
function client:onclose(code, reason)
    print("closecode: "..code..", reason: "..reason)
end
function client:onerror(string)
    error("error: "..string)
end