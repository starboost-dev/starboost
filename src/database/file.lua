function getDirectoryVisibleItems(path)
    local _items = love.filesystem.getDirectoryItems("songs/")
    for i, item in pairs(_items) do
        if string.sub(item,1,1) == "." then
            table.remove(_items,i)
        end    
    end
    table.sort(_items)
    return _items
end