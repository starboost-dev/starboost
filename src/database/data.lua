-- Database
    -- Song - table(n:values)
        -- Path - string
        -- AudioPath - string
        -- Title - string
        -- Artists - table  
            -- string
        -- Album - string (this will be used alongside packs and collections)
        --
        -- Duration - number (in ms)
        -- AudioPreview - number (in ms)

        -- Genres - table 
            -- string (free answer, just look it up dummy! also "Undefined" option)
            
        -- PackTags (collections, but charter defined, can still be hidden by user, is exported regardless) - table 
            -- string (the tag itself) exported - table(key:values) 
                -- hidden (local property) not exported
                
        -- CollectionTags (all the *song select* collections it is included in, user only, not exported) - table 
            -- string

        -- ChartData - table
            -- BPMs - table in song select (show as range if > 1)
                -- number 
            -- Backgrounds - table 
                -- string
            -- ForegroundPaths - table 
                -- string
            -- Difficulty - number
            -- Name - string
            -- Charters - table 
                -- string

local function newEntry(path)
    local _chart = false
    local _chart_data = {}
    local _genre = false

    local _data = {
        Path = path,
        AudioPath = "",
        Title = "Untitled",
        Artist = "Unknown",
        Album = "Unknown",
        Genres = {
            "Undefined"
        },
        SongData = {
            Offset = 0,
            Duration = 0,
            AudioPreview = 0
        }, 
        PackTags = {}, 
        CollectionTags = {},
        ChartData = {}
    }

    for command in love.filesystem.lines(path .."/data") do
        local _next_element = string.gmatch(command,"%s(.+)")   
        local _property = command:getFirstWord()

        if _property == "title" then
            _data.Title = _next_element()
            
        elseif _property == "artist" then
            _data.Artist = _next_element()

        elseif _property == "offset" then
            _data.SongData.Offset = tonumber(_next_element())

        elseif _property == "album" then
            _data.Album = _next_element()

        elseif _property == "audio_file" then
            _data.AudioPath = _next_element()

        elseif _property == "audio_preview" then
            _data.SongData.AudioPreview = tonumber(_next_element())

        elseif _property == "chart" then
            _chart = true
            _chart_data = {}
            _chart_data.Name = ""
            _chart_data.Difficulty = 0
            _chart_data.Charters = {}
            _chart_data.BPMs = {}
            _chart_data.ForegroundPaths = {}
            _chart_data.MapString = ""
            _chart_data.Backgrounds = {}
            _chart_data.NoteCount = 0
            _chart_data.StarCount = 0
            _chart_data.MoonCount = 0
            _chart_data.CometCount = 0
            table.insert(_data.ChartData, _chart_data) 

        elseif _property == "name" then 
            _chart_data.Name = _next_element()

        elseif _property == "author" then 
            local _cutie = _next_element()
            _chart_data.Charters[#_chart_data.Charters+1] = _cutie

        elseif _property == "tag" then 
            local _tag = _next_element()
            _chart_data.PackTags[#_chart_data.PackTags+1] = {
                key = _tag,
                hidden = false
            }
            
        elseif _property == "genre" then 
            if _genre then
                _chart_data.Genres[#_chart_data.Genres+1] = _next_element()
            else
                _genre = true
                _chart_data.Genres[#_chart_data.Genres] = _next_element()
            end

        elseif _property == "difficulty" then 
            local _diff = _next_element()
            _chart_data.Difficulty = tonumber(_diff)

        elseif _property == "background_file" then
            local _path = _next_element()

            local _bg = {
                path = _path,
                img = love.graphics.newImage(_data.Path .."/".. _path)
            }
            _chart_data.Backgrounds[#_chart_data.Backgrounds+1] = _bg

        elseif _property == "foreground_file" then
            local _path = _next_element()
            _chart_data.ForegroundPaths[#_chart_data.ForegroundPaths+1] = _path
        elseif command ~= "" and _chart then
            _chart_data.MapString = _chart_data.MapString .. command .. "\n"
        end
    end

    return _data
end

local function saveDatabase()

end

local function loadDatabase()

end


function buildDatabase()
    Database = {}
    
    loadDatabase()

    for _, data in pairs(Database) do
        data.remove = true
    end

    local _list = getDirectoryVisibleItems("songs/")
    if #_list == 0 then 
        CreateNewSong()
        _list = getDirectoryVisibleItems("songs/")
    end

    -- lets try to make entries or find them
    for i=1, #_list do
        if not Database[_list[i]] then
            Database[i] = newEntry(game_song_directory .. _list[i])
        else
            Database[i].remove = false
        end
    end

    for _, data in pairs(Database) do
        if data.remove then data = nil end
    end

    table.sort(Database, function (k1, k2) return k1.Title < k2.Title end)
    
    saveDatabase()
end

function loadDatabaseAudio(_data)
    LoadedData = _data
    if _data.AudioPath ~= "none" then
        local pathfile = LoadedData.Path .. "/" .. LoadedData.AudioPath
        -- load song on thread
        lily.newSource(pathfile, "stream"):onComplete(function(_, song)
            LoadedData.Song = song
            -- get basic audio samples and filtered audio samples to use in the editor
            LoadedData.SongData.SampleCount  = LoadedData.Song:getDuration("samples")   -- samples 
            LoadedData.SongData.Duration = math.floor(LoadedData.Song:getDuration("seconds")*1000) -- ms
            lily.newSoundData(pathfile):onComplete(function(_, song_data)    
                LoadedData.SongData.normal = song_data 
                LoadedData.SongData.filtered = LoadedData.SongData.normal:clone() 
                LoadedData.SongData.BitDepth = LoadedData.SongData.normal:getBitDepth()
                LoadedData.SongData.SampleRate = LoadedData.SongData.normal:getSampleRate()
                songSelectSetSongPreview(LoadedData)
            end)
        end)
    end
end

-- ChartLoader
function loadDatabaseMap(_data, _chart)
    local _map = _data.ChartData[_chart].MapString
    
    LoadedChartData = table.dcopy(_data.ChartData[_chart])

    LoadedChartData.Variables = {}
    LoadedChartData.Map = {}

    for line in _map:gmatch("[^\n]+") do
        local _property = line:getFirstWord()

        if _property == "assign" then
            local _var_name = string.match(line,"%s(%S+)")  
            local _var_value = string.match(line,"%s%S+%s(.+)")   
            LoadedChartData.Variables[_var_name] = _var_value
        else
            -- get the ms 
            local _ms = tonumber(string.sub(line,1,string.find(line,"%s")))
            local _command = string.sub(line,string.find(line,"%s")+1)
            table.insert(LoadedChartData.Map, {
                ms = _ms,
                command = _command,
                hit = false
            })
        end
    end
    sortMap(LoadedChartData)
    LoadedChartData.NoteCount, LoadedChartData.StarCount, LoadedChartData.MoonCount, LoadedChartData.CometCount = getChartNoteCount(LoadedChartData)
    LoadedChartData.Beats = getChartMeasures(LoadedChartData)
    optimizeIngameMap(LoadedChartData)
    setNoteProperties(LoadedChartData.Map, "editor")
    LoadedChartData.Foregrounds = {}
    for i=1, #LoadedChartData.ForegroundPaths do
        LoadedChartData.Foregrounds[i] = {
            img = love.graphics.newImage(_data.Path .."/".. LoadedChartData.ForegroundPaths[i]),
            x = 0,
            y = 0
        }
    end
end

function optimizeIngameMap(chart)
    chart.IngameMap = table.dcopy(chart.Map)
    setNoteProperties(chart.IngameMap, "ingame")
end

function sortMap(_map)
    table.sort(_map,function(_k1, _k2) 
        if _k1.ms == _k2.ms then
            return _k1.command < _k2.command 
        else
            return _k1.ms < _k2.ms 
        end
    end)
end
