function replaceChartVariables(_text) -- Variables: %var_name
    local i = 0
    local _success = false
    while true do
      local _start, _end = string.find(_text, "%%(%w+)", i+1)
      i = _start
      if _start == nil then break end
      local _variable = string.sub(_text, _start+1, _end)
      if LoadedChartData.Variables[_variable] then 
        _success = true
        _text = string.gsub(_text, "%%".. _variable, LoadedChartData.Variables[_variable])
      else
        error("Variables [".. _variable .."] has no value assigned!")
      end
    end
    return _text, _success
end

function setNoteProperties(chart, _mode)
    local _delete = false
    if _mode == "ingame" then 
        _delete = true
    end
    local _color = {}
    local _spin = {}
    local _velocity =  {}
    local _rotation = 1
  
    for i=1, #chart do
        if _mode == "editor" then
            _spin = {0, 0, 0, 0, 0}
            _rotation = 0
        end

        local data = chart[i]
        local _ms = data.ms 
        local _command, _variable = replaceChartVariables(data.command)
        local _next_element = string.gmatch(_command,"%s(%S+)") 
        local _speed = scroll_speed      

        if string.match(_command, object.spin) then
            local _option = _next_element() or 1
            local _value = tonumber(_next_element() or 0)
            
            updateProperty(_spin, _option, _value)

            if _delete then data.remove = true end
        elseif string.match(_command,object.scroll) then
            local _option = _next_element() or 1
            local _value = tonumber(_next_element() or 0)
            
            updateProperty(_velocity, _option, _value)

            if _delete then data.remove = true end
        elseif string.match(_command,object.color) then
            local _option = _next_element()
            
            local _r = tonumber(_next_element() or 0)
            local _g = tonumber(_next_element() or 0)
            local _b = tonumber(_next_element() or 0)
            
            local _value = {_r/255, _g/255, _b/255}

            updateProperty(_color, _option, _value)
        
            if _delete then data.remove = true end
        elseif string.match(_command,object.star) then
            local _lane = tonumber(_next_element())
                
                data.color = _color[_lane]
                data.spin = _spin[_lane]
                data.velocity = _velocity[_lane] 
                data.width = 1
        elseif string.match(_command,object.moon) then
            local _lane = tonumber(_next_element())
            local _width = tonumber(_next_element())
            
                data.color = _color[_lane]
                data.spin = _spin[_lane]
                data.velocity = _velocity[_lane] 
                data.rotation = _rotation
                data.width = _width
            elseif string.match(_command,object.comet) then
                local _duration = tonumber(_next_element())
            local _x1 = tonumber(_next_element()) -- start horizontal position 
            local _x2 = tonumber(_next_element()) -- end horizontal position after duration 
            local _mode = _next_element() -- end horizontal position after duration 
            local _style = _next_element() or "straight"-- end horizontal position after duration 
            
            local _xdiff = _x2 - _x1 
            
            if _xdiff > 0 then 
                if _duration > 0 then 
                        data.color = _color.comet_right_hold
                    data.spin = _spin.comet_right_hold
                        data.velocity = _velocity.comet_right_hold
                else
                        data.color = _color.comet_right_hit
                        data.spin = _spin.comet_right_hit
                        data.velocity = _velocity.comet_right_hit
                end
            elseif _xdiff == 0 then 
                if _duration > 0 then 
                        data.color = _color.comet_center_hold
                        data.spin = _spin.comet_center_hold
                        data.velocity = _velocity.comet_center_hold
                else
                        data.color = _color.comet_center_hit
                        data.spin = _spin.comet_center_hit
                        data.velocity = _velocity.comet_center_hit
                end
            elseif _xdiff < 0 then 
                if _duration > 0 then 
                        data.color = _color.comet_left_hold
                        data.spin = _spin.comet_left_hold
                        data.velocity = _velocity.comet_left_hold
                else
                        data.color = _color.comet_left_hit
                        data.spin = _spin.comet_left_hit
                        data.velocity = _velocity.comet_left_hit
                end
            end
                data.mode = _mode
                data.render_by = _duration * 1.1
        end
    end
    for i, data in pairs(chart) do
        if data.remove then table.remove(chart,i) end
    end
end

function updateProperty(table, option, value)
  if option == "reset" then
      for i=1, 5 do 
          table[i] = nil
      end
      table.comet_left_hold = nil
      table.comet_left_hit = nil
      table.comet_center_hold = nil
      table.comet_center_hit = nil
      table.comet_right_hold = nil
      table.comet_right_hit = nil
  elseif option == "all" then
      for i=1, 5 do 
          table[i] = value
      end
      table.comet_left_hold = value
      table.comet_left_hit = value
      table.comet_center_hold = value
      table.comet_center_hit = value
      table.comet_right_hold = value
      table.comet_right_hit = value
  elseif option == "comet" then
      table.comet_left_hold = value
      table.comet_left_hit = value
      table.comet_center_hold = value
      table.comet_center_hit = value
      table.comet_right_hold = value
      table.comet_right_hit = value
  elseif option == "comet_left" then
      table.comet_left_hold = value
      table.comet_left_hit = value
  elseif option == "comet_left_hold" then
      table.comet_left_hold = value
  elseif option == "comet_left_hit" then
      table.comet_left_hit = value
  elseif option == "comet_center" then
      table.comet_center_hold = value
      table.comet_center_hit = value
  elseif option == "comet_center_hold" then
      table.comet_center_hold = value
  elseif option == "comet_center_hit" then
      table.comet_center_hit = value
  elseif option == "comet_right" then
      table.comet_right_hold = value
      table.comet_right_hit = value
  elseif option == "comet_right_hold" then
      table.comet_right_hold = value
  elseif option == "comet_right_hit" then
      table.comet_right_hit = value
  else
      table[tonumber(option)] = value
  end
end

function getChartNoteCount(chart, _max_ms)
  local _count = 0
  local star_count = 0
  local moon_count = 0
  local comet_count = 0

  for i=1, #chart.Map do

      local data = chart.Map[i]
      local _ms = data.ms

      if i>#chart.Map then break end
      if _max_ms and _ms > _max_ms then break end

      local _command = replaceChartVariables(data.command)
      local _next_element = string.gmatch(_command,"%s(%S+)")   
  
      if string.match(data.command,object.star) then
          _count = _count + 1
          star_count = star_count + 1
      end
      if string.match(data.command,object.moon) then
          _count = _count + 1
          moon_count = moon_count + 1
      end

      if string.match(data.command,object.comet) then 
          local _duration = tonumber(_next_element()) -- duration of comet line
          local _x1 = tonumber(_next_element()) -- start horizontal position 
          local _x2 = tonumber(_next_element()) -- end horizontal position after duration 
          local _mode = _next_element() -- end horizontal position after duration 
          local _xdiff = _x2 - _x1 


          if _xdiff > 0
          or _xdiff < 0 then
              if _mode ~= "line" 
              and _mode ~= "hold" then 
                  comet_count = comet_count + 1 
                  _count = _count + 1 
              end
          end
      end
  end

  local _ticks = getChartTicks(chart, _max_ms)
  comet_count = comet_count + _ticks
  _count = _count + _ticks
  return _count, star_count, moon_count, comet_count
end

function getChartTicks(chart, _max_ms)
  local _list1 = {}
  local _list2 = {}
  
  -- 1 gather all data on list
  for _, data in pairs(chart.Map) do
      local _ms = data.ms
      local _command = replaceChartVariables(data.command)
      local _next_element = string.gmatch(_command,"%s(%S+)")

      if _max_ms and _ms > _max_ms then break end
      if string.match(data.command,object.comet) then 
          local _duration = tonumber(_next_element()) -- duration of comet line
          local _x1 = tonumber(_next_element()) -- start horizontal position 
          local _x2 = tonumber(_next_element()) -- end horizontal position after duration 
          local _mode = _next_element() -- end horizontal position after duration 
          local _xdiff = _x2 - _x1

          if _duration > 0 
          and _mode ~= "line" then
              if _xdiff > 0 then
                  table.insert(
                      _list1, 
                      {
                          ms = _ms,
                          time = _duration
                      }
                  )
              elseif _xdiff < 0 then
                  table.insert(
                      _list2, 
                      {
                          ms = _ms,
                          time = _duration
                      }
                  )                
              end
          end
      end
  end
  
  return getTicks(_list1, _max_ms) + getTicks(_list2, _max_ms)
end

function getTicks(_list, _max_ms)
  local _current_ms = -1
  local _ms = 0
  local _time = 0
  local _current_duration = 0
  local _total = 0

  for i=1, 1+#_list do

      if _list[i] then
          _ms = _list[i].ms
          _time = _list[i].time      
      end
      
      if _max_ms and _ms > _max_ms then break end

      if i > #_list 
      or _ms > _current_ms
      and _current_duration > 0 then 
          local _ticks = math.ceil(_current_duration/lateral_tick)
          _total = _total + _ticks
          _current_duration = 0
          _current_ms = _ms
      end
      
      if i > #_list then break end

      if _time > _current_duration then 
          _current_duration = _time     
      end
  end

  return _total    
end

function getChartFirstInputPosition()
  for _, data in pairs(LoadedChartData.Map) do 
      local _ms = data.ms 
      if not _ms or _ms > 5000 then break end
      local _command = replaceChartVariables(data.command)
      if string.match(_command,object.star) 
      or string.match(_command,object.moon)  
      or string.match(_command,object.comet) then
          return _ms
      end
  end
  return 5000
end


function getChartMeasures(chart)
    local _list = {}
    local _ms = 0
    local _song_bpm = 0
    local _song_beat = 0
    local _beat = 0
    local _song_duration = LoadedData.SongData.Duration
    
    while _ms < _song_duration do
        _beat = _beat + 1
        _ms = _ms + 1/(_song_bpm/60)*1000
        local _measure = false
        
        for _, data in pairs(chart.Map) do
            if _ms >= data.ms 
            and not data.hit then
                local _command = replaceChartVariables(data.command)
                local _next_element = string.gmatch(_command,"%s(%S+)")
                if string.match(_command,object.bpm) then
                    _measure = true
                      data.hit = true
                    _song_bpm = tonumber(_next_element())
                    _beat = 0
                    _ms = data.ms
                end
                if string.match(_command,object.beat) then
                    _measure = true
                      data.hit = true
                    _song_beat = tonumber(_next_element())
                    _beat = 0
                    _ms = data.ms
                end
            end
        end
        
        if _beat >= _song_beat then 
            _beat = _beat - _song_beat 
            _measure = true        
        end
        
        table.insert(_list, {ms = _ms, measure = _measure})
    end
    return _list
end

function getColumn(_x)
    local _offset_x = game_width/2-image.playfield.stage_sides:getWidth()/2
    if _x < _offset_x - image.playfield.stage_sides:getWidth()*0.5 then 
        return -10
    elseif _x > _offset_x + image.playfield.stage_sides:getWidth()*1.5 then
        return 10
    end
    return math.floor(10*(_x - _offset_x)/10/(lane_w/2))-5
end

function snapToBeat(_ms, _ratio)
    -- given an ms value, it snapes it to the beat, or a beat ratio
    local _ratio = _ratio or 1
    local _return_ms = 0
    local _beat = 1
    local chart = LoadedChartData

    debug_text = ""
    for i=1, #chart.Beats do
        if _ms > chart.Beats[i].ms then
            if chart.Beats[i].ms > _return_ms then
                _return_ms = chart.Beats[i].ms
                _beat = i+1
            end
        end
    end
    -- now apply L + ratio
    local _beat_separation = 0
    if chart.Beats[_beat+1] then
        local _beat_duration = chart.Beats[_beat+1].ms - chart.Beats[_beat].ms
        _beat_separation = (_ratio * _beat_duration)
        local _inbeat_ms = _ms - _return_ms
        _return_ms = _return_ms + _beat_separation * math.floor(_inbeat_ms / _beat_separation) 
    else
        _return_ms = _ms
    end
    
    return _return_ms, _beat_separation
end

function isHitOccupied(_ms_check,_lane_check)
-- checks if a note is occupying an ms on a lane position. returns the chart data position if true
    local chart = LoadedChartData

    for i=1, #chart.Map do
        if math.floor(0.5+chart.Map[i].ms) == math.floor(0.5+_ms_check) then
            local _command = chart.Map[i].command
            local _next_element = string.gmatch(_command,"%s(%S+)")   
            if string.match(_command,object.star) then
                _lane = tonumber(_next_element())
                if _lane_check == _lane then
                    return i
                end
            elseif string.match(_command,object.moon) then
                local _lane = tonumber(_next_element())
                local _width = tonumber(_next_element())
                if _lane_check >= _lane
                and _lane_check < _lane + _width then
                    return i
                end
            end
        end
    end
    return false
end
    
function isCometOccupied(_ms_check, _column_check)
    -- checks if a note is occupying an ms on a lane position. returns the chart data position if true
    local chart = LoadedChartData

    for i=1, #chart.Map do
        local _ms = chart.Map[i].ms
        if math.floor(0.5+_ms) == math.floor(0.5+_ms_check) then
            local _command = chart.Map[i].command
            local _next_element = string.gmatch(_command,"%s(%S+)")   
            if string.match(_command,object.comet) then
                local _ms2 = tonumber(_next_element()) + _ms
                local _column1 = tonumber(_next_element()) 
                local _column2 = tonumber(_next_element()) 
                
                if _column_check == _column1 then
                    return i
                end
            end
        end
    end
    return false
end

function SaveSong()
    local _string = "title " .. LoadedData.Title
    _string = _string .. "\n" .. "artist " .. LoadedData.Artist
    -- for i=1, #LoadedData.Artists do
    	-- _string = _string .. "\n" .. "artist " .. LoadedData.Artists[i]
    -- end -- REMI TODO
    _string = _string .. "\n".. "audio_file " .. LoadedData.AudioPath 
	_string = _string .. "\n".. "offset " .. LoadedData.SongData.Offset or "0"

    
    for _, chart in pairs(LoadedData.ChartData) do 
        _string = _string .. "\n"
        _string = _string .. "\n".. "chart"
        _string = _string .. "\n".. "name " .. chart.Name  
        for i=1, #chart.Charters do
        	_string = _string .. "\n" .. "author " .. chart.Charters[i]
        end
        _string = _string .. "\n".. "difficulty " .. chart.Difficulty

        for i=1, #chart.Backgrounds do
            _string = _string .. "\n".. "background_file " .. chart.Backgrounds[i].path
        end      
        for i=1, #chart.ForegroundPaths do
            _string = _string .. "\n".. "foreground_file " .. chart.ForegroundPaths[i].path
        end      
        -- for _name, _value in pairs(chart.Variable) do
            -- _string = _string .. "\n".. "assign " .. _name .. " " .. _value
        -- end -- i think these are in mapstring?
        
        _string = _string .. "\n" .. chart.MapString
    end
    -- saves song as the artist
    local _filename = LoadedData.Path .. "/data"

    love.filesystem.createDirectory("songs")
    return love.filesystem.write(_filename, _string)
end

function updateChartProperties()
    LoadedChartData.NoteCount = getChartNoteCount(LoadedChartData)
    sortMap(LoadedChartData.Map)
    setNoteProperties(LoadedChartData.Map, "editor")
end
