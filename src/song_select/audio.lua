local preview_audio = nil
local preview_loop = true
local preview_delay = 0
local preview_delayed = true

function getSongPreview(data)
    local _song_data = data.SongData.normal
    local _samplerate = data.SongData.SampleRate
    local _channels = _song_data:getChannelCount()
    local _bitdepth = data.SongData.BitDepth
    local _duration = _samplerate * 25
    
    local _preview_sample = math.floor(data.SongData.AudioPreview * (_samplerate/1000))
    
    local _preview = love.sound.newSoundData( _duration, _samplerate, _bitdepth, _channels)
    for i=0, _duration-1, _channels do
        for ch=1, _channels do
            _preview:setSample(i, ch, _song_data:getSample(i+_preview_sample, ch))
        end
    end
    sone.fadeOut(_preview, 5)
    sone.fadeIn(_preview, 2)
    return love.audio.newSource(_preview, "stream")
end
    
function songSelectStopPreview()
    if preview_audio then preview_audio:stop() end
end

function songSelectSetSongPreview(data)
    songSelectStopPreview()
    preview_audio = nil
    preview_audio = getSongPreview(data)
    preview_loop = true
end

function songSelectSetPreviewDelay()
    preview_delay = 0.5
    preview_delayed = true
end

function songSelectLoopPreview(_dt)
    if preview_delay > 0 then
        preview_delay = preview_delay - _dt
    elseif preview_delayed then
        preview_delayed = false
        loadDatabaseAudio(songSelectGetSelectedSongData())
    end

    if preview_loop
    and preview_audio
    and not preview_audio:isPlaying() then
        preview_audio:play()
    end
end

function songSelectStopPreview()
    preview_loop = false
    if preview_audio then preview_audio:stop() end
end