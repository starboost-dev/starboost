image.song_select = {}
image.song_select.background = love.graphics.newImage("res/image/song_select/background.png")
image.song_select.header = love.graphics.newImage("res/image/song_select/header.png")
image.song_select.footer = love.graphics.newImage("res/image/song_select/footer.png")
image.song_select.image_container = love.graphics.newImage("res/image/song_select/image_container.png")
image.song_select.infobox1 = love.graphics.newImage("res/image/song_select/infobox1.png")
image.song_select.infobox2 = love.graphics.newImage("res/image/song_select/infobox2.png")
image.song_select.infobox3 = love.graphics.newImage("res/image/song_select/infobox3.png")
image.song_select.quickinfo = love.graphics.newImage("res/image/song_select/quickinfo.png")
image.song_select.partner_gradient = love.graphics.newImage("res/image/song_select/partner_gradient.png")
image.song_select.song_active = love.graphics.newImage("res/image/song_select/song_active.png")
image.song_select.song_inactive = love.graphics.newImage("res/image/song_select/song_inactive.png")
image.song_select.start_button = love.graphics.newImage("res/image/song_select/start_button.png")

display_timer = 0
display_time = 5/60

f_radio32 = love.graphics.newFont("res/font/radiofb.ttf", 32)
f_radio24 = love.graphics.newFont("res/font/radiofb.ttf", 24)
f_radio16 = love.graphics.newFont("res/font/radiofb.ttf", 16)

f_signika32 = love.graphics.newFont("res/font/signika.ttf", 32)
f_signika24 = love.graphics.newFont("res/font/signika.ttf", 24)
f_signika16 = love.graphics.newFont("res/font/signika.ttf", 16)

require "src/song_select.input"
require "src/song_select.render"
require "src/song_select.audio"
require "src/song_select.draw"
require "src/song_select.loop"
require "src/song_select.partner"