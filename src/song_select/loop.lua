local song_listed = false

function songSelectLoop(_dt)

    if display_timer > display_time then display_timer = display_timer - display_time end
    display_timer = display_timer + 1 * _dt
    songSelectUpdateDisplay()
    
    if not song_listed then 
        song_listed = true
        buildDatabase() 
        songSelectGetList()
    end
    songSelectInput(_dt)
    songSelectLoopPreview(_dt)
end