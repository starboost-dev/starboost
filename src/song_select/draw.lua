function songSelectDraw()
    songSelectDrawBackground()
    songSelectDrawList()
    songSelectDrawStartButton()
    songSelectDrawPartner(getCurrentPartner())
    songSelectDrawHeader()
    songSelectDrawFooter()
end
