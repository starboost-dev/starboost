local _text_header = love.graphics.newText( f_radio16 , "" )
local _text_footer = love.graphics.newText( f_signika16 , "" )

local _text_song_name = love.graphics.newText( f_signika16 , "" )
local _text_song_artist = love.graphics.newText( f_signika16 , "" )
local _text_song_diff = love.graphics.newText( f_radio16 , "" )

local _text_infobox1 = love.graphics.newText( f_radio16 , "" )
local _text_infobox2 = love.graphics.newText( f_radio16 , "" )
local _text_infobox3 = love.graphics.newText( f_radio16 , "" )
local _text_infobox_item = love.graphics.newText( f_signika16 , "" )

local _text_quickinfo = love.graphics.newText( f_radio16 , "" )

local _text_partner = love.graphics.newText( f_signika16 , "" )
local _text_partner_name = love.graphics.newText( f_radio24 , "" )
local _text_partner_ability = love.graphics.newText( f_radio16 , "" )

local _text_start_button = love.graphics.newText( f_signika16 , "" )

local leaderboards = nil
local _sc = 0
local dim = 0.7

local box_status = 2
local background_shown = 1
local background_timer = 0
local background_time = 20
local selected_chart = 1

local song_selected = 1
local infobox_scroll = 0

local difficulty_colors = {
    {173/255, 255/255, 47/255},
    {173/255, 216/255, 230/255},
    {240/255, 128/255, 128/255},
    {221/255, 160/255, 221/255}
}

local _init = true
local SongList = {}

function songSelectGetList()
    SongList = table.kenum(table.dcopy(Database))
end

function songSelectGetSelectedSongData()
    return Database[SongList[#SongList-song_selected+1]]
end

function songSelectGetSelectedChart()
    return selected_chart
end

function songSelectMoveSelection(n, dt)
    local _current = song_selected
    if n ~= 0 or _init then
        song_selected = song_selected + n
        song_selected = math.max(1,math.min(#SongList,song_selected))
        if _current == song_selected then return end
        _current = song_selected
        songSelectSetPreviewDelay()
    end
end


function songSelectSetBoxStatus(n)
    box_status = n
end

function songSelectUpdateDisplay()
    if display_timer > display_time then _sc = _sc + 1 end
    if _sc > 32 then _sc = -32 end
end

function songSelectDrawList()
    local _margin = 8

    local _offset_x = 0
    local _offset_y = 0

    local _padding_y = 4
    local _padding_x = 8

    local _line_padding_x = 78
    local _scroll_step = - image.song_select.song_active:getHeight() - _margin
    local _move_x = 50

    for i=1, #SongList do
        local _pos_y = (i-1) * (_margin + image.song_select.song_active:getHeight())

        _offset_x = -_move_x
        _offset_y = image.song_select.header:getHeight() + _pos_y - (song_selected * _scroll_step)

        local _width = image.song_select.song_active:getWidth() - _line_padding_x - _move_x
        local _image = image.song_select.song_inactive
        if song_selected == #SongList-i+1 then 
            _width = image.song_select.song_active:getWidth() - _line_padding_x
            _offset_x = 0
            _image = image.song_select.song_active
            songSelectDrawInfo(Database[SongList[#SongList-song_selected+1]]) 
        end

        local _data = Database[SongList[i]]
        
        love.graphics.draw(
            _image,
            _offset_x,
            _offset_y
        )

        _text_song_name:setf(string.sub(_data.Title,1),game_width, "left")

        local _c = -_sc
        while _text_song_name:getWidth() > _width do
            _text_song_name:setf(string.sub(_data.Title, math.max(0,_sc+_c), #_data.Title-_c), game_width, "left")
            _c = _c + 1
        end
        
        love.graphics.draw(
            _text_song_name,
            _offset_x + image.song_select.song_active:getWidth() - _line_padding_x - _padding_x - _text_song_name:getWidth(),
            _offset_y + _padding_y
        )
        _text_song_artist:setf({c_main,_data.Artist},game_width,"left")
        love.graphics.draw(
            _text_song_artist,
            _offset_x + image.song_select.song_active:getWidth() - _line_padding_x - _padding_x - _text_song_artist:getWidth(),
            _offset_y + image.song_select.song_active:getHeight() - _padding_y - _text_song_artist:getHeight()
        )

        for j=1, #_data.ChartData do    
            local _padding = 4
            local _box_x = 32
            local _box_y = 16

            _text_song_diff:setf({difficulty_colors[j], _data.ChartData[j].Difficulty}, _box_x,"center")
            if j <= 2 then 
                love.graphics.draw(
                    _text_song_diff,
                    _offset_x + _padding + image.song_select.song_active:getWidth() - _line_padding_x + (_padding + _box_x) * (j-1),
                    _offset_y + _padding_y
                )
            elseif j <= 4 then
                love.graphics.draw(
                    _text_song_diff,
                    _offset_x + _padding + image.song_select.song_active:getWidth() - _line_padding_x + (_padding + _box_x) * (j-3),
                    _offset_y + _padding + _padding_y + _box_y
                )
            end
        end
    end
end

function songSelectDrawStartButton()

    local _margin_x = 24
    local _offset_x = image.song_select.song_active:getWidth() + image.song_select.infobox1:getWidth()/2 - image.song_select.start_button:getWidth()/2 + _margin_x
    local _offset_y = game_height - image.song_select.footer:getHeight() - image.song_select.start_button:getHeight()

    if songSelectGetSelectedSongData().SongData.SampleRate then
        love.graphics.draw(
            image.song_select.start_button,
            _offset_x, 
            _offset_y
        )
    else
        _text_start_button:setf("Preprocessing song data...", image.song_select.start_button:getWidth(), "center")
        love.graphics.draw(
            _text_start_button,
            _offset_x, 
            _offset_y
        )
    end
end

function songSelectDrawInfo(data)
    if data then
        local _margin_x = 24
        local _margin_y = -6

        local _padding_x = 24
        local _padding_y = 24

        local _text_padding = 8
        local _text_margin = 1
        
        local _quickinfo_icon = 36
        local _quickinfo_text = 81
        local _quickinfo_row1 = 36
        local _quickinfo_row = 40

        local _box1 = 111
        local _box2 = 139
        local _box3 = 97
        local _boxh = 36

        local image_w = 200
        local image_h = 200

        local _contenth = 142
        local _contentw = 349

        local _offset_x = image.song_select.song_active:getWidth()
        local _offset_y = image.song_select.header:getHeight()

        -- image
        if display_timer > display_time then background_timer = background_timer + 1 end
        
        if background_timer > background_time then 
            background_shown = background_shown + 1 
            background_timer = background_timer - background_time
        end

        if background_shown > #data.ChartData[selected_chart].Backgrounds then background_shown = 1 end

        if data.ChartData[selected_chart].Backgrounds[background_shown] then
            local _width = data.ChartData[selected_chart].Backgrounds[background_shown].img:getWidth()
            local _height = data.ChartData[selected_chart].Backgrounds[background_shown].img:getHeight()
        
            local _size = 1
            if _width < _height then
                _size = image_w/_width
            else
                _size = image_h/_height
            end

            if not data.ChartData[selected_chart].Backgrounds[background_shown].quad then
                local _px = 1
                local _off_x = 0
                local _off_y = 0
                if _width < _height then
                    _px = _width
                    _off_y = (_height - _px)/2
                else
                    _px = _height
                    _off_x = (_width - _px)/2
                end
                data.ChartData[selected_chart].Backgrounds[background_shown].quad = love.graphics.newQuad( _off_x, _off_y, _px, _px, _width, _height )
            end

            love.graphics.draw(
                data.ChartData[selected_chart].Backgrounds[background_shown].img,
                data.ChartData[selected_chart].Backgrounds[background_shown].quad,
                _offset_x + _margin_x,
                _offset_y + _margin_y,
                0,
                _size
            )
        end
        love.graphics.draw(
            image.song_select.image_container,
            _offset_x + _margin_x,
            _offset_y + _margin_y
        )

        -- quickinfo
        love.graphics.draw(
            image.song_select.quickinfo,
            _offset_x + _margin_x + image.song_select.infobox1:getWidth() - image.song_select.quickinfo:getWidth(),
            _offset_y + _margin_y
        )
        _text_quickinfo:setf( getTime(data.SongData.Duration), _quickinfo_text, "center") 
        love.graphics.draw(
            _text_quickinfo,
            _offset_x + _margin_x + image.song_select.infobox1:getWidth() - image.song_select.quickinfo:getWidth() + _quickinfo_icon,
            _offset_y + _margin_y + _text_padding + _text_margin 
        )
        _text_quickinfo:setf( data.ChartData[selected_chart].NoteCount, _quickinfo_text, "center") 
        love.graphics.draw(
            _text_quickinfo,
            _offset_x + _margin_x + image.song_select.infobox1:getWidth() - image.song_select.quickinfo:getWidth() + _quickinfo_icon,
            _offset_y + _margin_y + _text_padding + _text_margin + _quickinfo_row1
        )
        _text_quickinfo:setf( data.ChartData[selected_chart].StarCount, _quickinfo_text, "center") 
        love.graphics.draw(
            _text_quickinfo,
            _offset_x + _margin_x + image.song_select.infobox1:getWidth() - image.song_select.quickinfo:getWidth() + _quickinfo_icon,
            _offset_y + _margin_y + _text_padding + _text_margin + _quickinfo_row1 + (_text_margin + _quickinfo_row)
        )
        _text_quickinfo:setf( data.ChartData[selected_chart].MoonCount, _quickinfo_text, "center") 
        love.graphics.draw(
            _text_quickinfo,
            _offset_x + _margin_x + image.song_select.infobox1:getWidth() - image.song_select.quickinfo:getWidth() + _quickinfo_icon,
            _offset_y + _margin_y + _text_padding + _text_margin + _quickinfo_row1 + (_text_margin + _quickinfo_row)*2
        )
        _text_quickinfo:setf( data.ChartData[selected_chart].CometCount, _quickinfo_text, "center") 
        love.graphics.draw(
            _text_quickinfo,
            _offset_x + _margin_x + image.song_select.infobox1:getWidth() - image.song_select.quickinfo:getWidth() + _quickinfo_icon,
            _offset_y + _margin_y + _text_padding + _text_margin + _quickinfo_row1 + (_text_margin + _quickinfo_row)*3
        )
        

        -- infobox
        love.graphics.draw(
            image.song_select["infobox"..box_status],
            _offset_x + _margin_x,
            _offset_y + _margin_y + _padding_y + image.song_select.image_container:getHeight()
        )
        
            
        _text_infobox1:setf(getLocale("song_select.button.difficulty"), game_width, "left" )
        local _size = 1
        local _width = _text_infobox1:getWidth() + _text_padding*2
        if _box1 < _width then
            _size = _box1/_width
            _text_infobox1:setf(getLocale("song_select.button.difficulty"), _width, "center" )
        else
            _text_infobox1:setf(getLocale("song_select.button.difficulty"), _box1, "center" )
        end
        love.graphics.draw(
            _text_infobox1,
            _offset_x + _margin_x + _text_margin,
            _offset_y + _margin_y +_text_padding*1/_size + _padding_y + image.song_select.image_container:getHeight(),
            0,
            _size
        )

        _text_infobox2:setf(getLocale("song_select.button.leaderboard"), game_width, "left" )
        local _size = 1
        local _width = _text_infobox2:getWidth() + _text_padding*2
        if _box2 < _width then
            _size = _box2/_width
            _text_infobox2:setf(getLocale("song_select.button.leaderboard"), _width, "center" )
        else
            _text_infobox2:setf(getLocale("song_select.button.leaderboard"), _box2, "center" )
        end
        love.graphics.draw(
            _text_infobox2,
            _offset_x + _margin_x + _text_margin*2 + _box1,
            _offset_y + _margin_y + _text_padding*1/_size + _padding_y + image.song_select.image_container:getHeight(),
            0,
            _size
        )

        _text_infobox3:setf(getLocale("song_select.button.options"), game_width, "left" )
        local _size = 1
        local _width = _text_infobox3:getWidth() + _text_padding*2
        if _box3 < _width then
            _size = _box3/_width
            _text_infobox3:setf(getLocale("song_select.button.options"), _width, "center" )
        else
            _text_infobox3:setf(getLocale("song_select.button.options"), _box3, "center" )
        end
        love.graphics.draw(
            _text_infobox3,
            _offset_x + _margin_x + _text_margin*3 + _box1 + _box2,
            _offset_y + _margin_y + _text_padding*1/_size + _padding_y + image.song_select.image_container:getHeight(),
            0,
            _size
        )

        if box_status == 1 then
            local _text = {}
            for i=1, #data.ChartData do
                table.insert_batch(_text, {1,1,1}, "[")
                table.insert_batch(_text, difficulty_colors[i % 4 + 1], data.ChartData[i].Difficulty)
                table.insert_batch(_text, {1,1,1}, "] ")
                table.insert_batch(_text, difficulty_colors[i % 4 + 1], data.ChartData[i].Name)
                table.insert_batch(_text, {1,1,1}, "\n")
            end
            _text_infobox_item:setf( _text, game_width, "left") 
            love.graphics.draw(
                _text_infobox_item,
                _offset_x + _margin_x + _text_padding,
                _offset_y + _margin_y + _text_padding + _boxh + _padding_y + image.song_select.image_container:getHeight()
            )
        elseif box_status == 2 then
            if leaderboards then

            else
                _text_infobox_item:setf(getLocale("song_select.leaderboards.missing"), _contentw, "center")
                love.graphics.draw(
                    _text_infobox_item,
                    _offset_x + _margin_x,
                    _offset_y + _margin_y + _text_padding + _boxh + _contenth/2 + _padding_y + image.song_select.image_container:getHeight()
                )
            end 
        end
    end
end

function getTime(n)
    local _ms = n
    local _s = n/1000
    local _m = _s/60
    local _M = math.floor(_m)
    local _S = math.floor(_s - _M*60)

    return _M .. ":" .. _S 
end

function songSelectDrawHeader()
    local _offset_x = 0
    local _offset_y = 0
    
    love.graphics.draw(
        image.song_select.header,
        _offset_x,
        _offset_y,
        0,
        _size
    )

    local _offset_y = 0
    local _offset_x = game_width

    local _padding_x = 8
    local _padding_y = 12

    if #SongList == 1 then
        _text_header:setf(getLocale("song_select.header.song", {num = #SongList}),game_width,"left")
    else
        _text_header:setf(getLocale("song_select.header.songs", {num = #SongList}),game_width,"left")
    end

    love.graphics.draw(
        _text_header,
        _offset_x - _padding_x - _text_header:getWidth(),
        _offset_y + _padding_y
    )
end

function songSelectDrawFooter()
    local _offset_x = 0
    local _offset_y = game_height - image.song_select.footer:getHeight()
    
    love.graphics.draw(
        image.song_select.footer,
        _offset_x,
        _offset_y,
        0,
        _size
    )

    if networkmessage then
        songSelectDrawChatClue()
    end
end

function songSelectDrawChatClue()
    _text_footer:setf(networkmessage,game_width,"left")

    local _offset_x = 0
    local _offset_y = game_height
    local _padding_x = 8
    local _padding_y = 8
    love.graphics.draw(
        _text_footer,
        _offset_x + _padding_x,
        _offset_y - _padding_y - _text_footer:getHeight()
    )
end

function songSelectDrawBackground()
    local _preColor = pack(love.graphics.getColor())

    local _width = image.song_select.background:getWidth()
    local _height = image.song_select.background:getHeight()

    local _size = 1
    if _width > _height then
        _size = game_width/_width
    else
        _size = game_height/_height
    end
    love.graphics.draw(
        image.song_select.background,
        0,
        0,
        0,
        _size,
        _size,
        (_width*_size-game_width)/2,
        (_height*_size-game_height)/2
    )

    local _color = table.dcopy(c_bg)
    _color[4] = dim
     
    love.graphics.setColor(_color)
    love.graphics.rectangle(
        "fill",
        0,
        0,
        game_width,
        game_height
    )

    love.graphics.setColor(unpack(_preColor))
end

function songSelectDrawPartner(_partner)
    if _partner
    and _partner > 0 
    and _partner < #partner_cases then
        local _height = 360
        local _width = 240
        local _size = _height/image.partner[_partner]:getHeight()   
    
        _text_partner_name:setf(getLocale("character."..getPartnerName(_partner)..".name"),_width,"center")
        _text_partner:setf(getLocale("song_select.partner.prename"),_width,"center")

        local _margin_x = 0
        local _margin_y = -6

        local _offset_x = _margin_x + game_width - _width
        local _offset_y = _margin_y + image.song_select.header:getHeight()

        love.graphics.draw(
            image.song_select.partner_gradient,
            _offset_x,
            _offset_y
        )
        love.graphics.draw(
            _text_partner,
            _offset_x,
            _offset_y
        )
        love.graphics.draw(
            _text_partner_name,
            _offset_x,
            _offset_y + _text_partner:getHeight()
        )
        love.graphics.draw(
            image.partner[_partner],
            _offset_x,
            _offset_y + _text_partner:getHeight() + _text_partner_name:getHeight(),
            0,
            _size
        )
        _text_partner_ability:setf(getLocale("character."..getPartnerName(_partner)..".effect"),_width,"center")

        local _padding_y = 4

        love.graphics.draw(
            _text_partner_ability,
            _offset_x,
            _offset_y + _padding_y + _text_partner:getHeight() + _text_partner_name:getHeight() + image.partner[_partner]:getHeight()*_size
        )
    end
end