-- Partner List
-- 山羊座 "capricorn constellation" 
-- みや ほのか 【宮矢 ほのか】 "miya honoka"
-- おとお あずみ 【響尾 泉】 "otoo izumi"
-- みなみじゅうじ座 "crux constellation"¨
-- アリア "aria"
-- 永吉 瑚惺 "nagayoshi kosei"

local partner_names = {
    "capricorn",
    "crux",
    "leo",
    "pictor",
    "saggitarius",
    "virgo"
}

partner_cases = table.enum(table.dcopy(partner_names))

local current_partner = 5

image.partner = {}
image.partner[partner_cases.capricorn] = love.graphics.newImage("res/image/partners/full_body/capri.png")
image.partner[partner_cases.saggitarius] = love.graphics.newImage("res/image/partners/full_body/miya.png")
image.partner[partner_cases.virgo] = love.graphics.newImage("res/image/partners/full_body/otocchi.png")
image.partner[partner_cases.leo] = love.graphics.newImage("res/image/partners/full_body/aria.png")
image.partner[partner_cases.crux] = love.graphics.newImage("res/image/partners/full_body/crux.png")
image.partner[partner_cases.pictor] = love.graphics.newImage("res/image/partners/full_body/pictor.png")

function getCurrentPartner()
    return current_partner
end

function getPartnerName(_partner)
    _partner = _partner or current_partner
    return partner_names[_partner]
end

function isCurrentPartner(_partner)
    if current_partner == _partner then
        return true
    end
    return false
end 

function setPartnerUp()
    current_partner = current_partner + 1
    if current_partner > #partner_cases then current_partner = current_partner - #partner_cases - 1 end
end

function setPartnerDown()
    current_partner = current_partner - 1
    if current_partner < 0 then current_partner = current_partner + #partner_cases + 1 end
end