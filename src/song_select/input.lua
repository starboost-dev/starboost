local _offset_x = image.song_select.song_active:getWidth()
local _offset_y = image.song_select.header:getHeight()

local _margin_x = 24
local _margin_y = -6

local _padding_x = 24
local _padding_y = 24

local _text_padding = 8
local _text_margin = 1

local _box1 = 111
local _box2 = 139
local _box3 = 97
local _boxh = 36

difficulty_button = {
    x = _offset_x + _margin_x + _text_margin,
    y = _offset_y + _margin_y +_text_padding + _padding_y + image.song_select.image_container:getHeight(),
    w = _box1,
    h = _boxh
}

leaderboard_button = {
    x = _offset_x + _margin_x + _text_margin*2 + _box1,
    y = _offset_y + _margin_y + _text_padding + _padding_y + image.song_select.image_container:getHeight(),
    w = _box2,
    h = _boxh
}

settings_button = {
    x = _offset_x + _margin_x + _text_margin*3 + _box1 + _box2,
    y = _offset_y + _margin_y + _text_padding + _padding_y + image.song_select.image_container:getHeight(),
    w = _box3,
    h = _boxh
}

song_area = {
    x = 0,
    y = image.song_select.header:getHeight(),
    w = image.song_select.song_active:getWidth(),
    h = game_height - image.song_select.header:getHeight() - image.song_select.footer:getHeight() 
}

start_button = {
    x = image.song_select.song_active:getWidth() + image.song_select.infobox1:getWidth()/2 - image.song_select.start_button:getWidth()/2 + _margin_x,
    y = game_height - image.song_select.footer:getHeight() - image.song_select.start_button:getHeight(),
    w = image.song_select.start_button:getWidth(),
    h = image.song_select.start_button:getWidth()
}


function songSelectInput(dt)
    local _pos = pack(love.mouse.getPosition())

    local _scroll = 0
    if mousewheelinput > 0 then
        _scroll = -1
    elseif mousewheelinput < 0 then
        _scroll = 1
    end

    mousewheelinput = 0

    if checkPointInArea(_pos, song_area) then
        songSelectMoveSelection(_scroll, dt)
    end

    if checkPointInArea(_pos, difficulty_button) then
        if love.mouse.isDown(1) then
            songSelectSetBoxStatus(1)
        end
    end
    
    if checkPointInArea(_pos, leaderboard_button) then
        if love.mouse.isDown(1) then
            songSelectSetBoxStatus(2)
        end
    end
    if checkPointInArea(_pos, settings_button) then
        if love.mouse.isDown(1) then
            songSelectSetBoxStatus(3)
        end
    end
    if checkPointInArea(_pos, start_button) then
        if songSelectGetSelectedSongData().SongData.SampleRate then
            if love.mouse.isDown(1) then
                local _data = songSelectGetSelectedSongData()
                local _chart_num = songSelectGetSelectedChart()
                loadDatabaseMap(
                    _data,
                    _chart_num
                )
                CurrentBackground = _data.ChartData[_chart_num].Backgrounds[1]
                game_state = game_state_cases.ingame
                songSelectStopPreview()
                gameplayPrepareSong(_data, _chart_num)
            end
        end
    end
end

function checkPointInArea(point, area)
    if point[1] > area.x 
    and point[1] < area.x + area.w
    and point[2] > area.y
    and point[2] < area.y + area.h then
        return true
    end
end