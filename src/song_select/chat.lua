function parseChatMessage(_message)
    local _data = string.gmatch(_message, "\t([^\t]+)")
    local _nick = _data()
    local _text = _data()
    local _user = _data()

    local _display_name = "*".._nick
    if _user == _nick then
        _display_name = _user
    end
    networkmessage = "CHAT | " .. _display_name .. ": " .. _text  
end