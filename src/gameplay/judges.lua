local judge_size = 1
local judge_message = nil
local judge_message_time = 1
local judge_message_fadeout = 2
local judge_message_timer = 0
local combo_message = 0

local judgements = {}

function gameplayResetJudgement()
    judgements = {}
end

function listJudgement(_ms)
    while #judgements >= 25 do
        table.remove(judgements,1)
    end
    table.insert(judgements, #judgements+1, _ms)
end

function judge(_ms)
    listJudgement(_ms)
    local _ms = math.abs(_ms) 
    local _judgement = ""
    local _combo = false

    if _ms <= judge_timing.exact then
        _judgement = "exact" 
        _combo = true       
    elseif _ms <= judge_timing.absolute then
        _judgement = "absolute"        
        _combo = true
    elseif _ms <= judge_timing.fragment then
        _judgement = "fragment"
        _combo = true
    elseif _ms <= judge_timing.collapse then
        _judgement = "collapse"
    elseif _ms > judge_timing.collapse then
        _judgement = "loss"
    end
    gameplayCount(_judgement, _combo)
    setJudgeMessage(_judgement, count_combo)
end

function setJudgeMessage(_judge,_combo)
    judge_message = image.gameplay["judge_".._judge]
    judge_message_timer = judge_message_time + judge_message_fadeout
    judge_size = 1.25
end

function gameplayUpdateJudgeMessage(dt)
    -- judge message size
    if judge_size > 1 then 
        judge_size = judge_size - 2 * dt
    end
    judge_size = math.max(judge_size, 1 )

    -- judge mesage time on screen
    judge_message_timer = judge_message_timer - dt
    if judge_message_timer and judge_message_timer <= 0 then 
        judge_message = nil
    end
end

function gameplayDrawJudge()
    -- draws the UI message on editor screen
    local _preColor = pack(love.graphics.getColor())

    local _judge = judge_message
    local _combo = combo_message

    if _judge then
        local _stage_w = 72*5
        local _width = _judge:getWidth()  
        local _height = _judge:getHeight()  

        local _size = judge_size*(_stage_w)/_width

        local _offset_x = game_width/2 - (_width*_size)/2 
        local _offset_y = (game_height-image.playfield.stage_bottom:getHeight())/2-_height/2
    
        love.graphics.setColor(1,1,1,judge_message_timer/judge_message_fadeout)
        love.graphics.draw(
            _judge,
            _offset_x,
            _offset_y,
            0,
            _size
        )
    end
    if _combo > 2 then
        local _message = love.graphics.newText(f_combo, tostring(_combo))
        
        local _stage_w = 72*5
        local _width = _message:getWidth()  
        local _height = _message:getHeight()  

        local _size = (_stage_w)/_width

        local _offset_x = game_width/2 - (_width*_size)/2 
        local _offset_y = (game_height-image.playfield.stage_bottom:getHeight())/2-_height/2
    
        love.graphics.setColor(unpack(c_main))
        love.graphics.draw(
            _message,
            _offset_x,
            _offset_y,
            0,
            _size
        )

    end
    love.graphics.setColor(unpack(_preColor))
end

function gameplayDrawAccuracy()
    local _preColor = pack(love.graphics.getColor())
    
    local _anchor_x = game_width/2
    local _anchor_y = game_height-image.playfield.stage_bottom:getHeight()/2+3
    
    local _offset_x = _anchor_x - image.playfield.judgement_line:getWidth()/2
    local _offset_y = _anchor_y

    love.graphics.setColor(1,1,1,0.3)

    local _range = judge_timing.collapse*2
    local _ratio = image.playfield.judgement_line:getWidth()/(_range)

    for i=1, #judge_coloring do
        local _color = judge_coloring[i].color
        _color[4] = 0.3

        love.graphics.setColor(unpack(_color))

        love.graphics.rectangle(
            "fill",
            _offset_x + (_range/2 - judge_coloring[i].ms) *_ratio,
            _offset_y - 10,
            judge_coloring[i].ms * _ratio,
            10
        )
        love.graphics.rectangle(
            "fill",
            _offset_x + (_range/2) *_ratio,
            _offset_y - 10,
            judge_coloring[i].ms * _ratio,
            10
        )
    end

    love.graphics.setColor(1,1,1,1)
    for i=1, #judgements do
        local _ms = math.max(-_range/2,math.min(_range/2,judgements[i]))
        love.graphics.line(
            _offset_x + (_range/2 -_ms) *_ratio,
            _offset_y - 10,
            _offset_x + (_range/2 -_ms) *_ratio,
            _offset_y + 10
        )
    end

    love.graphics.setColor(unpack(_preColor))
end