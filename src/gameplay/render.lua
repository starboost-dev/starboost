local standard_star_color = {123/255, 109/255, 232/255}
local standard_star_color = {27/255, 0/255, 232/255}

local render_distance = game_render
local lower_render_distance = game_render/10

-- draws all the gameplay elements
function gameplayDraw() 
    local _steps = 0
    local _chart = LoadedChartData.IngameMap
    if game_state == game_state_cases.editor then 
        _chart = LoadedChartData.Map
    end

    render_index = render_index or 1
    for i=render_index, #_chart do
        _steps = _steps + 1 

        local _first_index = nil

        local data = _chart[i]
        local _ms = data.ms     
        local _render_by = data.render_by or 0     
        
        if game_state == game_state_cases.editor then
            data.spin = 0
            data.velocity = 1
        end
        data.velocity = data.velocity or 1  
            
        local _speed = scroll_speed * data.velocity
        
        if not (_ms > song_time + (render_distance) * 1/_speed)  
        and _ms > song_time  - _render_by - (lower_render_distance) * 1/_speed then
            if not _first_index then _first_index = i end

            local _hit = data.hit
            local _command = replaceChartVariables(data.command)

            local _next_element = string.gmatch(_command,"%s(%S+)")  
            
            if string.match(_command, object.background) then
                local _file = tonumber(_next_element())
                
                if song_time >= _ms then
                    if _file == "none" then
                        CurrentBackground = nil
                    elseif LoadedChartData.Backgrounds[_file]
                    and not _hit then
                        data.hit = true
                        CurrentBackground = LoadedChartData.Backgrounds[_file]
                    end
                else
                    data.hit = false
                end
            elseif string.match(_command, object.foreground) then
                local _file = tonumber(_next_element())
                local _x = tonumber(_next_element() or nil)
                local _y = tonumber(_next_element() or nil)
                if song_time > _ms then
                    if LoadedChartData.Foregrounds[_file]
                    and not _hit then
                        data.hit = true
                        if _x and _y then
                            LoadedChartData.Foregrounds[_file].status = true
                            LoadedChartData.Foregrounds[_file].x = _x
                            LoadedChartData.Foregrounds[_file].y = _y
                        else
                            if LoadedChartData.Foregrounds[_file].status then
                                LoadedChartData.Foregrounds[_file].status = nil
                            else
                                LoadedChartData.Foregrounds[_file].status = true
                            end
                        end
                    end
                else
                    data.hit = false
                end
            elseif string.match(_command,object.star) then
                if not data.hit then 
                    local _lane = tonumber(_next_element())

                    gameplayDrawStar(_ms, _lane, data) 
                end
            elseif string.match(_command,object.moon) then
                if not data.hit then 
                    local _lane = tonumber(_next_element())

                    gameplayDrawMoon(_ms, _lane, data) 
                end
            elseif string.match(_command,object.comet) then
                if not data.gone_true then
                    local _ms2 = tonumber(_next_element())
                    local _x1 = tonumber(_next_element())
                    local _x2 = tonumber(_next_element())
                    local _mode = _next_element()
                    local _style = _next_element()

                    if _mode == "line" then
                        gameplayDrawCometLine(_ms, _ms2, _x1, _x2, data, _mode, _style)
                    else
                        if _style == "bezier_curve_top"
                        or _style == "bezier_curve_bottom" then
                            gameplayDrawCometBezier(_ms, _ms2, _x1, _x2, data, _mode, _style)
                        else
                            gameplayDrawCometLine(_ms, _ms2, _x1, _x2, data)
                        end
                        gameplayDrawCometStart(_ms, _ms2, _x1, _x2, data, _mode)
                        gameplayDrawCometEnd(_ms, _ms2, _x1, _x2, data)
                    end
                end
            end
        end
    end 
end

function gameplayDrawCometBezier( _ms, _ms2, _from, _to, _data, _mode, _style)
    local _preColor = pack(love.graphics.getColor())

    local _color = _data.color or standard_star_color 
    local _velocity = _data.velocity or 1

    local _offset_x = game_width/2
    local _offset_y = stage_h
    local _lane_w = 72
    local _column_w = _lane_w/2
    local _speed = scroll_speed * _velocity

    local _relative_factor = stage_h/1000 * _speed

    local _startx = _column_w * _from
    local _starty = (song_time - song_delay - _ms) * _relative_factor
    local _endx = _column_w * _to
    local _endy = (song_time - song_delay - _ms - _ms2) * _relative_factor
    local _w = _endx - _startx
    local _h = _endy - _starty
    
    local _x1, _x2, _x3, _y1, _y2, _y3, _curve, _points
    if _style == "bezier_curve_bottom" then
        _x1 = _startx
        _y1 = _starty
        _x2 = _startx + _w
        _y2 = _starty
        _x3 = _endx
        _y3 = _endy
    elseif _style == "bezier_curve_top" then
        _x1 = _startx
        _y1 = _starty
        _x2 = _startx
        _y2 = _starty + _h
        _x3 = _endx
        _y3 = _endy
    end
    _curve = love.math.newBezierCurve( _offset_x+_x1, _offset_y+_y1, _offset_x+_x2, _offset_y+_y2, _offset_x+_x3, _offset_y+_y3 )
    _points = _curve:render(5)

    love.graphics.setLineJoin("miter")
    love.graphics.setLineStyle("smooth")
    if _mode ~= "line" then
        love.graphics.setColor(1,1,1)
        love.graphics.setLineWidth(7)
        love.graphics.line(_points)
    end

    love.graphics.setColor(unpack(_color))
    love.graphics.setLineWidth(4)
    love.graphics.line(_points)

    if _mode ~= "line"
    and song_time > _ms and song_time < _ms+_ms2 then
        love.graphics.setColor(1,1,1)
        
        local _t =  1/(_ms2/(song_time-_ms))
        local _x, _y = _curve:evaluate(_t)
        local _size = 1
        local _width = image.gameplay.comet_shine:getWidth()
        local _height = image.gameplay.comet_shine:getHeight()

        love.graphics.print(_t, 300)
        love.graphics.draw(
            image.gameplay.comet_shine,
            _x,
            _y,
            0,
            _size,
            _size,
            _width/2,
            _height/2
        )
    end

    love.graphics.setColor(unpack(_preColor))
end

function gameplayDrawCometLine(_ms, _ms2, _from, _to, _data, _mode, _style)
    local _preColor = pack(love.graphics.getColor())

    local _ms = _ms
    local _ms2 = _ms2

    local _color = _data.color or standard_star_color 
    local _velocity = _data.velocity or 1
    
    local _offset_x = game_width/2
    local _offset_y = stage_h
    local _lane_w = 72
    local _column_w = _lane_w/2
    local _speed = scroll_speed * _velocity
    
    local _relative_factor = stage_h/1000 * _speed

    local _position1 = (song_time - song_delay - _ms) * _relative_factor
    local _position2 = (song_time - song_delay - _ms - _ms2) * _relative_factor
    
    -- render comet
    love.graphics.setLineJoin("miter")
    love.graphics.setLineStyle("smooth")

    if _mode ~= "line" then
        love.graphics.setColor(1,1,1)
        love.graphics.setLineWidth(7)
        love.graphics.line(
            _offset_x + _column_w * _from,  _offset_y + _position1,
            _offset_x + _column_w * _to,  _offset_y + _position2
        )
    end
    love.graphics.setColor(unpack(_color))
    love.graphics.setLineWidth(4)
    love.graphics.line(
        _offset_x + _column_w * _from,  _offset_y + _position1,
        _offset_x + _column_w * _to,  _offset_y + _position2
    )
    
    if _mode ~= "line"
    and song_time > _ms and song_time < _ms+_ms2 then 
        local _color = _data.color or standard_star_color 
        local _velocity = _data.velocity or 1

        local _offset_x = game_width/2
        local _offset_y = stage_h
        local _lane_w = 72
        local _column_w = _lane_w/2
        local _speed = scroll_speed * _velocity
        
        local _relative_factor = stage_h/1000 * _speed

        local _position = _ms * _relative_factor
        local _position1 = (song_time - song_delay - _ms) * _relative_factor
        local _position2 = (song_time - song_delay - _ms - _ms2) * _relative_factor

        local _distance_x = _column_w * _to - _column_w * _from
        local _distance_y = _position2 - _position1
        local _distance = math.sqrt(_distance_x*_distance_x + _distance_y*_distance_y) 

        local _step_size = 18/image.gameplay.comet_line_color:getWidth()
        local _step_width = _step_size * image.gameplay.comet_line_color:getWidth() 
        local _size = 1
        local _width = image.gameplay.comet_shine:getWidth()
        local _height = image.gameplay.comet_shine:getHeight()
        local _angle = angle(_distance_x,_distance_y)
        local _n = math.floor(_distance/_step_width)
        local i = (song_time-_ms)/_ms2

        love.graphics.setColor(1,1,1)
        love.graphics.draw(
            image.gameplay.comet_shine,
            _offset_x + _column_w * _from + math.cos(_angle) * (i * _step_width * _n),
            _offset_y,
            0,
            _size,
            _size,
            _width/2,
            _height/2
        )
    end
    love.graphics.setColor(unpack(_preColor))
end

function gameplayDrawCometStart(_ms, _ms2, _from, _to, _data, _mode)
    local _preColor = pack(love.graphics.getColor())

    local _ms = _ms
    local _mode = _mode or "normal"

    local _img = image.gameplay.comet_border1
    local _img_color = image.gameplay.comet_color1
    if _mode == "hold" then
        _img = image.gameplay.comet_border2
        _img_color = image.gameplay.comet_color2
    end

    local _color = _data.color or standard_star_color 
    local _spin = _data.spin or 1
    local _velocity = _data.velocity or 1

    local _start_offset_x = _img:getWidth()
    local _start_offset_y = _img:getHeight()

    local _offset_x = game_width/2
    local _offset_y = stage_h
    local _lane_w = 72
    local _column_w = _lane_w/2
    local _speed = scroll_speed * _velocity
    
    local _relative_factor = stage_h/1000 * _speed

    local _position = (song_time - song_delay - _ms) * _relative_factor
    
    local _rotate_ms = song_time - song_delay - _ms
    local _rotate_speed = math.rad(360) * _relative_factor * _spin
    
    -- render comet
    love.graphics.setColor(unpack(_color))

    love.graphics.draw(
        _img_color,
        _offset_x + _column_w * _from,
        _offset_y + _position,
        _rotate_speed * _rotate_ms/1000,
        1, 1,   
        _start_offset_x/2, _start_offset_y/2
    )

    love.graphics.setColor(1,1,1,1)

    love.graphics.draw(
        _img,
        _offset_x + _column_w * _from,
        _offset_y + _position,
        _rotate_speed * _rotate_ms/1000,
        1, 1,   
        _start_offset_x/2, _start_offset_y/2
    )

    love.graphics.setColor(unpack(_preColor))
end


function gameplayDrawCometEnd(_ms, _ms2, _from, _to, _data)
    local _preColor = pack(love.graphics.getColor())

    local _ms = _ms
    local _ms2 = _ms2

    local _color = _data.color or standard_star_color 
    local _spin = _data.spin or 1
    local _velocity = _data.velocity or 1
    
    local _end_offset_x = image.gameplay.comet_border2:getWidth()
    local _end_offset_y = image.gameplay.comet_border2:getHeight()

    local _offset_x = game_width/2
    local _offset_y = stage_h
    local _lane_w = 72
    local _column_w = _lane_w/2
    local _speed = scroll_speed * _velocity
    
    local _relative_factor = stage_h/1000 * _speed

    local _position = (song_time - song_delay - _ms - _ms2) * _relative_factor
    
    local _rotate_ms = song_time - song_delay - _ms - _ms2
    local _rotate_speed = math.rad(360) * _relative_factor * _spin
    
    -- render comet
    love.graphics.setColor(unpack(_color))

    love.graphics.draw(
        image.gameplay.comet_color2,
        _offset_x + _column_w * _to,
        _offset_y + _position,
        _rotate_speed * _rotate_ms/1000,
        1, 1,   
        _end_offset_x/2, _end_offset_y/2
    )
    
    love.graphics.setColor(1,1,1,1)
    
    love.graphics.draw(
        image.gameplay.comet_border2,
        _offset_x + _column_w * _to,
        _offset_y + _position,
        _rotate_speed * _rotate_ms/1000,
        1, 1,   
        _end_offset_x/2, _end_offset_y/2
    )

    love.graphics.setColor(unpack(_preColor))
end



function gameplayDrawStar(_ms,_lane,_data)
    local _preColor = pack(love.graphics.getColor())

    local _ms = _ms
    local _lane = _lane
    local _color = _data.color or standard_star_color 
    local _spin = _data.spin or 1
    local _velocity = _data.velocity or 1
    local _alpha = _data.opacity
    _color[4] = _alpha

    local _note_offset_x = image.gameplay.star_border:getWidth()
    local _note_offset_y = image.gameplay.star_border:getHeight()

    local _speed = scroll_speed * _velocity
    local _relative_factor = stage_h/1000 * _speed
    local _position = (song_time - song_delay - _ms) * _relative_factor

    local _rotate_ms = _ms - song_time - song_delay
    local _rotate_speed = math.rad(360) * _relative_factor * _spin

    local _offset_x = game_width/2 - image.playfield.stage_sides:getWidth()/2 + 2
    local _offset_y = stage_h

    -- render stars
    love.graphics.setColor(unpack(_color))
    love.graphics.draw(
        image.gameplay.star_color,
        _offset_x + _note_offset_x/2 + 1 + (_lane-1) * lane_w + (_lane-1),
        _offset_y - _note_offset_y/2 + _position ,
        _rotate_speed * _rotate_ms/1000,
        1, 1,   
        _note_offset_x/2, _note_offset_y/2
    )
    love.graphics.setColor(1,1,1,_alpha)
    love.graphics.draw(
        image.gameplay.star_border,
        _offset_x + _note_offset_x/2 + (_lane-1) * lane_w + (_lane-1),
        _offset_y - _note_offset_y/2 + _position,
        _rotate_speed * _rotate_ms/1000,
        1, 1,
        _note_offset_x/2, _note_offset_y/2
    )
    
    love.graphics.setColor(unpack(_preColor))
end

-- draws a note
function gameplayDrawMoon(_ms,_lane,_data)
    local _preColor = pack(love.graphics.getColor())

    local _lane = _lane or 1
    local _width = _data.width or 2
    local _color = _data.color or standard_star_color 
    local _rotation = _data.rotation or 1
    local _velocity = _data.velocity or 1
    local _alpha = _data.opacity
    table.insert(_color,4,_alpha)


    local _note_offset_x = image.gameplay.moon_border1:getWidth()
    local _note_offset_y = image.gameplay.moon_border1:getHeight()

    local _moonwidth1 = image.gameplay.moon_border1:getWidth()
    local _moonwidth2 = image.gameplay.moon_border2:getWidth()
    local _speed = scroll_speed * _velocity

    local _relative_factor = stage_h/1000 * _speed
    local _position = (song_time - song_delay - _ms) * _relative_factor

    local _rot_offset = -lane_w * 3
    local _mrotstart =  -45 * _rotation

    local _isleft = 1.
    local _mooncenter1 = _rot_offset
    local _mooncenter2 = -_moonwidth1 + _rot_offset
    local _mooncenter3 = -_moonwidth2 * (_width - 2) + _rot_offset - _moonwidth1

    if _lane >= 3 then
        _rot_offset, 
        _mrotstart,
        _isleft = -_rot_offset, -_mrotstart, 2

        --_mooncenter1,_mooncenter3 = -_mooncenter3 + _moonwidth1, _mooncenter1 + _moonwidth1
        _mooncenter1 = _moonwidth2 * (_width - 2) + _rot_offset + _moonwidth1*2
        _mooncenter2 = _moonwidth1 + _rot_offset
        _mooncenter3 = _mooncenter2
    end

    local _rotate_ms = _ms - song_time - song_delay
    local _rotate_speed = math.rad(360) * _relative_factor * _rotation
    local _rotate_moon = (math.rad(_mrotstart) * _relative_factor) * (_ms - song_time - song_delay)/1000
    
    local _offset_x = game_width/2 - image.playfield.stage_sides:getWidth()/2 + 2
    local _offset_y = stage_h

    -- section 1
    love.graphics.setColor(unpack(_color))
    love.graphics.draw(
        image.gameplay.moon_color1,
        _offset_x + _mooncenter1 + (_lane-1) * lane_w + _width/2 + (_lane-1),
        _offset_y - _note_offset_y/2 + _position,
        _rotate_moon * _rotate_ms/1000,
        1, 1,
        _mooncenter1, _note_offset_y/2
    )
    love.graphics.setColor(1,1,1,_alpha)
    love.graphics.draw(
        image.gameplay.moon_border1,
        _offset_x + _mooncenter1 + (_lane - 1) * lane_w + _width/2 + (_lane-1),
        _offset_y - _note_offset_y/2 + _position,
        _rotate_moon * _rotate_ms/1000,
        1, 1,
        _mooncenter1, _note_offset_y/2
    )
    
    -- section 2
    for i=1, _width-2 do
        love.graphics.setColor(unpack(_color))
        love.graphics.draw(
            image.gameplay.moon_color2,
            _offset_x - (_moonwidth2 * (i - _isleft) - _mooncenter2) + (_lane + i - 1) * lane_w + _width/2 + (_lane-1),
            _offset_y - _note_offset_y/2 + _position,
            _rotate_moon * _rotate_ms/1000,
            1, 1,
            -(_moonwidth2 * (i - _isleft) - _mooncenter2), _note_offset_y/2
        )
        love.graphics.setColor(1,1,1,_alpha)
        love.graphics.draw(
            image.gameplay.moon_border2,
            _offset_x - (_moonwidth2 * (i - _isleft) - _mooncenter2) + (_lane + i - 1) * lane_w + _width/2 + (_lane-1),
            _offset_y - _note_offset_y/2 + _position,
            _rotate_moon * _rotate_ms/1000,
            1, 1,
            -(_moonwidth2 * (i - _isleft) - _mooncenter2), _note_offset_y/2
        )
    end
    
    -- section 3
    love.graphics.setColor(unpack(_color))
    love.graphics.draw(
        image.gameplay.moon_color3,
        _offset_x + _mooncenter3 + (_lane + _width - 2) * lane_w + _width/2 + (_lane-1),
        _offset_y - _note_offset_y/2 + _position,
        _rotate_moon * _rotate_ms/1000,
        1, 1,
        _mooncenter3, _note_offset_y/2
    )
    love.graphics.setColor(1,1,1,_alpha)
    love.graphics.draw(
        image.gameplay.moon_border3,
        _offset_x + _mooncenter3 + (_lane + _width - 2) * lane_w + _width/2 + (_lane-1),
        _offset_y - _note_offset_y/2 + _position,
        _rotate_moon * _rotate_ms/1000,
        1, 1,
        _mooncenter3, _note_offset_y/2
    )

    love.graphics.setColor(unpack(_preColor))
end

function gameplayDrawShineAnimation()
    local _preColor = pack(love.graphics.getColor())

    local _offset_x = game_width/2 - image.playfield.stage_sides:getWidth()/2
    local _offset_y = stage_h

    for i=1, #shine_anim_holder do
        local _anim = shine_anim_holder[i]
        local _lane = _anim.lane
        local _color = _anim.color or {1,1,1}

        love.graphics.setColor(unpack(_color))
    
        local _x = _offset_x + 2 + _lane-1 + (_lane-1)*lane_w
        local _y = _offset_y - _anim.height

        drawAnimation(
            _anim,
            _offset_x + 2 + _lane-1 + (_lane-1)*lane_w,
            _offset_y - _anim.height
        )
    end
    love.graphics.setColor(unpack(_preColor))
end

function GameplayDrawPause()
    love.graphics.draw(image.gameplay.pause_overlay)
end