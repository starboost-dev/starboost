function gameplayTogglePartnerEffect()
    gameplayToggleConsistencyChallenge(isCurrentPartner(partner_cases.capricorn))
    gameplayToggleGhostPlayfield(isCurrentPartner(partner_cases.crux))
    gameplayTogglePraise(isCurrentPartner(partner_cases.leo))
    gameplayToggleMonochromia(isCurrentPartner(partner_cases.pictor))
    gameplayToggleAccuracyChallenge(isCurrentPartner(partner_cases.saggitarius))
    gameplayToggleFadeIn(isCurrentPartner(partner_cases.virgo))
end

function gameplayTogglePraise(_partner) end
function gameplayToggleMonochromia(_partner) end
function gameplayToggleAccuracyChallenge(_partner) end
function gameplayToggleFadeIn(_partner) end