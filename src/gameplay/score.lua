local performance_count = {}
performance_count.combo = 0   
performance_count.loss = 0
performance_count.collapse = 0
performance_count.fragment = 0  
performance_count.absolute = 0
performance_count.exact = 0

local score_case = 5
local score_cases = table.enum {
    "additive",
    "subtractive", 
    "percentatge_additive",
    "percentatge_subtractive",
    "percentatge_dynamic"
}


local score_factor = {}
score_factor.loss = 0
score_factor.collapse = 0
score_factor.fragment = 1/2
score_factor.absolute = 1
score_factor.exact = 1

local score_value = {}
score_value.loss = 0
score_value.collapse = 1
score_value.fragment = 0
score_value.absolute = 0
score_value.exact = 1

local score_goal = 1000000
local score_display = nil
local score_display_string = ""

local consistency_challenge = false

function gameplayToggleConsistencyChallenge(bool)
    consistency_challenge = bool
end

function gameplayCount(_type, _combo)
    performance_count[_type] = performance_count[_type] + 1 
    if _combo then 
        performance_count.combo = performance_count.combo + 1
    else
        performance_count.combo = 0
    end 

    if consistency_challenge 
    and _type == "loss" then
        error("You missed, so bad!")
    end
end

function gameplayResetCounts()
    for _type in pairs(performance_count) do
        performance_count[_type] = 0
    end
end

function gameplayDrawCounts()
    _display = score_display_string or 0
    gameplayDrawScore(_display)
    if false then
        local _offset_x = 700
        local _offset_y = 50
        local _print = {}
        local _total = 0
        for _type in pairs(score_factor) do
            _total = _total + performance_count[_type]
        end
        table.insert_batch(_print, {1,1,1}, "debug: " .. debug_text) 
        table.insert_batch(_print, {1,1,1}, "\n[COMBO] " .. performance_count.combo)
        table.insert_batch(_print, {1,1,1}, "\n")
        table.insert_batch(_print, {1,1,1}, "\n[TOTAL] " .. _total)
        for _type in pairs(score_factor) do
            table.insert_batch(_print, {1,1,1}, "\n[" .. _type .. "] ".. performance_count[_type])
        end

        love.graphics.print(
            _print,
            f_game,
            _offset_x,
            _offset_y
        )
    end
end

function calculateScore(_dt)

    local _display = score_display
    local _score = 0 
    local _note_factor = 0

    if score_case == score_cases.additive then
        _note_factor = score_goal/LoadedChartData.NoteCount
        _score = 0 
        _display = _display or _score
        for judge, value in pairs(score_factor) do
            _score = _score + performance_count[judge] * _note_factor * ( value)  + score_value[judge]
        end
    elseif score_case == score_cases.subtractive then
        _note_factor = score_goal/LoadedChartData.NoteCount
        _score = score_goal + LoadedChartData.NoteCount
        _display = _display or _score
        for judge, value in pairs(score_factor) do
            _score = _score - performance_count[judge] * _note_factor  * (score_factor["exact"] - value) + score_value[judge]
        end
    elseif score_case == score_cases.percentatge_additive then
        _note_factor = 100/LoadedChartData.NoteCount
        _score = 0
        _display = _display or _score
        for judge, value in pairs(score_factor) do
            _score = _score + performance_count[judge] * _note_factor * (value) 
        end
    elseif score_case == score_cases.percentatge_subtractive then
        _note_factor = 100/LoadedChartData.NoteCount
        _score = 100
        _display = _display or _score
        for judge, value in pairs(score_factor) do
            _score = _score - performance_count[judge] * _note_factor * (1-value) 
        end
    elseif score_case == score_cases.percentatge_dynamic then
        local _count = 0 
        _note_factor = 100
        _score = 0
        _display = _display or _score
        for judge, value in pairs(score_factor) do
            _count = _count + performance_count[judge] * _note_factor
            _score = _score + performance_count[judge] * _note_factor * (value) 
        end
        _score = 100 * _score/_count
        if _count == 0 then _score = 100 end
    end
    if _display ~= _score then
        local _diff = _score - _display
        if math.abs(_diff) > _note_factor/10 then
            _display = _display + _diff * 3 * _dt
        else
            _display = _score
        end
    end
    
    if score_case == score_cases.additive
    or score_case == score_cases.subtractive then
        game_score = math.floor(_score)
        score_display = math.floor(_display)
        score_display_string = score_display

    elseif score_case == score_cases.percentatge_additive
        or score_case == score_cases.percentatge_subtractive
        or score_case == score_cases.percentatge_dynamic then
        game_score = math.floor(_score*100)/100
        score_display = _display
        score_display_string = string.format("%.2f",score_display).."%"   
             
    end
end

function gameplayDrawScore(score)
    local _preColor = pack(love.graphics.getColor())

    local _size = 1
    while string.len(score) < string.len(score_goal)-1 do
        score = 0 .. score
    end

    local _digit_w = 45 * _size
    local _offset_y = 0
    local _offset_x = game_width - (string.len(score)) * _digit_w  - 60

    for i=1, string.len(score) do
        local _digit = string.sub(score,i,i)
        love.graphics.draw(
            image.gameplay.score_number[_digit] or image.gameplay.score_number[0],
            _offset_x + i*_digit_w,
            _offset_y,  
            0,
            _size
        )
    end

    love.graphics.setColor(unpack(_preColor))
end
