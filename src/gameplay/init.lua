image.gameplay = {}
image.gameplay.judge_loss = love.graphics.newImage("res/image/judge_loss.png")
image.gameplay.judge_collapse = love.graphics.newImage("res/image/judge_collapse.png")
image.gameplay.judge_fragment = love.graphics.newImage("res/image/judge_fragment.png")
image.gameplay.judge_absolute = love.graphics.newImage("res/image/judge_absolute.png")
image.gameplay.judge_exact = love.graphics.newImage("res/image/judge_exact.png")

image.gameplay.lane_glow = love.graphics.newImage("res/image/lane_glow.png")
image.gameplay.side_glow_left = love.graphics.newImage("res/image/side_glow_left.png")
image.gameplay.side_glow_right = love.graphics.newImage("res/image/side_glow_right.png")

image.gameplay.pause_overlay = love.graphics.newImage("res/image/pause_overlay.png")

image.gameplay.score_number = {}
image.gameplay.score_number["0"] = love.graphics.newImage("res/image/score_number/0.png")
image.gameplay.score_number["1"] = love.graphics.newImage("res/image/score_number/1.png")
image.gameplay.score_number["2"] = love.graphics.newImage("res/image/score_number/2.png")
image.gameplay.score_number["3"] = love.graphics.newImage("res/image/score_number/3.png")
image.gameplay.score_number["4"] = love.graphics.newImage("res/image/score_number/4.png")
image.gameplay.score_number["5"] = love.graphics.newImage("res/image/score_number/5.png")
image.gameplay.score_number["6"] = love.graphics.newImage("res/image/score_number/6.png")
image.gameplay.score_number["7"] = love.graphics.newImage("res/image/score_number/7.png")
image.gameplay.score_number["8"] = love.graphics.newImage("res/image/score_number/8.png")
image.gameplay.score_number["9"] = love.graphics.newImage("res/image/score_number/9.png")
image.gameplay.score_number["%"] = love.graphics.newImage("res/image/score_number/%.png")
image.gameplay.score_number["."] = love.graphics.newImage("res/image/score_number/..png")

image.gameplay.star_color = love.graphics.newImage("res/image/star_color.png")
image.gameplay.star_border = love.graphics.newImage("res/image/star_border.png")

image.gameplay.moon_color1 = love.graphics.newImage("res/image/moon_color1.png")
image.gameplay.moon_color2 = love.graphics.newImage("res/image/moon_color2.png")
image.gameplay.moon_color3 = love.graphics.newImage("res/image/moon_color3.png")
image.gameplay.moon_border1 = love.graphics.newImage("res/image/moon_border1.png")
image.gameplay.moon_border2 = love.graphics.newImage("res/image/moon_border2.png")
image.gameplay.moon_border3 = love.graphics.newImage("res/image/moon_border3.png")

image.gameplay.comet_color1 = love.graphics.newImage("res/image/comet_color1.png")
image.gameplay.comet_color2 = love.graphics.newImage("res/image/comet_color2.png")
image.gameplay.comet_border1 = love.graphics.newImage("res/image/comet_border1.png")
image.gameplay.comet_border2 = love.graphics.newImage("res/image/comet_border2.png")
image.gameplay.comet_line_color = love.graphics.newImage("res/image/comet_line_color.png")
image.gameplay.comet_line_border = love.graphics.newImage("res/image/comet_line_border.png")
image.gameplay.comet_shine = love.graphics.newImage("res/image/comet_shine.png")

animation.gameplay = {}
animation.gameplay.hit_animation1 = loadAnimation("res/image/hit_animation/1.png", 72, 144, 1000/60)
animation.gameplay.hit_animation2 = loadAnimation("res/image/hit_animation/2.png", 72, 144, 1000/60)
animation.gameplay.hit_animation3 = loadAnimation("res/image/hit_animation/3.png", 72, 144, 1000/60)
animation.gameplay.hit_animation4 = loadAnimation("res/image/hit_animation/4.png", 72, 144, 1000/60)
animation.gameplay.hit_animation5 = loadAnimation("res/image/hit_animation/5.png", 72, 144, 1000/60)

shine_anim_holder = {}

f_combo = love.graphics.newFont("res/font/radiofb.ttf")
f_combo:setFilter("linear", "nearest")

-- timings
judge_timing = {}
judge_timing.collapse = 150
judge_timing.fragment = 100
judge_timing.absolute = 50
judge_timing.exact = 25

judge_coloring = {
    {ms = judge_timing.collapse, color = {0,0.5,0.5}},
    {ms = judge_timing.fragment, color = {1,1,0}},
    {ms = judge_timing.absolute, color = {1,0,0}},
    {ms = judge_timing.exact, color = {1,1,1}}
}

lateral_tick = 250
gameplay_pause = false

require "src/gameplay.input"
require "src/gameplay.intro"
require "src/gameplay.outro"
require "src/gameplay.judges"
require "src/gameplay.loop"
require "src/gameplay.render"
require "src/gameplay.score"
require "src/gameplay.song"
require "src/gameplay.draw"
require "src/gameplay.partner"

Keybind.ingame = {
    lateralLeft = { input = "x"},  -- this game is ab hitting objects at the right time!
    keyOne = { input = "c"},
    keyTwo = { input = "v"},
    keyThree = { input = "b"},
    keyFour = { input = "n"},
    keyFive = { input = "m"},
    lateralRight = { input = ","},
    pause = { input = "escape"}, -- pause the song, take a break, give up, idk
    reset = { input = "r"}, -- quick reset, huh
}