
function prepareSongAudioData()
    song_samplerate = LoadedData.SongData.SampleRate
    song_duration = LoadedData.SongData.Duration
    song_samplecount  = LoadedData.SongData.SampleCount
end

function updateSongData(dt)
    if LoadedData.Song then 
        if song_delay > 0 then 
            song_delay = song_delay - dt*1000
            song_delayed = true
        end

        if not LoadedData.Song:isPlaying() 
        and game_state == game_state_cases.ingame then
            if song_delay <= 0  
            and song_delayed then
                LoadedData.Song:play()
                song_delayed = false
            end
        else
            -- get song position
            song_sample = LoadedData.Song:tell("samples")
            song_time = math.floor(1000*song_sample/LoadedData.SongData.SampleRate) + LoadedData.SongData.Offset + game_audio_offset

            local _ms = song_time
            -- get current beat
            song_count_beat = 0
            song_count_measure = 0

            for i=1, #LoadedChartData.Beats do
                if _ms >= LoadedChartData.Beats[i].ms then
                    if LoadedChartData.Beats[i].measure then
                        song_count_beat = 0
                        song_count_measure = song_count_measure + 1
                    else
                        song_count_beat = song_count_beat + 1
                    end                    
                end
            end
            -- update bpm and beat changes!
            for _, data in pairs(LoadedChartData.IngameMap) do 
                if _ms >= data.ms then
                    local _next_element = string.gmatch(data.command,"%s(%S+)")
                    if string.match(data.command,object.bpm) then
                        song_bpm = tonumber(_next_element()) 
                    end
                    if string.match(data.command,object.beat) then
                        song_beat = tonumber(_next_element()) 
                    end
                end
            end 
        end
    end
end
    
