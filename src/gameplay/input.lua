local lateral1_hold_timer = 0
local lateral2_hold_timer = 0
local lateral1_held = false
local lateral2_held = false
local lateral1_miss_time = lateral_tick/1000
local lateral2_miss_time = lateral_tick/1000
local lateral1_miss_timer = 1
local lateral2_miss_timer = 1

local opacity_step = 0.015
local opacity_base = 1

local input_state = {}
input_state.lateral1 = 0
input_state.lateral2 = 0
input_state.lane1 = 0
input_state.lane2 = 0
input_state.lane3 = 0
input_state.lane4 = 0
input_state.lane5 = 0

local smashes = {}

function gameplayUpdateInputTimers(dt)
    --gameplay element: holds
    lateral1_hold_timer = lateral1_hold_timer - dt  * song_speed
    lateral1_hold_timer = math.max(0,lateral1_hold_timer)

    if false then
        debug_text = "Left"
        debug_text = debug_text .. "\n " .. lateral1_hold_timer
        debug_text = debug_text .. "\n " .. lateral1_miss_timer
        debug_text = debug_text .. "\n Right"
        debug_text = debug_text .. "\n " .. lateral2_hold_timer
        debug_text = debug_text .. "\n " .. lateral2_miss_timer
    end

    if lateral1_hold_timer > 0 then
        if input_state.lateral1 > 0 then lateral1_held = true end

        lateral1_miss_timer = lateral1_miss_timer - dt * song_speed
        
        if lateral1_miss_timer <= 0 then 
            if lateral1_held then 
                gameplayCount("exact", true)
                setJudgeMessage("exact", count_combo)
            else
                gameplayCount("loss", false)
                setJudgeMessage("loss", count_combo)
            end            
            lateral1_held = false
            lateral1_miss_timer = lateral1_miss_timer + lateral1_miss_time
        end    
    else
        if lateral1_miss_timer < lateral1_miss_time then 
            if lateral1_held then 
                gameplayCount("exact", true)
                setJudgeMessage("exact", count_combo)
            else
                gameplayCount("loss", false)
                setJudgeMessage("loss", count_combo)
            end            
        end
        lateral1_held = false
        lateral1_miss_timer = lateral1_miss_time
    end

    lateral2_hold_timer = lateral2_hold_timer - dt  * song_speed
    lateral2_hold_timer = math.max(0,lateral2_hold_timer)

    if lateral2_hold_timer > 0 then        
        if input_state.lateral2 > 0 then lateral2_held = true end

        lateral2_miss_timer = lateral2_miss_timer - dt  * song_speed

        if lateral2_miss_timer <= 0 then 
            if lateral2_held then 
                gameplayCount("exact", true)
                setJudgeMessage("exact", count_combo)
            else
                gameplayCount("loss", false)
                setJudgeMessage("loss", count_combo)
            end            
            lateral2_held = false
            lateral2_miss_timer = lateral2_miss_timer + lateral2_miss_time
        end    
    else
        if lateral2_miss_timer < lateral2_miss_time then 
            if lateral2_held then 
                listJudgement(0)
                gameplayCount("exact", true)
                setJudgeMessage("exact", count_combo)
            else
                gameplayCount("loss", false)
                setJudgeMessage("loss", count_combo)
            end            
        end
        lateral2_held = false
        lateral2_miss_timer = lateral2_miss_time
    end
end

function updateInputState()

    if Keybind.Press(Keybind.ingame.reset) then 
        local _data = songSelectGetSelectedSongData()
        local _chart_num = songSelectGetSelectedChart()
        if LoadedData.Song then LoadedData.Song:stop() end
        LoadedChartData.IngameMap = table.dcopy(LoadedChartData.Map)
        gameplayResetJudgement()
        gameplayResetCounts()
        gameplayPrepareSong(_data, _chart_num)
    end

    if Keybind.Press(Keybind.ingame.pause) then
        if gameplay_pause then
            gameplay_pause = false
            LoadedData.Song:play()
        else
            gameplay_pause = true
            LoadedData.Song:pause()
        end
    end

    -- LEFT LATERAL
    if Keybind.Down(Keybind.ingame.lateralLeft) then
        input_state.lateral1 = input_state.lateral1 + 1
    else
        input_state.lateral1 = 0        
    end

    -- RIGHT LATERAL
    if Keybind.Down(Keybind.ingame.lateralRight) then
        input_state.lateral2 = input_state.lateral2 + 1
    else
        input_state.lateral2 = 0        
    end

    -- LANE ONE
    if Keybind.Down(Keybind.ingame.keyOne) then
        input_state.lane1 = input_state.lane1 + 1
    else
        input_state.lane1 = 0
    end

    -- LANE TWO
    if Keybind.Down(Keybind.ingame.keyTwo) then
        input_state.lane2 = input_state.lane2 + 1
    else
        input_state.lane2 = 0
    end

    -- LANE THREE
    if Keybind.Down(Keybind.ingame.keyThree) then
        input_state.lane3 = input_state.lane3 + 1
    else
        input_state.lane3 = 0
    end

    -- LANE FOUR
    if Keybind.Down(Keybind.ingame.keyFour) then
        input_state.lane4 = input_state.lane4 + 1
    else
        input_state.lane4 = 0
    end

    -- LANE FIVE
    if Keybind.Down(Keybind.ingame.keyFive) then
        input_state.lane5 = input_state.lane5 + 1
    else
        input_state.lane5 = 0
    end

    
    local _smashes = 0
    if input_state.lateral1 == 1 then _smashes = _smashes + 1 end
    if input_state.lateral2 == 1 then _smashes = _smashes + 1 end
    if input_state.lane1 == 1 then _smashes = _smashes + 1 end
    if input_state.lane2 == 1 then _smashes = _smashes + 1 end
    if input_state.lane3 == 1 then _smashes = _smashes + 1 end
    if input_state.lane4 == 1 then _smashes = _smashes + 1 end
    if input_state.lane5 == 1 then _smashes = _smashes + 1 end
    countSmashes(_smashes)
end

function gameplayCommands(i)
    -- get the ms 
    local _ms = LoadedChartData.IngameMap[i].ms
    local _command = LoadedChartData.IngameMap[i].command
    local _time = song_time - song_delay
    local _position = song_time - song_delay - _ms
    local _next_element = string.gmatch(_command,"%s(%S+)")   

    LoadedChartData.IngameMap[i].opacity = LoadedChartData.IngameMap[i].opacity or opacity_base

    if string.match(_command,object.star)
    or string.match(_command,object.moon) then
        if LoadedChartData.IngameMap[i].hit == false then -- if not cleared from calculations
            local _lane = tonumber(_next_element())
            local _width = LoadedChartData.IngameMap[i].width or 1
            
            local _max_range = _ms+judge_timing.collapse   --  _ms_diff = + judge_collapse MAX
            local _min_range = _ms-judge_timing.collapse   --  _ms_diff = - judge_collapse MIN

            if _time > _max_range then -- its a miss
                LoadedChartData.IngameMap[i].hit = true
                judge(_position)
            elseif _time > _min_range then  -- or otherwise in range to judge
                LoadedChartData.IngameMap[i].opacity = LoadedChartData.IngameMap[i].opacity + opacity_step
                for l=_lane, _lane+_width-1 do -- check per possible hittin lane
                    if input_state["lane"..l] == 1 then
                        gameplayAddHitShine(l,LoadedChartData.IngameMap[i].color)
                        LoadedChartData.IngameMap[i].hit = true
                        judge(_position)
                        input_state["lane"..l] = 2
                        break
                    end
                end
            end
        end
    elseif string.match(_command,object.comet) then
        local _duration = tonumber(_next_element()) -- duration of comet line
        local _x1 = tonumber(_next_element()) -- start horizontal position 
        local _x2 = tonumber(_next_element()) -- end horizontal position after duration 
        local _mode = _next_element() -- end horizontal position after duration 

        local _xdiff = _x2 - _x1 
        
        
        if  not LoadedChartData.IngameMap[i].gone_true
        and _time > _ms + _duration then
            LoadedChartData.IngameMap[i].gone_true = true
        end            

        -- hold component
        if  not LoadedChartData.IngameMap[i].gone
        and _time > _ms then
            LoadedChartData.IngameMap[i].gone = true
            if _mode ~= "line" then
                if _xdiff > 0 then -- right
                    if _duration/1000 > lateral2_hold_timer then lateral2_hold_timer = _duration/1000 end
                elseif _xdiff < 0 then -- left
                    if _duration/1000 > lateral1_hold_timer then lateral1_hold_timer = _duration/1000 end
                end
            end
        end             
        
    
        -- hit component
        if  _mode ~= "line" 
        and _mode ~= "hold" 
        and not LoadedChartData.IngameMap[i].hit then -- if not cleared from calculation
            local _max_range = _ms+judge_timing.fragment 
            local _min_range = _ms-judge_timing.fragment   

            if _xdiff ~= 0 then
                if _time > _max_range then -- its a missed hit on comet
                    LoadedChartData.IngameMap[i].hit = true
                    gameplayCount("loss", false)
                    setJudgeMessage("loss", count_combo)
                elseif _time > _min_range then  -- or otherwise in range to hit
                    if _xdiff > 0 then -- right
                        if input_state.lateral2 == 1 then
                            listJudgement(0)
                            LoadedChartData.IngameMap[i].hit = true
                            gameplayCount("exact", true)
                            setJudgeMessage("exact", count_combo)
                        end
                    elseif _xdiff < 0 then -- left
                        if input_state.lateral1 == 1 then
                            listJudgement(0)
                            LoadedChartData.IngameMap[i].hit = true
                            gameplayCount("exact", true)
                            setJudgeMessage("exact", count_combo)
                        end
                    end
                end
            end
        end
    end
end

function gameplayAddHitShine(_lane, _color)
    local _anim = newAnimation(animation.gameplay["hit_animation"..math.floor(math.random(5))])
    addLoopingFunction(_anim, function(_anim) 
        table.remove(shine_anim_holder, _anim.i)
    end)
    _anim.lane = _lane
    _anim.color = _color
    table.insert(shine_anim_holder, _anim)
end

-- draws a glow in lane if there pressed
function gameplayDrawLaneGlow()
    local _preColor = pack(love.graphics.getColor())

    love.graphics.setColor(1,1,1,1)

    local _offset_x = game_width/2 - image.playfield.stage_sides:getWidth()/2
    local _offset_y = 0    
    
    for n=1, 5 do
        if input_state["lane"..n] > 0 then
            love.graphics.draw(image.gameplay.lane_glow,
            _offset_x + 2 + n-1 + (n-1)*lane_w,
            6)
        end
    end

    love.graphics.setColor(1,1,1,0.7)

    if input_state.lateral1 > 0 then
        local _offset_x = game_width/2 - image.playfield.stage_sides:getWidth()/2 - image.gameplay.side_glow_left:getWidth() + 16
        local _offset_y = game_height - image.playfield.stage_bottom:getHeight() - image.gameplay.side_glow_left:getHeight() + 3
        love.graphics.draw(image.gameplay.side_glow_left,_offset_x,_offset_y,0)
    end
    if input_state.lateral2 > 0 then
        local _offset_x = game_width/2 + image.playfield.stage_sides:getWidth()/2 - 16
        local _offset_y = game_height - image.playfield.stage_bottom:getHeight() - image.gameplay.side_glow_right:getHeight() + 3
        love.graphics.draw(image.gameplay.side_glow_right,_offset_x,_offset_y,0)
    
    end

    love.graphics.setColor(unpack(_preColor))
end

function countSmashes(_num)
    local _v = {_num, 1}
    table.insert(smashes,_v)
end

function updateSmashes(_dt)
    for i=1, #smashes do
        if smashes[i] then 
            smashes[i][2] = smashes[i][2] - _dt
            if smashes[i][2] <= 0 then 
                table.remove(smashes, i)
            end
        end
    end
end

function getSmashes()
    local _smash_count = 0
    for i=1, #smashes do
        if smashes[i] then _smash_count = _smash_count + smashes[i][1] end
    end
    return _smash_count
end