local intro_time = 5000
local intro_timer = 0
local intro_text = love.graphics.newText( f_combo )

function gameplayPrepareSong(_data, _chart_num)
    gameplayTogglePartnerEffect()
    _data.Song:stop() 
    song_delay = math.max(1500, intro_time - getChartFirstInputPosition()) 
    intro_timer = intro_time
    song_time = 0 
    song_count_beat = 0
    song_count_measure = 0
    score_display = nil
end

function gameplayUpdateIntro(_dt)
    intro_timer = intro_timer - math.floor(_dt * 1000)
end

function gameplayDrawSongTitle()
    if intro_timer > 0 then
        local _preColor = pack(love.graphics.getColor())

        local _size = 4

        intro_text:setf( 
            {
                {1,1,1}, LoadedData.Title .. "\n",
                {0.5,0.5,0.5}, "by ",
                {1,1,1}, LoadedData.Artist        
            },
            game_width/_size,
            "center"
        )

        local _width = intro_text:getWidth()
        local _height = intro_text:getHeight()
        local _position_y = stage_h * 3/8 

        local _a = math.max(0,intro_timer-500)/1000

        love.graphics.setColor(0,0,0,math.min(0.5,_a/2))

        love.graphics.rectangle(
            "fill", 
            game_width/2 -_width*_size*0.5,
            _position_y - _height*_size*0.25,
            _width*_size,
            _height*_size
        )

        love.graphics.setColor(1,1,1,_a)
        
        love.graphics.draw(
            intro_text,
            0,
            _position_y - _height,
            0,
            _size
        )

        love.graphics.setColor(unpack(_preColor))
    end
end

function gameplayDrawSongSubtitle()
    if  intro_timer > 0 then
        local _preColor = pack(love.graphics.getColor())

        local _size = 2

        intro_text:setf( 
            {
                {0.5,0.5,0.5}, "Chart by ",
                {1,1,1}, LoadedChartData.Charters[1] .. "\n",
                {1,1,1}, "[",
                {1,0,0}, LoadedChartData.Difficulty,
                {1,1,1}, "] ",
                {1,1,1}, LoadedChartData.Name        
            },
            game_width/_size,
            "center"
        )

        local _width = intro_text:getWidth()
        local _height = intro_text:getHeight()
        local _position_y = stage_h * 3/4 

        local _a = math.max(0,intro_timer-500)/1000

        love.graphics.setColor(0,0,0,math.min(0.5,_a/2))

        love.graphics.rectangle(
            "fill",
            game_width/2 -_width*_size*0.5,
            _position_y - _height*_size*0.5,
            _width*_size,
            _height*_size
        )

        love.graphics.setColor(1,1,1,_a)
        
        love.graphics.draw(
            intro_text,
            0,
            _position_y - _height,
            0,
            _size
        )

        love.graphics.setColor(unpack(_preColor))
    end
end