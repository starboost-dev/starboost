local ghost_mode = false

function gameplayToggleGhostPlayfield(bool)
    ghost_mode = bool
end

function ingameDraw()
    prof.push("background") 
        playfieldDrawBackground(c_bg)
    prof.pop("background")

    prof.push("beat glow")
    if not ghost_mode then
        playfieldDrawStageGlow()
    end
    prof.pop("beat glow")

    prof.push("lane glow")
    if not ghost_mode then
        gameplayDrawLaneGlow()
    end
    prof.pop("lane glow")

    prof.push("beats") 
        playfieldDrawBeats() 
    prof.pop("beats")

    prof.push("stage sides") 
    if not ghost_mode then
        playfieldDrawStageSides() 
    end
    prof.pop("stage sides")
    
    prof.push("gameplay")
        gameplayDraw()
    prof.pop("gameplay")

    prof.push("shine")
    if not ghost_mode then
        gameplayDrawShineAnimation()
    end
    prof.pop("shine")

    prof.push("stage bottom")
    if not ghost_mode then
        playfieldDrawStageBottom()
    end
    prof.pop("stage bottom")

    prof.push("overlay")
    if not ghost_mode then
        playfieldDrawOverlay()
    end
    prof.pop("overlay")

    prof.push("accuracy") 
    if not ghost_mode then
        gameplayDrawAccuracy()
    end
    prof.pop("accuracy")

    prof.push("judge") 
        gameplayDrawJudge() 
    prof.pop("judge")


    prof.push("foreground")
        playfieldDrawForegrounds()
    prof.pop("foreground")

    prof.push("intro title")
        gameplayDrawSongTitle()
    prof.pop("intro title")

    prof.push("intro subtitle") 
        gameplayDrawSongSubtitle() 
    prof.pop("intro subtitle")
    
    prof.push("pause")
    if gameplay_pause then
        GameplayDrawPause()
    end
    prof.push("pause")
    
    prof.push("counts")
        gameplayDrawCounts() 
    prof.pop("counts")
end