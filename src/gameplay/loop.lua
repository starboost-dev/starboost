function gameplayLoop(_dt)
    playfieldInput(_dt) 
    if not gameplay_pause then
        prof.push("song_data") updateSongData(_dt) prof.pop("song_data")
        prof.push("timers") gameplayUpdateTimers(_dt) prof.pop("timers")
    end
    prof.push("input") updateInputState() prof.pop("input")
    
    if not gameplay_pause then
        prof.push("gameplay elements")
        for i=1, #LoadedChartData.IngameMap do
            gameplayCommands(i)
        end
        prof.pop("gameplay elements")
        
        prof.push("score") calculateScore(_dt) prof.pop("score")
    end
end

function gameplayUpdateTimers(_dt) 
    for i=1, #shine_anim_holder do
        local _anim = shine_anim_holder[i]
        if _anim then
            _anim.i = i
            playAnimation(_anim, _dt)
        end
    end
    updateSmashes(_dt)
    gameplayUpdateIntro(_dt)
    gameplayUpdateJudgeMessage(_dt)
    gameplayUpdateInputTimers(_dt)
end