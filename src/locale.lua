function buildLocale(path)
  loc = {}
  for line in love.filesystem.lines(path) do
    local _key = string.match(line, "(%S+) =")
    local _value = string.match(line, "= \"(.+)\"")

    if _key then loc[_key] = _value end
  end
end

function replaceVariables(_text, _tbl) -- variable: %var_name
    local i = 0
    while true do
      local _start, _end = string.find(_text, "%%(%w+)", i+1)

      if _start == nil then break else i = _start end

      local _variable = string.sub(_text, _start+1, _end)
      if _tbl[_variable] then 
        _text = string.gsub(_text, "%%".. _variable, _tbl[_variable])
      end
    end
    return _text
end

function getLocale(_key, _tbl)
    local _text = loc[_key]
    if _text then
      local datetime_tbl = {
          a = os.date("%a",os.time()), --%a	abbreviated weekday name (e.g., Wed)
          A = os.date("%A",os.time()), --%A	full weekday name (e.g., Wednesday)
          b = os.date("%b",os.time()), --%b	abbreviated month name (e.g., Sep)
          B = os.date("%B",os.time()), --%B	full month name (e.g., September)
          c = os.date("%c",os.time()), --%c	date and time (e.g., 09/16/98 23:48:10)
          d = os.date("%d",os.time()), --%d	day of the month (16) [01-31]
          H = os.date("%H",os.time()), --%H	hour, using a 24-hour clock (23) [00-23]
          I = os.date("%I",os.time()), --%I	hour, using a 12-hour clock (11) [01-12]
          M = os.date("%M",os.time()), --%M	minute (48) [00-59]
          m = os.date("%m",os.time()), --%m	month (09) [01-12]
          p = os.date("%p",os.time()), --%p	either "am" or "pm" (pm)
          S = os.date("%S",os.time()), --%S	second (10) [00-61]
          w = os.date("%w",os.time()), --%w	weekday (3) [0-6 = Sunday-Saturday]
          x = os.date("%x",os.time()), --%x	date (e.g., 09/16/98)
          X = os.date("%X",os.time()), --%X	time (e.g., 23:48:10)
          Y = os.date("%Y",os.time()), --%Y	full year (1998)
          y = os.date("%y",os.time()), --%y	two-digit year (98) [00-99]
          Q = os.date("%%",os.time())  --%%	the character `%´
      }
      _text = replaceVariables(_text, datetime_tbl)
      if _tbl then _text = replaceVariables(_text, _tbl) end
      return _text
    end
    return ""
end
