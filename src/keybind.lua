Keybind = {}

function Keybind.Down(_keybind)
-- check if a keybind is being held down this frame
    if love.keyboard.isScancodeDown(_keybind.input)
    and not Keybind.lock(_keybind) then
        return true
    end
end

function Keybind.Down_delayed(_keybind,_delay, _dt)
-- check if a keybind is being held down this frame
    if _keybind.delay then 
        _keybind.delay = _keybind.delay - _dt
        if _keybind.delay <= 0 then _keybind.delay = nil end
    else
        if love.keyboard.isScancodeDown(_keybind.input)
        and not Keybind.lock(_keybind) then
            _keybind.delay = _delay
            return true
        end
    end
end

function Keybind.Press(_keybind)
    if love.keyboard.isScancodeDown(_keybind.input) 
    and not Keybind.lock(_keybind) then
        -- check if a keybind has just been pressed this frame, doesnt work for the next frame, only 1 frame of being pressed
        if not _keybind.flag then
            _keybind.flag = true
            return true
        end
    else
        _keybind.flag = nil
    end
end

function Keybind.lock(_keybind)
    if _keybind.locked then 
        _keybind.locked = nil
        return false
    else
        _keybind.locked = true
        return true
    end
end

function Keybind.lock(_keybind)
    if _keybind == Keybind.editor.lockSetting then
        return false 
    else
        if Keybind.Press(Keybind.editor.lockSetting) then
            if _keybind.locked then 
                _keybind.locked = nil
                return true
            else
                _keybind.locked = true
                return true
            end
        end
        if _keybind.locked then 
            return true
        else
            return false
        end
    end
end

function Keybind.GASP(_keybind, _text)
-- Get As String Pairs  
    local _color = {0.5,0.5,0.5}
    if _keybind.locked then
        _color = {1,0,0}
    elseif love.keyboard.isScancodeDown(_keybind.input) then
        _color = {1,1,0}
    end    
    local _text = _text or ""
    return _color, "<" .. _keybind.input .. ">" .. _text
end
