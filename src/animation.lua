-- very simple spritesheet -> animation system implemented in 1h -- so cool tho
animation = {}

-- use this to load it, but remember to copy the contents using newAnimation() before use!!
function loadAnimation(_path, _width, _height, _speed) 
	local _image = love.graphics.newImage(_path)
    local _dx, _dy = _image:getDimensions()

    local _t = {}
    local _count = _dx/_width
    for i=1, _count do
        local _quad = love.graphics.newQuad((i-1)*_width,  0,  _width, _height, _dx, _dy)
        table.insert(_t, _quad)
    end

    local _anim = {}
    _anim.image = _image
    _anim.width = _width
    _anim.height = _height
    _anim.frames = _t
    _anim.frame = 1
    _anim.framecount = _count
    _anim.speed = _speed/1000 -- in ms
    _anim.timer = 0
    _anim.color = {1,1,1}

    return _anim
end

function newAnimation(_anim)
    return table.dcopy(_anim)
end

function addLoopingFunction(_anim, _func)
    _anim.loop_function = _func
end

function playAnimation(_anim, _dt)
    _anim.timer = _anim.timer + _dt
    if _anim.timer >= _anim.speed then 
        _anim.timer = _anim.timer - _anim.speed 
        _anim.frame = _anim.frame + 1
    end
    if _anim.frame > _anim.framecount then
        _anim.frame = _anim.frame - _anim.framecount 
        if _anim.loop_function then _anim.loop_function(_anim) end
    end
end


function drawAnimation(_anim, _x, _y, _r, _sx, _sy, _ox, _oy, _kx, _ky)
    local _preColor = pack(love.graphics.getColor())
    
    love.graphics.draw(
        _anim.image,
        _anim.frames[_anim.frame], 
        _x, _y, _r, _sx, _sy, _ox, _oy, _kx, _ky
    )

    love.graphics.setColor(unpack(_preColor))
end

function drawAnimationSheet(_anim, _x, _y, _r, _sx, _sy, _ox, _oy, _kx, _ky)
    local _x = _x or 0
    local _preColor = pack(love.graphics.getColor())
    
    for i=1, #_anim.frames do
        love.graphics.draw(
            _anim.image,
            _anim.frames[i], 
            _x + (i-1)*_anim.width, _y, _r, _sx, _sy, _ox, _oy, _kx, _ky
        )
    end

    love.graphics.setColor(unpack(_preColor))
end