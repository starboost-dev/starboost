const puppeteer = require('puppeteer')

;(async () => {
  const browser = await puppeteer.launch()
  const page = await browser.newPage()
  await page.goto('http://127.0.0.1:5500/res/song%20select%20ui%20prototype/', {
    waitUntil: 'networkidle0'
  })
  const elements = await page.$('#songinfoinfo img')
  await page.evaluate(() => (document.body.style.background = 'transparent'))
  await elements.screenshot({ path: 'myImg.png', omitBackground: true })
  await browser.close()
})()