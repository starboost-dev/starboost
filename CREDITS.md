## Developers

  - **Game Concept & Design, Backend Developer** - remi
  - **Game Design, Engine Developer** -  lustlion
  - **Minor Engine Contributions** - beane
  - **Minor Engine Contributions** - sfr

## Graphics

  - **Partners** - Generated with NovelAI 
  - **UI design, Game Sprites** - remi

## Charters

  - remi
  - beane
  - sfr

## Documentation
  - **Manually kept updated by** lustlion

## Fonts

  - **Signika** - Anna Giedrys
    > https://github.com/Ancymonic/Signika  
    > https://raw.githubusercontent.com/Ancymonic/Signika/master/OFL.txt

  - **Radio Space** - Dan Zadorozny (Iconian Fonts)
    > http://www.iconian.com/index.html  
    > http://www.iconian.com/commercial.html (tl;dr **"My fonts are free for noncommercial use"**)

## Libraries

  - **jprof** - pfirsich
    > https://github.com/pfirsich/jprof 
    > https://raw.githubusercontent.com/pfirsich/jprof/master/LICENSE

  - **sone** - camchenry
    > https://github.com/camchenry/sone 
    > https://raw.githubusercontent.com/camchenry/sone/master/LICENSE